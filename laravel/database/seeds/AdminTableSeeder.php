<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('administradores')->insert([
            'nome'  => 'trupe',
            'email' => 'contato@trupe.net',
            'senha' => bcrypt('senhatrupe'),
        ]);
    }
}

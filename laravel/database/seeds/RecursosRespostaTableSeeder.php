<?php

use Illuminate\Database\Seeder;

class RecursosRespostaGeralTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('recursos_resposta_geral')->insert([
            'resposta_geral' => '',
            'ativo'          => 'false'
        ]);
    }
}

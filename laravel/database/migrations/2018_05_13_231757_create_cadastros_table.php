<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCadastrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadastros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('senha', 60);

            // dados pessoais
            $table->boolean('dados_pessoais_enviados')->default(0);
            $table->string('nome');
            $table->string('nome_certificado');
            $table->string('cpf');
            $table->longText('crm');
            $table->string('cep');
            $table->string('endereco');
            $table->string('numero');
            $table->string('complemento');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('uf');
            $table->string('telefone_residencial');
            $table->string('telefone_comercial');
            $table->string('telefone_celular');
            $table->string('nascimento');
            $table->string('sexo');
            $table->string('lateralidade');
            $table->longText('socio_amb');

            // documentacao obrigatoria
            $table->string('documentacao_obrigatoria_status')->default('não preenchido');
            $table->longText('documentacao_obrigatoria');

            // especializacao em cardiologia
            $table->string('especializacao_cardiologia_status')->default('não preenchido');
            $table->longText('estagio_2_anos_clinica_medica');
            $table->longText('estagio_2_anos_clinica_medica_nao_credenciado');
            $table->longText('estagio_2_anos_clinica_medica_e_cardiologia');
            $table->longText('residencia_2_anos_clinica_medica');
            $table->longText('titulo_especialista_clinica_medica');
            $table->longText('estagio_2_anos_cardiologia');
            $table->longText('estagio_2_anos_cardiologia_nao_credenciado');
            $table->longText('residencia_2_anos_cardiologia');
            $table->longText('residencia_2_anos_cardiologia_exterior');
            $table->longText('comprovante_capacitacao_clinica_medica');
            $table->longText('comprovante_atuacao_cardiologia');

            // documentos facultativos
            $table->string('documentos_facultativos_status')->default('não preenchido');
            $table->longText('participacao_congressos_cardiologia');
            $table->longText('congresso_brasileiro_cargiologia');
            $table->longText('mestrado_cardiologia');
            $table->longText('doutorado_livre_docencia_cardiologia');
            $table->longText('cursos_presenciais_reciclagem');
            $table->longText('curso_presencial_sbc');
            $table->longText('participacao_jornadas_simposios');
            $table->longText('conclusao_savic_acls_teca_a');
            $table->longText('conclusao_cursos_distancia');
            $table->longText('curso_online_captec');
            $table->longText('publicacao_artigo_original');
            $table->longText('capitulo_livro_cardiologia');
            $table->longText('oral_ou_poster_congresso');
            $table->longText('participacao_registros_brasileiros');
            $table->longText('experiencia_profissional_15_anos');
            $table->longText('experiencia_profissional_mestrado_doutorado');

            // declaracoes finais
            $table->string('declaracoes_finais_status')->default('não preenchido');
            $table->longText('declaracao_pessoa_portadora_deficiencia');
            $table->longText('declaracao_veracidade_informacoes');

            // data de envio
            $table->string('data_envio_documentacao')->nullable();
            $table->string('data_envio_substituicao')->nullable();

            // status
            // novo         - inscriçao em aberto
            // enviado      - inscriçao enviada pelo usuário
            // andamento    - inscriçao em andamento no admin
            // solicitado   - inscriçao com solicitaçoes de substituiçao pelo admin
            // substituido  - inscriçao com solicitaçoes retornadas pelo usuário
            // valido       - inscriçao validada por admin
            // invalido     - inscriçao invalidada por admin
            $table->string('status')->default('novo');
            $table->string('status_data')->nullable();
            $table->string('status_admin')->nullable();
            $table->integer('status_admin_id')->nullable();

            // pontuacao
            $table->decimal('pontuacao_especializacao', 3, 1);
            $table->decimal('pontuacao_facultativa', 3, 1);
            $table->decimal('pontuacao_total', 3, 1);

            // revisao
            $table->string('revisao_envio')->nullable();
            $table->text('revisao_mensagem');
            $table->string('revisao_conclusao')->nullable();

            // recurso
            $table->longtext('recurso');

            // pagamento
            $table->boolean('pago');

            // resultado final
            $table->decimal('pontuacao_prova', 4, 1);
            $table->decimal('pontuacao_prova_pos_recurso', 4, 1);
            $table->string('status_avaliacao'); // aprovado ou reprovado
            $table->string('pontuacao_prova_excecao');

            // liberado
            $table->boolean('liberado');
            $table->string('data_primeiro_envio')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });

        \DB::statement("ALTER TABLE cadastros AUTO_INCREMENT = 19549;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cadastros');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoLiberacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_liberacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cadastro_id')->unsigned()->nullable();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');

            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->text('observacoes');
            $table->longText('dados');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historico_liberacoes');
    }
}

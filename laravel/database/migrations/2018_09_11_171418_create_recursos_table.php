<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recursos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cadastro_id')->unsigned()->nullable();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');

            $table->string('protocolo')->unique();
            $table->string('prova', 10);
            $table->integer('questao');
            $table->integer('questao_matriz');
            $table->text('justificativa');
            $table->text('bibliografia');

            $table->string('documento_do_recurso');
            $table->string('comprovante_de_deposito');

            $table->string('data_envio')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recursos');
    }
}

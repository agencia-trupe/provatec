<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecursosRespostasTable extends Migration
{
    public function up()
    {
        Schema::create('recursos_respostas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('questao_matriz')->unique();
            $table->text('resposta');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('recursos_respostas');
    }
}

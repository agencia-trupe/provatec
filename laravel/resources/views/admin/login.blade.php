@extends('frontend.common.template')

@section('content')

    <div class="login center">
        <div class="main-box half-width half-width-center">
            <h2>Acesso de Administração</h2>
            <form action="{{ route('admin.loginPost') }}" method="POST">
                @if(session('erro-login'))
                    <div class="erro">{{ session('erro-login') }}</div>
                @endif

                {!! csrf_field() !!}
                <input type="email" name="email" value="{{ old('email') }}" placeholder="login [e-mail]" required>
                <input type="password" name="senha" placeholder="senha" required>
                <input type="submit" value="ENTRAR">
            </form>
        </div>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    <div class="main-admin">
        <div class="center">
            @include('admin._nav')

            <a href="{{ route('admin.inscricao', $inscricao->id) }}" class="barra-nome-pontos">
                <div class="nome">
                    <strong>{{ $inscricao->nome }}</strong>
                    <span>|</span>
                    inscrição recebida em:
                    {{ Tools::formataDataHorario($inscricao->data_envio_documentacao) }}
                </div>
                <div class="pontos">
                    TOTAL DE PONTOS:
                    <span>{{ $inscricao->pontuacao_total == 0 ? '00' : (float)$inscricao->pontuacao_total }}</span>
                </div>
            </a>
            @if($inscricao->status != 'enviado' && $inscricao->status != 'andamento')
                <div class="barra-status {{ $inscricao->status }}">
                    @if($inscricao->status == 'valido')
                    INSCRIÇÃO VALIDADA EM {{ Tools::formataData($inscricao->status_data) }} POR {{ strtoupper($inscricao->status_admin) }}
                    @elseif($inscricao->status == 'invalido')
                    INSCRIÇÃO INVALIDADA EM {{ Tools::formataData($inscricao->status_data) }} POR {{ strtoupper($inscricao->status_admin) }}
                    @elseif($inscricao->status == 'solicitado')
                    SOLICITAÇÕES DE SUBSTITUIÇÃO ENVIADAS EM {{ Tools::formataData($inscricao->status_data) }} POR {{ strtoupper($inscricao->status_admin) }}
                    @elseif($inscricao->status == 'substituido')
                    PENDÊNCIAS SUBSTITUÍDAS PELO USUÁRIO EM {{ Tools::formataData($inscricao->status_data) }}
                    @endif
                </div>
            @endif
        </div>

        <div class="admin-comprovantes">
            <div class="center">
                <h1>
                    {!! App\Models\Campos::getTituloDescritivo($inscricao, $comprovante, $grupo) !!}
                </h1>
                @if($inscricao->getComprovanteStatus($comprovante, $grupo))
                    <div style="margin:-15px 0 30px;text-align:center">
                        @include('admin._comprovante-btn', [
                            'displayDiv' => true,
                            'link' => route('admin.comprovante', [
                                $inscricao->id,
                                $comprovante,
                                $grupo
                            ]),
                            'status' => $inscricao->getComprovanteStatus($comprovante, $grupo),
                            'label'  => $inscricao->getComprovanteStatusLabel($comprovante, $grupo),
                            'total'  => $inscricao->pontuacao_total
                        ])
                    </div>
                @endif
                @if($inscricao->getComprovanteStatus($comprovante, $grupo) == 'substituido')
                    <div class="comprovante-substituicao">
                        @if($grupo)
                            <p class="solicitacao">{{ $inscricao->getComprovante($comprovante)[$grupo]['msg_solicitacao'] }}</p>
                            <p class="resposta">{{ $inscricao->getComprovante($comprovante)[$grupo]['resposta'] }}</p>
                        @else
                            <p class="solicitacao">{{ $inscricao->getComprovante($comprovante)->msg_solicitacao }}</p>
                            <p class="resposta">{{ $inscricao->getComprovante($comprovante)->resposta }}</p>
                        @endif
                    </div>
                @endif
            </div>

            @if(count($arquivos))
            <div class="admin-comprovantes-arquivos">
                @foreach($arquivos as $arquivo)
                    @if(substr(strtolower($arquivo), -4) == '.pdf')
                        <object data="{{ asset('comprovantes/'.$arquivo) }}" type="application/pdf">
                            <embed src="{{ asset('comprovantes/'.$arquivo) }}" type="application/pdf" />
                        </object>
                    @else
                        <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">
                            <img src="{{ asset('comprovantes/'.$arquivo) }}" alt="">
                        </a>
                    @endif
                    <div style="font-family: Exo\ 2,Helvetica,Arial,sans-serif;text-align:center;color:#666;font-style:italic;font-size:14px;margin:.5em 0 2em">
                        enviado pelo usuário em:
                        {{ Tools::getDataNomeArquivo($arquivo) }}
                    </div>
                @endforeach
            </div>
            @else
            <h1 style="font-size:17px">nenhum arquivo enviado</h1>
            @endif

            <div class="center">
                <div class="admin-comprovantes-botoes">
                    <a href="{{ route('admin.comprovante.validar', [$inscricao->id, $comprovante, $grupo]) }}">VÁLIDO</a>
                    <a href="{{ route('admin.comprovante.invalidar', [$inscricao->id, $comprovante, $grupo]) }}" class="--invalido">INVÁLIDO</a>
                    <a href="#" class="admin-open-substituicao">REQUER SUBSTITUIÇÃO</a>
                </div>
                @if($errors->any())
                <form action="{{ route('admin.comprovante.solicitarSubstituicao', [$inscricao->id, $comprovante, $grupo]) }}" method="POST" class="form-substituicao" style="display:block">
                @else
                <form action="{{ route('admin.comprovante.solicitarSubstituicao', [$inscricao->id, $comprovante, $grupo]) }}" method="POST" class="form-substituicao" style="display:none">
                @endif
                    {!! csrf_field() !!}
                    <h3>SOLICITAR SUBSTITUIÇÃO DE COMPROVANTE</h3>
                    <p>Substituição de: <span>{!! App\Models\Campos::getTituloDescritivo($inscricao, $comprovante, $grupo) !!}</span></p>
                    @if($errors->any())
                        <div class="erro">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                        </div>
                    @endif
                    <textarea name="solicitacao" placeholder="Solicitação" required></textarea>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>


@endsection

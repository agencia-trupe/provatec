@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(count($inscricoes))
        <h2>{{ $inscricoes->total() }} {{ $inscricoes->total() > 1 ? 'inscrições não enviadas' : 'inscrição não enviada' }}</h2>

        <table class="table-listagem">
            <thead>
                <th style="text-align:left">nome:</th>
                <th>inscrição:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($inscricoes as $inscricao)
                <tr>
                    <td>{!! $inscricao->nome ?: '<em>nome não cadastrado</em>' !!}</td>
                    <td class="inscricao">{{ $inscricao->id }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="warning">Nenhuma inscrição encontrada</div>
        @endif

        {!! $inscricoes->render() !!}
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(count($inscricoes))
        <h2>{{ count($inscricoes) }} {{ count($inscricoes) > 1 ? 'inscrições encontradas' : 'inscrição encontrada' }}</h2>

        <table class="table-listagem">
            <thead>
                <th style="text-align:left">nome:</th>
                <th>inscrição:</th>
                <th>status:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($inscricoes as $inscricao)
                <tr class="table-link" href="{{ route('admin.inscricao', $inscricao->id) }}">
                    <td>{{ $inscricao->nome }}</td>
                    <td class="inscricao">{{ $inscricao->id }}</td>
                    <td class="data">
                        @if($inscricao->status == 'enviado')
                        nova
                        @elseif($inscricao->status == 'andamento')
                        em andamento
                        @elseif($inscricao->status == 'solicitado')
                        substituições solicitadas
                        @elseif($inscricao->status == 'substituido')
                        substituições recebidas
                        @elseif($inscricao->status == 'valido')
                        validada
                        @elseif($inscricao->status == 'invalido')
                        invalidada
                        @endif
                    </td>
                    <td class="table-btn">VISUALIZAR</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="warning">Nenhuma inscrição encontrada</div>
        @endif

        {!! $inscricoes->render() !!}
    </div>

@endsection

<table class="table-campos title-small">
    <tr>
        <td>nome:</td>
        <td>{{ $inscricao->nome }}</td>
    </tr>
    <tr>
        <td>e-mail:</td>
        <td>{{ $inscricao->email }}</td>
    </tr>
    <tr>
        <td>nome para o certificado:</td>
        <td>{{ $inscricao->nome_certificado }}</td>
    </tr>
    <tr>
        <td>CPF:</td>
        <td>{{ $inscricao->cpf }}</td>
    </tr>
    <tr>
        <td>CRM:</td>
        <td>
            @foreach($inscricao->getComprovante('crm') as $key => $crm)
                <div class="comprovante-row">
                    {{ $crm['numero'] }}
                    @include('admin._comprovante-btn', [
                        'link'   => route('admin.comprovante', [
                            $inscricao->id,
                            'crm',
                            $key
                        ]),
                        'status' => $inscricao->getComprovanteStatus('crm', $key),
                        'label'  => $inscricao->getComprovanteStatusLabel('crm', $key)
                    ])
                </div>
            @endforeach
        </td>
    </tr>
    <tr>
        <td>endereço:</td>
        <td>
            {{ $inscricao->endereco }}
            {{ $inscricao->numero }}
            {{ $inscricao->complemento ? '- '.$inscricao->complemento : '' }}
            - {{ $inscricao->bairro }}
            - {{ $inscricao->cidade }}
            - {{ $inscricao->uf }}
            - {{ $inscricao->cep }}
        </td>
    </tr>
    <tr>
        <td>telefone residencial:</td>
        <td>{{ $inscricao->telefone_residencial ?: '-' }}</td>
    </tr>
    <tr>
        <td>telefone comercial:</td>
        <td>{{ $inscricao->telefone_comercial ?: '-' }}</td>
    </tr>
    <tr>
        <td>telefone celular:</td>
        <td>{{ $inscricao->telefone_celular }}</td>
    </tr>
    <tr>
        <td>data de nascimento:</td>
        <td>{{ $inscricao->nascimento }}</td>
    </tr>
    <tr>
        <td>sexo:</td>
        <td>{{ $inscricao->sexo }}</td>
    </tr>
    <tr>
        <td>lateralidade:</td>
        <td>{{ $inscricao->lateralidade }}</td>
    </tr>
    <tr>
        <td>sócio AMB:</td>
        <td>
            {{ $inscricao->getComprovante('socio_amb')->select }}
            @if($inscricao->getComprovante('socio_amb')->select == 'Sim')
                @include('admin._comprovante-btn', [
                    'link'   => route('admin.comprovante', [
                        $inscricao->id,
                        'socio_amb'
                    ]),
                    'status' => $inscricao->getComprovanteStatus('socio_amb'),
                    'label'  => $inscricao->getComprovanteStatusLabel('socio_amb')
                ])
            @endif
        </td>
    </tr>
</table>

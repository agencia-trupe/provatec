<table class="table-campos title-small">
    <tr>
        <td>graduação:</td>
        <td>
            {{ Tools::getOutra($inscricao->getComprovante('documentacao_obrigatoria'), 'graduacao') }}
            | {{ $inscricao->getComprovante('documentacao_obrigatoria')->ano_graduacao }}
            | {{ $inscricao->getComprovante('documentacao_obrigatoria')->uf }}
            @include('admin._comprovante-btn', [
                'link'   => route('admin.comprovante', [
                    $inscricao->id,
                    'documentacao_obrigatoria'
                ]),
                'status' => $inscricao->getComprovanteStatus('documentacao_obrigatoria'),
                'label'  => $inscricao->getComprovanteStatusLabel('documentacao_obrigatoria')
            ])
        </td>
    </tr>
    <tr>
        <td>declaração de veracidade das informações:</td>
        <td>
            sim
            @include('admin._comprovante-btn', [
                'link'   => route('admin.comprovante', [
                    $inscricao->id,
                    'declaracao_veracidade_informacoes'
                ]),
                'status' => $inscricao->getComprovanteStatus('declaracao_veracidade_informacoes'),
                'label'  => $inscricao->getComprovanteStatusLabel('declaracao_veracidade_informacoes')
            ])
        </td>
    </tr>
    <tr>
        <td>declaração de pessoa portadora de deficiência:</td>
        <td>
            @if($inscricao->getComprovante('declaracao_pessoa_portadora_deficiencia') && count($inscricao->getComprovante('declaracao_pessoa_portadora_deficiencia')->arquivos))
                sim
                @include('admin._comprovante-btn', [
                    'link'   => route('admin.comprovante', [
                        $inscricao->id,
                        'declaracao_pessoa_portadora_deficiencia'
                    ]),
                    'status' => $inscricao->getComprovanteStatus('declaracao_pessoa_portadora_deficiencia'),
                    'label'  => $inscricao->getComprovanteStatusLabel('declaracao_pessoa_portadora_deficiencia')
                ])
            @else
                -
            @endif
        </td>
    </tr>
</table>

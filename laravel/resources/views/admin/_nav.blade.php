<div class="total-inscricoes-enviadas">
    Total de inscrições recebidas: <strong>{{ $totalInscricoesEnviadas }}</strong>
    <a href="{{ route('admin') }}">[ver resumo]</a>
</div>

<form action="{{ route('admin.busca') }}" method="GET" class="form-filtro">
    <input type="text" name="inscricao" placeholder="número de inscrição" value="{{ request()->get('inscricao') }}">
    <input type="text" name="nome" placeholder="nome" value="{{ request()->get('nome') }}">
    <input type="submit" value="BUSCAR">
</form>

<nav>
    <a href="{{ route('admin.novasInscricoes') }}" @if(Tools::isActive('admin.novasInscricoes')) class="active" @endif>NOVAS INSCRIÇÕES</a>
    <a href="{{ route('admin.validacoesEmAndamento') }}" @if(Tools::isActive('admin.validacoesEmAndamento')) class="active" @endif>VALIDAÇÕES EM ANDAMENTO</a>
    <a href="{{ route('admin.substituicoesSolicitadas') }}" @if(Tools::isActive('admin.substituicoesSolicitadas')) class="active" @endif>SUBSTITUIÇÕES SOLICITADAS</a>
    <a href="{{ route('admin.substituicoesRecebidas') }}" @if(Tools::isActive('admin.substituicoesRecebidas')) class="active" @endif>SUBSTITUIÇÕES RECEBIDAS</a>
    <a href="{{ route('admin.liberacoes') }}" @if(Tools::isActive('admin.liberacoes')) class="active" @endif>LIBERAÇÕES</a>
    <a href="{{ route('admin.excecoes') }}" @if(Tools::isActive('admin.excecoes')) class="active" @endif>EXCEÇÕES</a>
    <div style="margin-top: 10px"></div>
    <a href="{{ route('admin.inscricoesValidadas') }}" @if(Tools::isActive('admin.inscricoesValidadas')) class="active" @endif>INSCRIÇÕES VALIDADAS</a>
    <a href="{{ route('admin.inscricoesInvalidadas') }}" @if(Tools::isActive('admin.inscricoesInvalidadas')) class="active" @endif>INSCRIÇÕES INVALIDADAS</a>
    <a href="{{ route('admin.naoEnviadas') }}" @if(Tools::isActive('admin.naoEnviadas')) class="active" @endif>INSCRIÇÕES NÃO ENVIADAS</a>
    <a href="{{ route('admin.solicitacoesDeRevisao') }}" @if(Tools::isActive('admin.solicitacoesDeRevisao')) class="active" @endif>SOLICITAÇÕES DE REVISÃO</a>
    <a href="{{ route('admin.recursos') }}" @if(Tools::isActive('admin.recursos*')) class="active" @endif>RECURSOS PÓS-PROVA</a>
</nav>

@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(count($inscricoes))
        <h2>{{ $inscricoes->total() }} {{ $inscricoes->total() > 1 ? 'inscrições' : 'inscrição' }} com esse status</h2>

        <table class="table-listagem">
            <thead>
                <th></th>
                <th>inscrição:</th>
                <th>data de inscrição:</th>
                <th>data de substituição:</th>
                <th>solicitado por:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($inscricoes as $inscricao)
                <tr class="table-link" href="{{ route('admin.inscricao', $inscricao->id) }}">
                    <td>{{ $inscricao->nome }}</td>
                    <td class="inscricao">{{ $inscricao->id }}</td>
                    <td class="data">{{ Tools::formataDataHorario($inscricao->data_envio_documentacao) }}</td>
                    <td class="data">{{ Tools::formataDataHorario($inscricao->status_data) }}</td>
                    <td class="dados">{{ $inscricao->status_admin }}</td>
                    <td class="table-btn">VALIDAR</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="warning">Nenhuma inscrição encontrada</div>
        @endif

        {!! $inscricoes->render() !!}
    </div>

@endsection

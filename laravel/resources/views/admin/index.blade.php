@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        <div class="main-admin-resumo">
            <h2>RESUMO</h2>

            @foreach($resumo as $titulo => $valor)
            <div class="row">
                <div class="titulo">{{ $titulo }}:</div>
                <div class="valor">{{ $valor }}</div>
            </div>
            @endforeach
        </div>
    </div>

@endsection

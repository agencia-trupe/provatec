@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        <div class="main-admin-liberacoes">
            <h2>HISTÓRICO DE LIBERAÇÕES GERAIS</h2>

            @if(count($liberacoes))
            <div>
                @foreach($liberacoes as $id => $liberacoes)
                <div class="row">
                    <div class="left">
                        <p>
                            <strong>
                                {{ $id }} |
                                {{ mb_strtoupper($liberacoes->first()->cadastro->nome) }}
                            </strong>
                            <br>
                            <small>
                                ENVIO INICIAL EM:
                                {{ Tools::formataDataHorario($liberacoes->first()->cadastro->data_primeiro_envio) }}
                            </small>
                        </p>
                    </div>
                    <div class="right">
                    @foreach($liberacoes as $historico)
                        @if($historico->admin)
                            <p>
                                LIBERADO EM: {{ Tools::formataDataHorario($historico->created_at) }} | por: {{ explode(' ', $historico->admin->nome)[0] }}
                                @if($historico->observacoes)
                                <br>
                                <em>observações:</em> {{ $historico->observacoes }}
                                @endif
                            </p>
                        @else
                            <p style="font-weight:bold">
                                REENVIADO EM:
                                {{ Tools::formataDataHorario($historico->created_at) }} | por: {{ explode(' ', $historico->cadastro->nome)[0] }}
                            </p>
                        @endif
                    @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <div class="warning">Nenhuma liberação encontrada</div>
            @endif
        </div>
    </div>

@endsection

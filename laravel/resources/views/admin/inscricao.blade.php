@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(session('erro'))
        <div class="warning warning-expirado" style="margin-bottom:20px">{{ session('erro') }}</div>
        @endif

        <div class="barra-nome-pontos">
            <div class="nome" style="vertical-align:middle">
                <strong>{{ $inscricao->nome }}</strong>
                <span>|</span>
                inscrição recebida em:
                {{ Tools::formataDataHorario($inscricao->data_envio_documentacao) }}
                <span style="display:block;margin:10px 0 0">
                    número de inscrição: <strong>{{ $inscricao->id }}</strong>
                </span>
            </div>
            <div class="pontos" style="line-height:1.8;vertical-align:middle">
                Pontuação Documentação:
                <span>{{ $inscricao->pontuacao_total == 0 ? '00' : (float)$inscricao->pontuacao_total }}</span>
                @if($inscricao->pontuacao_prova > 0)
                <br>
                Pontuação Prova:
                <span>{{ (float)$inscricao->pontuacao_prova }}</span>
                <br>
                @if($inscricao->pontuacao_prova_pos_recurso)
                Pontuação Pós-recurso:
                <span>{{ (float)$inscricao->pontuacao_prova_pos_recurso }}</span>
                <br>
                @endif
                PONTUAÇÃO TOTAL:
                <span>{{ (float) $inscricao->pontuacao_final }}</span>
                @endif
                @if($inscricao->status_avaliacao)
                <br>
                status:
                <span>{{ strtoupper($inscricao->status_avaliacao) }}</span>
                @endif
            </div>
        </div>
        @if($inscricao->status != 'enviado' && $inscricao->status != 'andamento')
            <div class="barra-status {{ $inscricao->status }}">
                @if($inscricao->status == 'valido')
                INSCRIÇÃO VALIDADA EM {{ Tools::formataData($inscricao->status_data) }} POR {{ strtoupper($inscricao->status_admin) }}
                @elseif($inscricao->status == 'invalido')
                INSCRIÇÃO INVALIDADA EM {{ Tools::formataData($inscricao->status_data) }} POR {{ strtoupper($inscricao->status_admin) }}
                @elseif($inscricao->status == 'solicitado')
                SOLICITAÇÕES DE SUBSTITUIÇÃO ENVIADAS EM {{ Tools::formataData($inscricao->status_data) }} POR {{ strtoupper($inscricao->status_admin) }}
                @elseif($inscricao->status == 'substituido')
                PENDÊNCIAS SUBSTITUÍDAS PELO USUÁRIO EM {{ Tools::formataData($inscricao->status_data) }}
                @endif
            </div>
        @endif

        @if($inscricao->revisao_envio)
        <div class="box-solicitacao-revisao" @if($inscricao->revisao_conclusao) style="opacity:.7" @endif>
            <h3>
                SOLICITAÇÃO DE REVISÃO
                <span>enviada em: <strong>{{ Tools::formataData($inscricao->revisao_envio) }}</strong></span>
                @if($inscricao->revisao_conclusao)
                <span>concluída em: <strong>{{ Tools::formataData($inscricao->revisao_conclusao) }}</strong></span>
                @else
                <a href="{{ route('admin.concluirRevisao', $inscricao->id) }}">CONCLUIR REVISÃO</a>
                @endif
            </h3>
            <p @if($inscricao->revisao_conclusao) style="font-size:.9em" @endif>{!! nl2br($inscricao->revisao_mensagem) !!}</p>
        </div>
        @endif

        <div class="tabs">
            <a href="{{ route('admin.inscricao', [$inscricao->id, 'dados-pessoais']) }}" @if($secao == 'dados-pessoais') class="active" @endif>DADOS PESSOAIS</a>
            <a href="{{ route('admin.inscricao', [$inscricao->id, 'documentacao-obrigatoria']) }}" @if($secao == 'documentacao-obrigatoria') class="active" @endif>DOC. OBRIGATÓRIA</a>
            <a href="{{ route('admin.inscricao', [$inscricao->id, 'especializacao']) }}" @if($secao == 'especializacao') class="active" @endif>ESPECIALIZAÇÃO | <strong>{{ $inscricao->pontuacao_especializacao == 0 ? '00' : (float)$inscricao->pontuacao_especializacao }} pts</strong></a>
            <a href="{{ route('admin.inscricao', [$inscricao->id, 'documentacao-facultativa']) }}" @if($secao == 'documentacao-facultativa') class="active" @endif>DOC. FACULTATIVA | <strong>{{ $inscricao->pontuacao_facultativa == 0 ? '00' : (float)$inscricao->pontuacao_facultativa }} pts</strong></a>
            <a href="{{ route('admin.inscricao', [$inscricao->id, 'pagamento']) }}" @if($secao == 'pagamento') class="active" @endif>PAGAMENTO</a>
            <a href="{{ route('admin.inscricao', [$inscricao->id, 'liberacoes']) }}" @if($secao == 'liberacoes') class="active" @endif>LIBERAÇÕES NO PRAZO</a>
        </div>
        <div class="secao">
            @include('admin._'.$secao)
        </div>

        @if($inscricao->status == 'valido' || $inscricao->status == 'invalido')
            <div class="btn-reavaliar"><a href="#">REAVALIAR</a></div>
            <div class="admin-inscricao-botoes" style="display:none">
                <a href="{{ route('admin.inscricao.validar', $inscricao->id) }}" class="validar">VALIDAR INSCRIÇÃO</a>
                <a href="{{ route('admin.inscricao.invalidar', $inscricao->id) }}" class="invalidar">INVALIDAR INSCRIÇÃO</a>
                @if($possuiSubstituicoes)
                <a href="{{ route('admin.inscricao.solicitarSubstituicoes', $inscricao->id) }}" class="solicitar-substituicoes">SOLICITAR SUBSTITUIÇÕES</a>
                @else
                <div class="solicitar-substituicoes disabled">SOLICITAR SUBSTITUIÇÕES</div>
                @endif
            </div>
        @else
        <div class="admin-inscricao-botoes">
            <a href="{{ route('admin.inscricao.validar', $inscricao->id) }}" class="validar">VALIDAR INSCRIÇÃO</a>
            <a href="{{ route('admin.inscricao.invalidar', $inscricao->id) }}" class="invalidar">INVALIDAR INSCRIÇÃO</a>
            @if($possuiSubstituicoes)
            <a href="{{ route('admin.inscricao.solicitarSubstituicoes', $inscricao->id) }}" class="solicitar-substituicoes">SOLICITAR SUBSTITUIÇÕES</a>
            @else
            <div class="solicitar-substituicoes disabled">SOLICITAR SUBSTITUIÇÕES</div>
            @endif
        </div>
        @endif
    </div>

@endsection

<table class="table-campos title-small">
    <tr>
        <td>status do pagamento:</td>
        <td>
            @if($inscricao->cpf && $inscricao->verificaPagamento())
            PAGO
            @else
            NÃO PAGO
            @endif
        </td>
    </tr>
</table>

@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(session('success'))
        <div class="success">{{ session('success') }}</div>
        @endif

        @if($total)
        <h2>{{ $total }} {{ $total > 1 ? 'recursos' : 'recurso' }} enviados</h2>

        <form action="{{ route('admin.recursos') }}" method="GET" class="form-filtro">
            <input type="text" name="nome_recursos" placeholder="filtrar recursos por nome" value="{{ request()->get('nome_recursos') }}">
            <input type="submit" value="FILTRAR">
        </form>
        @endif

        @if(count($recursos))
        <table class="table-listagem">
            <thead>
                <th></th>
                <th>inscrição:</th>
                <th>protocolo:</th>
                <th>data envio:</th>
                {{-- <th>prova/questão:</th> --}}
                <th>questão matriz:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($recursos as $recurso)
                <tr class="table-link" href="{{ route('admin.recursos.show', $recurso) }}">
                    <td>{{ $recurso->cadastro->nome }}</td>
                    <td class="inscricao">{{ $recurso->cadastro->id }}</td>
                    <td class="inscricao">{{ $recurso->protocolo }}</td>
                    <td class="data">{{ Tools::formataDataHorario($recurso->data_envio) }}</td>
                    {{-- <td class="dados">{{ $recurso->prova }}-{{ $recurso->questao }}</td> --}}
                    <td class="dados">{{ $recurso->questao_bloco }}</td>
                    <td class="table-btn">VISUALIZAR RECURSO</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @elseif($total)
        <div class="warning">Nenhum recurso encontrado</div>
        @endif

        @if($total)
        <div class="admin-inscricao-botoes">
            <a href="{{ route('admin.recursos.extrair') }}" class="principal">EXTRAIR RELATÓRIO ORDENADO</a>
        </div>

        <div class="divider"></div>
        <div class="recursos-show"><h3>ADICIONAR RESPOSTAS</h3></div>
        <form action="{{ route('admin.recursos.respostas') }}" method="POST">
            {!! csrf_field() !!}
            <table class="table-listagem">
                <thead>
                    <th><small style="white-space:nowrap">questão matriz:</small></th>
                    <th style="width:100%"><small>resposta do recurso da questão:</small></th>
                </thead>
                <tbody>
                    @foreach($respostas as $resposta)
                    <tr>
                        <td style="text-align:center;white-space:nowrap">{{ $resposta->questao_bloco }}</td>
                        <td class="input">
                            <input type="text" name="respostas[{{ $resposta->id }}]" value="{{ $resposta->resposta }}">
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="admin-inscricao-botoes">
                <input type="submit" value="SALVAR RESPOSTAS" class="principal">
            </div>
        </form>

        <div class="divider"></div>
        <div class="recursos-show">
            <h3>ADICIONAR DIZERES PADRÃO AOS RECURSOS</h3>

            <form action="{{ route('admin.recursos.respostaGeral') }}" class="form-resposta-geral" method="POST">
                {!! csrf_field() !!}
                <textarea name="resposta_geral">{{ $respostaGeral->resposta_geral }}</textarea>
                <div class="admin-inscricao-botoes">
                    <input type="submit" value="SALVAR DIZERES PADRÃO" class="principal">
                    @if(! $respostaGeral->ativo)
                        <a href="{{ route('admin.recursos.exibir', 1) }}" class="validar">FINALIZAR E EXIBIR RESPOSTAS</a>
                    @else
                        <a href="{{ route('admin.recursos.exibir', 0) }}" class="invalidar">PARAR EXIBIÇÃO DE RESPOSTAS</a>
                    @endif
                </div>
                <div style="text-align:center;margin-top:30px">
                    <a href="{{ route('admin.recursos.extrairEmails') }}">
                        <h2>Extrair lista de e-mails</h2>
                    </a>
                </div>
            </form>
        </div>
        @else
        <div class="warning">Nenhum recurso enviado</div>
        @endif
    </div>

@endsection

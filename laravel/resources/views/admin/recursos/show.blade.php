@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(session('success'))
        <div class="success">{{ session('success') }}</div>
        @endif

        <div class="recursos-show">
            <h3>VISUALIZAÇÃO DO RECURSO</h3>
            <p>
                Nome:
                <strong>{{ $recurso->cadastro->nome }}</strong>
                <br>
                Inscrição:
                <strong>{{ $recurso->cadastro->id }}</strong>
                <br>
                Protocolo:
                <strong>{{ $recurso->protocolo }}</strong>
                <br>
                Enviado em:
                <strong>{{ Tools::formataDataHorario($recurso->data_envio) }}</strong>
                <br>
                Recurso impetrado para a questão <strong>{{ $recurso->questao_bloco }}</strong> da prova <strong>{{ $recurso->prova }}</strong>
            </p>
            <p>
                Justificativa:<br>
                {!! nl2br($recurso->justificativa) !!}
            </p>
            <p>
                Bibliografia:<br>
                {!! nl2br($recurso->bibliografia) !!}
            </p>

            <form action="{{ route('admin.recursos.alterar', $recurso) }}" method="POST" class="form-recurso-envio">
                {!! csrf_field() !!}
                @foreach([
                    'documento_do_recurso' => 'Documento do Recurso com assinatura e firma reconhecida:',
                    'comprovante_de_deposito' => 'Comprovante de depósito da taxa para enviar recurso:'
                    ] as $input => $label)
                <div class="recurso-alteracao documentacao">
                    <label>{{ $label }}</label>
                    <a href="{{ asset('comprovantes/'.$recurso->{$input}) }}" target="_blank">VISUALIZAR COMPROVANTE</a>
                    <div class="btn-adicionar-comprovante">
                        <span></span>
                        Substituir comprovante
                        <input type="file" name="arquivo" class="fileupload one-file-hide" data-url="{{ route('admin.recursos.uploadComprovante', $input) }}">
                    </div>
                    <div class="files-wrapper"></div>
                </div>
                @endforeach
                <input type="submit" value="SALVAR ALTERAÇÃO">
            </form>

            <div class="divider"></div>
            <div class="admin-inscricao-botoes">
                <a href="{{ route('admin.recursos') }}" class="secundario">VOLTAR</a>
            </div>
        </div>
    </div>

@endsection

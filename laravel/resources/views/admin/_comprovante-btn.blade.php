@if(isset($total) && $total == 30 && !$status)
    <a href="{{ $link }}" class="btn-admin-comprovante --nao-avaliado">
        NÃO AVALIADO. PONTUAÇÃO MÁXIMA JÁ OBTIDA
    </a>
@else
    @if(isset($displayDiv))
        <div class="btn-admin-comprovante @if($status) --{{ $status }} @endif">
            {{ $label ?: 'VER COMPROVANTE '}}
        </div>
    @else
        <a href="{{ $link }}" class="btn-admin-comprovante @if($status) --{{ $status }} @endif">
            {{ $label ?: 'VER COMPROVANTE '}}
        </a>
    @endif
@endif

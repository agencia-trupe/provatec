@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(session('success'))
        <div class="success">{{ session('success') }}</div>
        @endif

        <form action="{{ route('admin.excecoes.adicionar') }}" method="POST" class="form-excecao">
            {!! csrf_field() !!}
            <p>Adicione cadastros que podem enviar sua inscrição mesmo após o término do período de inscrições.</p>

            @if($errors->any() || session('erro'))
            <p class="erro">Inscrição não encontrada.</p>
            @endif

            <input type="text" name="inscricao" placeholder="número de inscrição" required>
            <input type="submit" value="ADICIONAR">
        </form>

        @if(count($excecoes))
        <h2>{{ $excecoes->count() }} {{ $excecoes->count() > 1 ? 'exceções liberadas' : 'exceção liberada' }}</h2>

        <table class="table-listagem">
            <thead>
                <th></th>
                <th>inscrição:</th>
                <th>adicionado em:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($excecoes as $excecao)
                <tr class="table-link" href="{{ route('admin.excecoes.excluir', $excecao->id) }}">
                    <td class="inscricao">{{ $excecao->cadastro->id }}</td>
                    <td>{{ $excecao->cadastro->nome }}</td>
                    <td class="data">{{ Tools::formataDataHorario($excecao->created_at) }}</td>
                    <td class="table-btn table-btn-excluir">EXCLUIR</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="warning">Nenhuma exceção encontrada</div>
        @endif
    </div>

@endsection

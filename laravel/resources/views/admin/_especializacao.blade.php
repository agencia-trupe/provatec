<table class="table-campos">
    @foreach(App\Models\Campos::especializacaoEmCardiologia() as $campo => $dados)
        <tr>
            <td>{!! $dados['label'] !!}:</td>
            <td>
                @if($inscricao->getComprovante($campo) && count($inscricao->getComprovante($campo)->arquivos))
                    {{ $inscricao->getComprovante($campo)->uf }} |
                    {{ $inscricao->getComprovante($campo)->ano }}
                    @include('admin._comprovante-btn', [
                        'link'   => route('admin.comprovante', [
                            $inscricao->id,
                            $campo
                        ]),
                        'status' => $inscricao->getComprovanteStatus($campo),
                        'label'  => $inscricao->getComprovanteStatusLabel($campo),
                        'total'  => $inscricao->pontuacao_total
                    ])
                @else
                    -
                @endif
            </td>
        </tr>
    @endforeach
</table>

@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(count($inscricoes))
        <h2>{{ $inscricoes->total() }} {{ $inscricoes->total() > 1 ? 'inscrições' : 'inscrição' }} com esse status</h2>

        <div class="admin-inscricao-botoes" style="text-align:left;margin:0 0 20px">
            <a href="{{ route('admin.inscricoesValidadas.extrair') }}" class="principal" style="margin:0">EXTRAIR INSCRIÇÕES VALIDADAS</a>
        </div>

        <table class="table-listagem">
            <thead>
                <th></th>
                <th>inscrição:</th>
                <th>recebido em:</th>
                <th>validado em:</th>
                <th>por:</th>
                <th>pontuação obtida:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($inscricoes as $inscricao)
                <tr class="table-link" href="{{ route('admin.inscricao', $inscricao->id) }}" @if($inscricao->revisao_envio && !$inscricao->revisao_conclusao) style="background:#FFF7CC !important" @endif>
                    <td>{{ $inscricao->nome }}</td>
                    <td class="inscricao">{{ $inscricao->id }}</td>
                    <td class="data">{{ Tools::formataDataHorario($inscricao->data_envio_documentacao) }}</td>
                    <td class="data">{{ Tools::formataDataHorario($inscricao->status_data) }}</td>
                    <td class="dados">{{ $inscricao->status_admin }}</td>
                    <td class="dados"><strong>{{ (float)$inscricao->pontuacao_total }}</strong></td>
                    <td class="table-btn">VISUALIZAR</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="warning">Nenhuma inscrição encontrada</div>
        @endif

        {!! $inscricoes->render() !!}
    </div>

@endsection

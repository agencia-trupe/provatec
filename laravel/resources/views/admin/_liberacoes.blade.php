<div class="admin-liberacoes-inscricao">
    <p>
        ENVIO INICIAL EM:
        {{ Tools::formataDataHorario($inscricao->data_primeiro_envio) }}
    </p>

    <form action="{{ route('admin.inscricao.liberar', $inscricao->id) }}" method="POST">
        {!! csrf_field() !!}
        <p>Realizar liberação para novo envio de documentos na data de hoje</p>
        <textarea name="observacoes" placeholder="observações"></textarea>
        <input type="submit" value="LIBERAR">
    </form>

    @if(count($inscricao->historicoLiberacoes))
    <div class="box-liberacoes">
        <h3>HISTÓRICO DE LIBERAÇÕES PARA ESTE CANDIDATO</h3>
        @foreach($inscricao->historicoLiberacoes as $historico)
        <div class="row">
            @if($historico->admin)
                <p>
                    LIBERADO EM:
                    {{ Tools::formataDataHorario($historico->created_at) }}<br>
                    <small>
                        por:
                        {{ explode(' ', $historico->admin->nome)[0] }}
                    </small>
                </p>
                @if($historico->observacoes)
                <p>
                    <strong>observações:</strong>
                    {{ $historico->observacoes }}
                </p>
                @endif
            @else
                <p style="font-weight:bold">
                    REENVIADO EM:
                    {{ Tools::formataDataHorario($historico->created_at) }}<br>
                    <small>
                        por:
                        {{ explode(' ', $historico->cadastro->nome)[0] }}
                    </small>
                </p>
            @endif
        </div>
        @endforeach
    </div>
    @endif
</div>

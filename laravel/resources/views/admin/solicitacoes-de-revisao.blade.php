@extends('frontend.common.template')

@section('content')

    <div class="main-admin center">
        @include('admin._nav')

        @if(count($inscricoes))
        <h2>{{ $inscricoes->total() }} {{ $inscricoes->total() > 1 ? 'inscrições' : 'inscrição' }} com esse status</h2>

        <table class="table-listagem">
            <thead>
                <th></th>
                <th>inscrição:</th>
                <th>validado em:</th>
                <th>por:</th>
                <th>pontuação obtida:</th>
                <th>solicitação enviada em:</th>
                <th>solicitação concluída em:</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($inscricoes as $inscricao)
                <tr class="table-link" href="{{ route('admin.inscricao', $inscricao->id) }}">
                    <td>{{ $inscricao->nome }}</td>
                    <td class="inscricao">{{ $inscricao->id }}</td>
                    <td class="data">{{ Tools::formataDataHorario($inscricao->status_data) }}</td>
                    <td class="dados">{{ $inscricao->status_admin }}</td>
                    <td class="dados"><strong>{{ (float)$inscricao->pontuacao_total }}</strong></td>
                    <td class="data">{{ Tools::formataData($inscricao->revisao_envio) }}</td>
                    <td class="data">{{ $inscricao->revisao_conclusao ?Tools::formataData($inscricao->revisao_conclusao) : 'em aberto' }}</td>
                    <td class="table-btn">VISUALIZAR</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="warning">Nenhuma inscrição encontrada</div>
        @endif

        {!! $inscricoes->render() !!}
    </div>

@endsection

<table class="table-campos">
    @foreach(App\Models\Campos::documentosFacultativos()['experiencia'] as $campo => $dados)
        <tr>
            <td>{{ $dados['label'] }}:</td>
            <td>
                @if($dados['limite_grupo'] == 0)
                    @if($inscricao->getComprovante($campo))
                    {{ $inscricao->getComprovante($campo)->ano }}
                    @include('admin._comprovante-btn', [
                        'link'   => route('admin.comprovante', [
                            $inscricao->id,
                            $campo
                        ]),
                        'status' => $inscricao->getComprovanteStatus($campo),
                        'label'  => $inscricao->getComprovanteStatusLabel($campo),
                        'total'  => $inscricao->pontuacao_total
                    ])
                    <div style="margin-top:4px">
                        <span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:.9em">
                            TOTAL = {{ $inscricao->pontuacaoDocumentoFacultativo($campo) }}
                        </span>
                    </div>
                    @else
                    -
                    @endif
                @elseif($dados['limite_grupo'] == 1)
                    @if($inscricao->getComprovante($campo) && count($inscricao->getComprovante($campo)->arquivos))
                        {{ $inscricao->getComprovante($campo)->ano }}
                        @include('admin._comprovante-btn', [
                            'link'   => route('admin.comprovante', [
                                $inscricao->id,
                                $campo
                            ]),
                            'status' => $inscricao->getComprovanteStatus($campo),
                            'label'  => $inscricao->getComprovanteStatusLabel($campo),
                            'total'  => $inscricao->pontuacao_total
                        ])
                    @else
                        -
                    @endif
                @else

                @endif
            </td>
        </tr>
    @endforeach

    <tr><td colspan="2"><div class="divider"></div></td></tr>

    @foreach(App\Models\Campos::documentosFacultativos()['atualizacao'] as $campo => $dados)
        <tr>
            <td>{!! $dados['label'] !!}:</td>
            <td>
                @if($dados['limite_grupo'] == 0)
                    @if($inscricao->getComprovante($campo))
                    {{ $inscricao->getComprovante($campo)->ano }}
                    @include('admin._comprovante-btn', [
                        'link'   => route('admin.comprovante', [
                            $inscricao->id,
                            $campo
                        ]),
                        'status' => $inscricao->getComprovanteStatus($campo),
                        'label'  => $inscricao->getComprovanteStatusLabel($campo),
                        'total'  => $inscricao->pontuacao_total
                    ])
                    <div style="margin-top:4px">
                        <span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:.9em">
                            TOTAL = {{ $inscricao->pontuacaoDocumentoFacultativo($campo) }}
                        </span>
                    </div>
                    @else
                    -
                    @endif
                @elseif($dados['limite_grupo'] == 1)
                    @if($inscricao->getComprovante($campo) && count($inscricao->getComprovante($campo)->arquivos))
                        {{ $inscricao->getComprovante($campo)->ano }}
                        @include('admin._comprovante-btn', [
                            'link'   => route('admin.comprovante', [
                                $inscricao->id,
                                $campo
                            ]),
                            'status' => $inscricao->getComprovanteStatus($campo),
                            'label'  => $inscricao->getComprovanteStatusLabel($campo),
                            'total'  => $inscricao->pontuacao_total
                        ])
                        <div style="margin-top:4px">
                            <span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:.9em">
                                TOTAL = {{ $inscricao->pontuacaoDocumentoFacultativo($campo) }}
                            </span>
                        </div>
                    @else
                        -
                    @endif
                @else
                    @if($inscricao->getComprovante($campo))
                        @foreach($inscricao->getComprovante($campo) as $key => $comprovante)
                            <div class="comprovante-row">
                                {{ $comprovante['ano'] }}
                                @include('admin._comprovante-btn', [
                                    'link'   => route('admin.comprovante', [
                                        $inscricao->id,
                                        $campo,
                                        $key
                                    ]),
                                    'status' => $inscricao->getComprovanteStatus($campo, $key),
                                    'label'  => $inscricao->getComprovanteStatusLabel($campo, $key),
                                    'total'  => $inscricao->pontuacao_total
                                ])
                            </div>
                        @endforeach
                        <div style="margin-top:4px">
                            <span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:.9em">
                                TOTAL = {{ $inscricao->pontuacaoDocumentoFacultativo($campo) }}
                            </span>
                        </div>
                    @else
                        -
                    @endif
                @endif
            </td>
        </tr>
        @if($campo == 'doutorado_livre_docencia_cardiologia')
            <tr><td>
                <span style="font-weight:700;font-size:1.1em">
                    TOTAL B1 = {{ $inscricao->pontuacaoSecoesDocumentosFacultativos()['b1'] }}
                </span>
            </td></tr>
            <tr><td colspan="2"><div class="divider"></div></td></tr>
        @endif
        @if($campo == 'curso_online_captec')
            <tr><td>
                <span style="font-weight:700;font-size:1.1em">
                    TOTAL B2 = {{ $inscricao->pontuacaoSecoesDocumentosFacultativos()['b2'] }}
                </span>
            </td></tr>
            <tr><td colspan="2"><div class="divider"></div></td></tr>
        @endif
        @if($campo == 'participacao_registros_brasileiros')
            <tr><td>
                <span style="font-weight:700;font-size:1.1em">
                    TOTAL B3 = {{ $inscricao->pontuacaoSecoesDocumentosFacultativos()['b3'] }}
                </span>
            </td></tr>
        @endif
    @endforeach
</table>

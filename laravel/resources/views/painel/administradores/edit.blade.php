@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Administrador</h2>
    </legend>

    {!! Form::model($usuario, [
        'route' => ['painel.administradores.update', $usuario->id],
        'method' => 'patch'])
    !!}

    @include('painel.administradores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection

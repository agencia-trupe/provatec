@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Administrador</h2>
    </legend>

    {!! Form::open(['route' => 'painel.administradores.store']) !!}

        @include('painel.administradores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection

@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('senha', 'Senha') !!}
    {!! Form::password('senha', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('senha_confirmation', 'Confirmação de Senha') !!}
    {!! Form::password('senha_confirmation', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.administradores.index') }}" class="btn btn-default btn-voltar">Voltar</a>

<nav class="documentacao-nav center">
    <a href="{{ route('documentacao.documentacaoObrigatoria') }}" @if(Tools::isActive('documentacao.documentacaoObrigatoria')) class="active" @endif>
        <span>1</span>
        <h3>Documentação Obrigatória / Graduação</h3>
        <p>- {{ \Auth::guard('cadastro')->user()->documentacao_obrigatoria_status }} -</p>
    </a>
    <a href="{{ route('documentacao.especializacaoEmCardiologia') }}" @if(Tools::isActive('documentacao.especializacaoEmCardiologia')) class="active" @endif>
        <span>2</span>
        <h3>Documentação Obrigatória / Clínica Médica e Cardiologia</h3>
        <p>- {{ \Auth::guard('cadastro')->user()->especializacao_cardiologia_status }} -</p>
    </a>
    <a href="{{ route('documentacao.documentosFacultativos') }}" @if(Tools::isActive('documentacao.documentosFacultativos')) class="active" @endif>
        <span>3</span>
        <h3>Documentos Facultativos / Quesito Atualização e Experiência Profissional</h3>
        <p>- {{ \Auth::guard('cadastro')->user()->documentos_facultativos_status }} -</p>
    </a>
    <a href="{{ route('documentacao.declaracoesFinais') }}" @if(Tools::isActive('documentacao.declaracoesFinais')) class="active" @endif>
        <span>4</span>
        <h3>Declarações Finais</h3>
        <p>- {{ \Auth::guard('cadastro')->user()->declaracoes_finais_status }} -</p>
    </a>
</nav>

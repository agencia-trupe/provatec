<nav class="documentacao-nav pendencias-nav">
    <a href="{{ route('documentacao.pendencias.documentacaoObrigatoria') }}" class="@if($active == 'documentacao-obrigatoria') active @endif @if(!Tools::verificaPendencias($pendencias, 'documentacao-obrigatoria')) disabled @endif">
        <span>1</span>
        <h3>Documentação Obrigatória</h3>
        <p>- {{ Tools::verificaPendencias($pendencias, 'documentacao-obrigatoria') ? 'com pendências' : 'VALIDADO' }} -</p>
    </a>
    <a href="{{ route('documentacao.pendencias.especializacaoEmCardiologia') }}" class="@if($active == 'especializacao-em-cardiologia') active @endif @if(!Tools::verificaPendencias($pendencias, 'especializacao-em-cardiologia')) disabled @endif">
        <span>2</span>
        <h3>Clínica Médica e Cardiologia</h3>
        <p>- {{ Tools::verificaPendencias($pendencias, 'especializacao-em-cardiologia') ? 'com pendências' : 'VALIDADO' }} -</p>
    </a>
    <a href="{{ route('documentacao.pendencias.documentosFacultativos') }}" class="@if($active == 'documentos-facultativos') active @endif @if(!Tools::verificaPendencias($pendencias, 'documentos-facultativos')) disabled @endif">
        <span>3</span>
        <h3>Documentos Facultativos</h3>
        <p>- {{ Tools::verificaPendencias($pendencias, 'documentos-facultativos') ? 'com pendências' : 'VALIDADO' }} -</p>
    </a>
    <a href="{{ route('documentacao.pendencias.declaracoesFinais') }}" class="@if($active == 'declaracoes-finais') active @endif @if(!Tools::verificaPendencias($pendencias, 'declaracoes-finais')) disabled @endif">
        <span>4</span>
        <h3>Declarações Finais</h3>
        <p>- {{ Tools::verificaPendencias($pendencias, 'declaracoes-finais') ? 'com pendências' : 'VALIDADO' }} -</p>
    </a>
</nav>

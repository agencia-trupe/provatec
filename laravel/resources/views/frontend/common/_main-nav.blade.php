@if(env('PROVA_REALIZADA'))
    <div class="main-nav center">
        <a href="{{ route('dadosPessoais') }}" @if(Route::currentRouteName() == 'dadosPessoais') class="active" @endif>Dados Pessoais</a>
        <a href="{{ route('informacoes') }}" @if(Route::currentRouteName() == 'informacoes') class="active" @endif>Informações sobre a prova, gabarito, cadernos, recursos</a>
    </div>
@else
    @if(Auth::guard('cadastro')->user()->status == 'solicitado')
        <div class="center" style="margin-bottom:20px">
            <div class="warning">Você possui pendências na sua inscrição. Vá para a página de documentação para reenviar os comprovantes.</div>
        </div>
    @endif
    @if(Tools::inscricoesEncerradas() && Auth::guard('cadastro')->user()->status == 'novo')
        <div class="center" style="margin-bottom:20px">
            <div class="warning warning-expirado">Inscrições encerradas. Você não pode mais enviar sua inscrição.</div>
        </div>
    @endif
    <nav class="main-nav center">
        <a href="{{ route('dadosPessoais') }}" @if(Route::currentRouteName() == 'dadosPessoais') class="active" @endif>Dados Pessoais</a>
        <a href="{{ route('documentacao') }}" @if(Tools::isActive('documentacao*')) class="active" @endif>Documentação</a>
        @if(Auth::guard('cadastro')->user()->status == 'novo' || Auth::guard('cadastro')->user()->status == 'solicitado')
        <a href="{{ route('envio') }}" @if(Route::currentRouteName() == 'envio') class="active" @endif>Envio</a>
        @endif
        @if(Auth::guard('cadastro')->user()->status == 'valido')
            <a href="{{ route('pagamento') }}" @if(Route::currentRouteName() == 'pagamento') class="active" @endif>Pagamento</a>
        @endif
        @if(Auth::guard('cadastro')->user()->data_envio_documentacao && Auth::guard('cadastro')->user()->status == 'valido')
            <a href="{{ route('sobreAProva') }}" @if(Route::currentRouteName() == 'sobreAProva') class="active" @endif>Sobre a Prova</a>
        @elseif(Auth::guard('cadastro')->user()->status != 'invalido')
            <a href="{{ route('sobreAProva') }}" @if(Route::currentRouteName() == 'sobreAProva') class="active" @endif>Sobre a Prova e Regulamento</a>
        @endif
    </nav>
@endif

    <header @if(Route::currentRouteName() === 'home' || Route::currentRouteName() === 'aguarde') class="header-home" @endif @if(str_is('admin*', Route::currentRouteName())) class="header-admin" @endif>
        <div class="center">
            <h1>
                Prova de Título de<br>
                Especialista em Cardiologia
                <small>20/12/2020</small>
            </h1>
            <h2>2020</h2>
            <div class="sbc"></div>
            <div class="detalhe"></div>

            @if(Auth::guard('admin')->check() && str_is('admin*', Route::currentRouteName()))
            <div class="logout-admin">
                Olá {{ explode(' ', Auth::guard('admin')->user()->nome)[0] }}.
                <a href="{{ route('admin.logout') }}">[sair]</a>
            </div>
            @endif
        </div>
    </header>

    @if(Auth::guard('cadastro')->check() && !str_is('admin*', Route::currentRouteName()))
        <div class="logout center">
            <p>
                @if(Auth::guard('cadastro')->user()->nome)
                Olá {{ explode(' ', Auth::guard('cadastro')->user()->nome)[0] }}.
                @else
                Olá.
                @endif
                <a href="{{ route('cadastro.logout') }}">[sair]</a>
                <br>
                Seu número de inscrição é: <strong>{{ Auth::guard('cadastro')->user()->id }}</strong>
            </p>
        </div>

        <div class="center">

            <div class="erros-documentacao" style="color: #1a345d !important;margin-bottom:40px">
                <p>
                    Caro candidato,
                </p>
                <p>
                    Durante a aplicação da prova, as questões com suas respectivas alternativas foram randomizadas em cada bloco. O seu gabarito individual foi enviado ontem, por e-mail, ao final da prova.
                </p>
                <p>
                    A Pontuação do Candidato, o Gabarito Oficial da prova matriz e o caderno da prova matriz foram disponibilizados hoje, no período da tarde, na página do candidato <a href="https://www.provatec.com.br">www.provatec.com.br</a>.
                </p>
                <p>
                    O gabarito e o caderno referente à prova matriz foram divididos por blocos com questões de 1 a 50, conforme modelo de prova aplicado, para facilitar a conferência.
                </p>
            </div>
        </div>

        @if(auth('cadastro')->user()->status == 'valido' && auth('cadastro')->user()->verificaPagamento() && !env('PROVA_REALIZADA') && env('CARTA_DE_CONFIRMACAO'))
            <div class="center">
                <a href="{{ route('cartaDeConfirmacao') }}" target="_blank" class="btn-carta-confirmacao">IMPRIMIR CARTA DE CONFIRMAÇÃO &raquo;</a>
            </div>
        @endif
    @endif

@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    @if(session('erro'))
    <div class="erros-documentacao center">
        {{ session('erro') }}
    </div>
    @endif

    <div class="dados-pessoais interna center">
        <div class="main-box">
            <h2>Dados Pessoais</h2>

            <form action="{{ route('dadosPessoais.post') }}" method="POST">
                {!! csrf_field() !!}

                @if(\Auth::guard('cadastro')->user()->data_envio_documentacao)
                    <div class="row">
                        <p>
                            nome:<br>
                            {{ $cadastro->nome }}
                        </p>
                        <p>
                            e-mail:<br>
                            {{ $cadastro->email }}
                        </p>
                        <p>
                            nome para o certificado:<br>
                            {{ $cadastro->nome_certificado }}
                        </p>
                        <p>
                            CPF:<br>
                            {{ $cadastro->cpf }}
                        </p>
                        @if($cadastro->getComprovante('crm'))
                        <p>
                            CRM:
                            @foreach($cadastro->getComprovante('crm') as $key => $value)
                            <br>{{ $cadastro->getComprovante('crm')[$key]['numero'] }}
                            @endforeach
                        </p>
                        @endif
                    </div>
                    <div class="row">
                        <input type="text" name="CEP" placeholder="CEP" class="mask-cep @if($enviado && !$cadastro->cep) erro-validacao @endif" value="{{ $cadastro->cep }}">
                    </div>
                    <div class="row">
                        <input type="text" name="endereco" placeholder="Endereço completo" value="{{ $cadastro->endereco }}" @if($enviado && !$cadastro->endereco) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="numero" placeholder="Número" value="{{ $cadastro->numero }}" @if($enviado && !$cadastro->numero) class="erro-validacao" @endif>
                        <input type="text" name="complemento" placeholder="Complemento" value="{{ $cadastro->complemento }}">
                        <input type="text" name="bairro" placeholder="Bairro" value="{{ $cadastro->bairro }}" @if($enviado && !$cadastro->bairro) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="cidade" placeholder="Cidade" value="{{ $cadastro->cidade }}" @if($enviado && !$cadastro->cidade) class="erro-validacao" @endif>
                        <input type="text" name="uf" placeholder="UF" value="{{ $cadastro->uf }}" maxlength="2" @if($enviado && !$cadastro->uf) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="telefone_residencial" placeholder="Telefone residencial" class="mask-telefone" value="{{ $cadastro->telefone_residencial }}">
                        <input type="text" name="telefone_comercial" placeholder="Telefone comercial" class="mask-telefone" value="{{ $cadastro->telefone_comercial }}">
                    </div>
                    <div class="row">
                        <input type="text" name="telefone_celular" placeholder="Telefone celular" class="mask-telefone @if($enviado && !$cadastro->telefone_celular) erro-validacao @endif" value="{{ $cadastro->telefone_celular }}" >
                        <input type="text" name="nascimento" placeholder="Data de nascimento" class="mask-nascimento @if($enviado && !$cadastro->nascimento) erro-validacao @endif" value="{{ $cadastro->nascimento }}">
                    </div>
                    <div class="row">
                        <p>
                            sexo:<br>
                            {{ $cadastro->sexo }}
                        </p>
                        <p>
                            lateralidade:<br>
                            {{ $cadastro->lateralidade }}
                        </p>
                        <p>
                            sócio AMB:<br>
                            {{ $cadastro->getComprovante('socio_amb')->select }}
                        </p>
                    </div>
                @else
                    <div class="row">
                        <input type="text" name="nome" placeholder="Nome completo" value="{{ $cadastro->nome }}" @if($enviado && !$cadastro->nome) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="nome_certificado" placeholder="Nome para o Certificado" value="{{ $cadastro->nome_certificado }}" @if($enviado && !$cadastro->nome_certificado) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="CPF" placeholder="CPF" class="mask-cpf @if($enviado && !$cadastro->cpf) erro-validacao @endif" value="{{ $cadastro->cpf }}">
                    </div>
                    <div class="row">
                        @if(!$cadastro->getComprovante('crm'))
                            <input type="hidden" name="crm_grupo[]" value="1">
                            <input type="text" name="crm_numero_1" placeholder="CRM" @if($enviado) class="erro-validacao" @endif>
                            <div class="btn-adicionar-comprovante">
                                <span></span>
                                Adicionar comprovante
                                <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('uploadComprovanteDadosPessoais', ['crm', 1]) }}">
                            </div>
                            <div class="files-wrapper"></div>
                        @else
                            @foreach($cadastro->getComprovante('crm') as $key => $value)
                                <input type="hidden" name="crm_grupo[]" value="{{ $key }}">
                                <input type="text" name="crm_numero_{{ $key }}" value="{{ $cadastro->getComprovante('crm')[$key]['numero'] }}" placeholder="CRM" @if($enviado && (!$cadastro->getComprovante('crm')[$key]['numero'] || !$cadastro->getComprovante('crm')[$key]['arquivos'])) class="erro-validacao" @endif>
                                <div class="btn-adicionar-comprovante">
                                    <span></span>
                                    Adicionar comprovante
                                    <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('uploadComprovanteDadosPessoais', ['crm', $key]) }}">
                                </div>
                                <div class="files-wrapper">
                                    @if(count($cadastro->getComprovante('crm')[$key]['arquivos']))
                                        @foreach($cadastro->getComprovante('crm')[$key]['arquivos'] as $arquivo)
                                            <span>
                                                <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                                <input type="hidden" name="crm_arquivos_{{ $key }}[]" value="{{ $arquivo }}">
                                                <a href="#" class="excluir">X</a>
                                            </span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <a href="#" class="btn-add-row" data-input="crm" data-url="{{ route('uploadComprovanteDadosPessoais', 'crm') }}">
                            <span></span>
                            Incluir outro CRM
                        </a>
                    </div>
                    <div class="row">
                        <input type="text" name="CEP" placeholder="CEP" class="mask-cep @if($enviado && !$cadastro->cep) erro-validacao @endif" value="{{ $cadastro->cep }}">
                    </div>
                    <div class="row">
                        <input type="text" name="endereco" placeholder="Endereço completo" value="{{ $cadastro->endereco }}" @if($enviado && !$cadastro->endereco) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="numero" placeholder="Número" value="{{ $cadastro->numero }}" @if($enviado && !$cadastro->numero) class="erro-validacao" @endif>
                        <input type="text" name="complemento" placeholder="Complemento" value="{{ $cadastro->complemento }}">
                        <input type="text" name="bairro" placeholder="Bairro" value="{{ $cadastro->bairro }}" @if($enviado && !$cadastro->bairro) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="cidade" placeholder="Cidade" value="{{ $cadastro->cidade }}" @if($enviado && !$cadastro->cidade) class="erro-validacao" @endif>
                        <input type="text" name="uf" placeholder="UF" value="{{ $cadastro->uf }}" maxlength="2" @if($enviado && !$cadastro->uf) class="erro-validacao" @endif>
                    </div>
                    <div class="row">
                        <input type="text" name="telefone_residencial" placeholder="Telefone residencial" class="mask-telefone" value="{{ $cadastro->telefone_residencial }}">
                        <input type="text" name="telefone_comercial" placeholder="Telefone comercial" class="mask-telefone" value="{{ $cadastro->telefone_comercial }}">
                    </div>
                    <div class="row">
                        <input type="text" name="telefone_celular" placeholder="Telefone celular" class="mask-telefone @if($enviado && !$cadastro->telefone_celular) erro-validacao @endif" value="{{ $cadastro->telefone_celular }}" >
                        <input type="text" name="nascimento" placeholder="Data de nascimento" class="mask-nascimento @if($enviado && !$cadastro->nascimento) erro-validacao @endif" value="{{ $cadastro->nascimento }}">
                    </div>
                    <div class="row">
                        <select name="sexo" @if($enviado && !$cadastro->sexo) class="erro-validacao" @endif>
                            <option value="" @if(!$cadastro->sexo) selected @endif>Sexo (selecionar)</option>
                            <option value="Masculino" @if($cadastro->sexo == 'Masculino') selected @endif>Masculino</option>
                            <option value="Feminino" @if($cadastro->sexo == 'Feminino') selected @endif>Feminino</option>
                        </select>
                        <select name="lateralidade" @if($enviado && !$cadastro->lateralidade) class="erro-validacao" @endif>
                            <option value="" @if(!$cadastro->lateralidade) selected @endif>Destro ou canhoto (selecionar)</option>
                            <option value="Destro" @if($cadastro->lateralidade == 'Destro') selected @endif>Destro</option>
                            <option value="Canhoto" @if($cadastro->lateralidade == 'Canhoto') selected @endif>Canhoto</option>
                        </select>
                    </div>
                    <div class="row">
                        <select name="socio_amb_select" @if($enviado && (!$cadastro->getComprovante('socio_amb') || ($cadastro->getComprovante('socio_amb')->select == 'Sim' && !count($cadastro->getComprovante('socio_amb')->arquivos))))) class="erro-validacao" @endif>
                            <option value="" @if($cadastro->getComprovante('socio_amb') && !$cadastro->getComprovante('socio_amb')->select) selected @endif>Sócio da AMB?</option>
                            <option value="Sim" @if($cadastro->getComprovante('socio_amb') && $cadastro->getComprovante('socio_amb')->select == 'Sim') selected @endif>Sim</option>
                            <option value="Não" @if($cadastro->getComprovante('socio_amb') && $cadastro->getComprovante('socio_amb')->select == 'Não') selected @endif>Não</option>
                        </select>
                        <div class="btn-adicionar-comprovante">
                            <span></span>
                            Adicionar comprovante
                            <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('uploadComprovanteDadosPessoais', 'socio_amb') }}">
                        </div>
                        <div class="files-wrapper">
                            @if($cadastro->getComprovante('socio_amb') && count($cadastro->getComprovante('socio_amb')->arquivos))
                                @foreach($cadastro->getComprovante('socio_amb')->arquivos as $arquivo)
                                    <span>
                                        <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                        <input type="hidden" name="socio_amb_arquivos[]" value="{{ $arquivo }}">
                                        <a href="#" class="excluir">X</a>
                                    </span>
                                @endforeach
                            @endif
                        </div>
                        <div class="legenda">
                            <p>Para comprovação de Associado AMB é necessário anexar declaração de quitação em papel timbrado da AMB com data de emissão a partir de 2020 – não serão aceitos boletos para este fim de comprovação.</p>
                            <p>Os associados da SBC não precisam anexar comprovante de sócio da AMB. A verificação da associação será feita através do login que será utilizado diretamente na plataforma de pagamento da taxa de inscrição.</p>
                        </div>
                    </div>
                @endif

                @if($enviado)
                    <input type="submit" value="SALVAR ALTERAÇÕES">

                    <div class="observacoes">
                        Agora você deve prosseguir o preenchimento clicando no item DOCUMENTAÇÃO no submenu no topo.<br>
                        Você pode voltar e alterar ou atualizar os DADOS PESSOAIS a qualquer momento voltando nesta página antes de finalizar e enviar.
                    </div>
                @else
                    <input type="submit" value="CADASTRAR">
                @endif
            </form>
        </div>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    <div class="login center">
        <div class="login-texto">
            <p>Caro(a) candidato(a), bem-vindo(a) à nova plataforma de inscrições da Prova de Título de Especialista em Cardiologia 2020.</p>
            @if(Tools::inscricoesEncerradas())
            <h4>As inscrições estão encerradas.</h4>
            <p style="margin-top:1em">O acesso agora permite que você acompanhe o status da sua inscrições e instruções para a prova caso tenha feito o envio até a data limite de 5 de novembro de 2020. Lembre-se do e-mail cadastrado, ele é o seu login.</p>
            @else
            <p>Você pode parar e continuar a inclusão de documentos a qualquer momento, fazendo o envio quando tudo estiver completo. Lembre-se do e-mail cadastrado, ele é o seu login.</p>
            <h4>As inscrições serão encerradas às 23 horas e 59 minutos do dia 5 de Novembro de 2020 (horário de Brasília).</h4>
            @endif
        </div>

        @unless(Tools::inscricoesEncerradas())
        <div class="main-box half-width">
            <h2>Primeiro Acesso</h2>
            <form action="{{ route('cadastro.cadastroPost') }}" method="POST">
                @if($errors->any())
                    <div class="erro">
                        @foreach($errors->all() as $error)
                        {{ $error }}<br>
                        @endforeach
                    </div>
                @endif

                {!! csrf_field() !!}
                <input type="email" name="email_cadastro" value="{{ old('email_cadastro') }}" placeholder="e-mail" required>
                <input type="password" name="senha" placeholder="criar senha" required>
                <input type="password" name="senha_confirmation" placeholder="repetir senha" required>
                <input type="submit" value="ENTRAR">
            </form>
        </div>
        @endunless

        <div class="main-box @unless(Tools::inscricoesEncerradas()) half-width @endunless">
            <h2>Já Possuo Cadastro</h2>
            <form action="{{ route('cadastro.loginPost') }}" method="POST">
                @if(session('erro-login'))
                    <div class="erro">{{ session('erro-login') }}</div>
                @endif

                {!! csrf_field() !!}
                <input type="email" name="email" value="{{ old('email') }}" placeholder="login [e-mail]" required>
                <input type="password" name="senha" placeholder="senha" required>
                <input type="submit" value="ENTRAR">
                <a href="{{ route('cadastro.esqueci') }}" class="esqueci">esqueci minha senha &raquo;</a>
            </form>
        </div>
    </div>

@endsection

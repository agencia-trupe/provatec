@extends('frontend.common.template')

@section('content')

    <div class="login center">
        <div class="main-box">
            <h2>Esqueci Minha Senha</h2>

            @if(session('enviado'))
                <div class="enviado">
                    {{ session('enviado') }}
                </div>
            @else
                <form action="{{ route('cadastro.redefinicao') }}" method="POST">
                    @if($errors->any())
                        <div class="erro">
                            @foreach($errors->all() as $error)
                            {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    {!! csrf_field() !!}
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                    <input type="submit" value="REDEFINIR SENHA">
                </form>
            @endif
        </div>
    </div>

@endsection

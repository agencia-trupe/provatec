@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="status interna center">
        <div class="main-box">
            <h2>Pagamento</h2>

            @if($cadastro->cpf && $cadastro->verificaPagamento())
                <div class="status-box">
                    <p>status do pagamento:</p>
                    <h3 style="margin-bottom:18px"><strong>PAGO</strong></h3>
                    @if(Tools::voucherDisponivel())
                        <a href="{{ route('sobreAProva') }}" class="botao-comprovante-pagamento">
                            VER INFORMAÇÕES DA PROVA &raquo;
                        </a>
                    @else
                        <p>Seu número de inscrição é: {{ $cadastro->id }}. Aguarde instruções para a realização da prova em breve.</p>
                    @endif
                </div>
            @else
                <p>
                    Você realizou o envio da documentação em:
                    <span class="data">
                        {{ Tools::formataData($cadastro->data_envio_documentacao) }}
                        &middot; STATUS: Documentação Validada
                    </span>
                </p>
                <p>
                    Agora você deve fazer o pagamento da taxa de inscrição no sistema de pagamentos da SBC.<br>
                    Acesse o link abaixo e logue-se usando o e-mail <strong>@cardiol</strong> caso seja um associado SBC para garantir seu desconto no valor da inscrição:
                </p>

                <div style="text-align:center">
                    <a href="https://ecommerce.cardiol.br/Oferta_Venda_Categoria/Index/270" target="_blank" class="botao-comprovante-pagamento">PAGAMENTO DA TAXA DE INSCRIÇÃO &raquo;</a>
                </div>
            @endif
        </div>
    </div>

@endsection

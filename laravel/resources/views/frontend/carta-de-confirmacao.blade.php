<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
@if(!str_is('admin*', Route::currentRouteName()))
    <meta name="viewport" content="width=device-width, initial-scale=1">
@endif
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $config->description }}">
    <meta name="keywords" content="{{ $config->keywords }}">

    <meta property="og:title" content="{{ $config->title }}">
    <meta property="og:description" content="{{ $config->description }}">
    <meta property="og:site_name" content="{{ $config->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
@endif

    <title>{{ $config->title }}</title>

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('css/main.css') !!}

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <div class="carta-de-confirmacao">
        <img src="{{ asset('assets/img/layout/carta-logo-sbc.jpg') }}" alt="">

        <p>
            TEC/2020<br>
            Rio de Janeiro, 06 de Setembro de 2020.<br>
            Número da Inscrição: {{ $inscricao }}<br>
            Dr.(a): {{ $nome }}
        </p>
        <p>Prezado(a) colega,</p>
        <p>
            Confirmamos e agradecemos sua inscrição no Exame de Suficiência para obtenção do Título de Especialista em Cardiologia - 2020, que será realizado <span class="bold underline">no dia 27 de Outubro de 2020 (domingo)</span>. Informamos que o candidato deverá comparecer nos locais descritos abaixo, com <span class="bold underline">antecedência de 1 hora</span>, <span class="bold">munido de carteira de identidade com foto (RG ou CRM), lápis, borracha e caneta esferográfica (tinta azul ou preta) fabricada com material transparente</span>. Não será permitida qualquer espécie de consulta, uso de telefone celular, notebook ou quaisquer outros aparelhos eletrônicos.
        </p>

        <div class="carta-de-confirmacao-box">
            <p class="bold">
                <span class="bold underline" style="display:block;text-align:center;margin-bottom:.5em">Data, local e resultado da PROVA TEÓRICA</span><br>
                Data da Prova: 27 de Outubro de 2020 (domingo)<br>
                Horário: 13h às 18h30 (aplicação da prova)<br>
                Local: Universidade Paulista (UNIP) localizada na Rua Vergueiro, 1211 – Bairro: Paraíso, na cidade de São Paulo/SP, CEP 01504-000, Brasil.
            </p>
            <p class="bold">
                Resultado: O gabarito e o resultado estarão disponíveis no dia 29 de Outubro de 2020, terça-feira, na
                página virtual do TEC (<a href="http://educacao.cardiol.br/cjtec/">http://educacao.cardiol.br/cjtec/</a>).
            </p>
            <p class="italic">
                <span class="bold">Importante:</span> Ao entrar no local designado para prova, o candidato receberá um saco plástico onde deverá ser colocado relógio (digital ou analógico), telefone celular, notebook, tablet ou quaisquer outros aparelhos eletrônicos. A mesma será lacrada pelos organizadores da prova e devolvida ao candidato, ficando sob a responsabilidade do candidato zelar por seus pertences <span class="bold">antes, durante e depois</span> da aplicação da prova. <span class="vermelho"><span class="bold">ATENÇÃO:</span> evite trazer mochilas, malas ou bolsas grandes. Não haverá guarda-volumes no local da prova. Venha com roupas confortáveis, mas lembre-se que a sala estará climatizada.</span>
            </p>
        </div>

        <p>
            A nota da prova será somada a nota atribuída a análise curricular que já esta disponível na página do candidato.<br>
            Conforme edital 2020, o prazo de solicitação de revisão de pontos se encerrou em <span class="bold underline">10 de Setembro de 2020</span>.
        </p>
        <p>
            <span class="underline">ATENÇÃO</span><br>
            Esclarecimentos adicionais poderão ser obtidos pelo e-mail <span class="underline italic">agendatec@cardiol.br</span> ou telefone (21) 3478-2745.<br>Esta carta deverá ser IMPRESSA e apresentada no local de avaliação.<br><span class="vermelho bold">É IMPRESCINDÍVEL A APRESENTAÇÃO DESTA CARTA.</span>
        </p>
        <p>Atenciosamente,</p>
        <p>Comissão Julgadora do Título de Especialista em Cardiologia da SBC</p>

        <p class="bold text-center" style="margin: 4em 0 3em">
            <span class="underline">COMO CHEGAR NA UNIVERSIDADE PAULISTA (UNIP)</span>
            <br>
            Site:
            <a href="https://www.unip.br/presencial/universidade/campi/paraiso_vergueiro.aspx">
                https://www.unip.br/presencial/universidade/campi/paraiso_vergueiro.aspx
            </a>
        </p>

        <p class="bold text-center">MAPA</p>

        <div class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.8803496191576!2d-46.641965784474074!3d-23.572740384677278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5999978b48df%3A0xceb9c9263e11fa9e!2sUNIP!5e0!3m2!1spt-BR!2sbr!4v1567777979736!5m2!1spt-BR!2sbr" frameborder="0" style="border:0;" allowfullscreen></iframe>
            <img src="{{ asset('assets/img/layout/mapa.png') }}" alt="">
        </div>

        <p class="text-center vermelho bold underline">
            ATENÇÃO: CHEGUE COM ANTECEDÊNCIA DE 1 HORA AO LOCAL DA PROVA.
        </p>

        <p>
            1. Lembramos que a Prova do TEC 2020 terá início <span class="bold underline">às 13 horas e não será tolerado atraso</span>.<br>
            2. Local: UNIP - <span class="bold underline">Rua Vergueiro, 1211 – Bairro: Paraíso, na cidade de São Paulo / SP – Brasil</span>
        </p>

        <div class="btn-imprimir text-center">
            <a href="javascript:window.print()">
                IMPRIMIR &raquo;
            </a>
        </div>
    </div>

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ $config->analytics }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>

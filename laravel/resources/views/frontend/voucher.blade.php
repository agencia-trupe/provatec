@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="voucher interna center">
        <div class="main-box">
            <h2>COMPROVANTE DE INSCRIÇÃO</h2>
            <img src="{{ asset('assets/img/layout/marcas-voucher.png') }}" alt="">
            <p class="inscricao">
                número de inscrição: <span>{{ $cadastro->id }}</span>
            </p>
            <div class="col">
                <p>nome: {{ $cadastro->nome }}</p>
                <p>e-mail: {{ $cadastro->email }}</p>
                <p>CPF: {{ $cadastro->cpf }}</p>
                <p>
                    @foreach($cadastro->getComprovante('crm') as $key => $crm)
                        CRM {{ $crm['numero'] }}<br>
                    @endforeach
                </p>
            </div>
            <div class="col">
                <p>sexo: {{ $cadastro->sexo }}</p>
                <p>{{ $cadastro->lateralidade }}</p>
                @if($cadastro->getComprovante('socio_amb')->select == 'Sim')
                <p>Sócio AMB</p>
                @endif
            </div>
        </div>

        <p>Imprima este comprovante e apresente-o no dia da Prova juntamente com documento de identificação com foto.</p>
        <a href="javascript:window.print()">imprimir &raquo;</a>
    </div>

@endsection

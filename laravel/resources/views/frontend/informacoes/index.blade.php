@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="pag-informacoes interna center">
        <div class="main-box">
            <h2>Informações Sobre a Prova - Status de Aprovação</h2>
            <p>
                <small>Data de realização da Prova:</small>
                <strong>20 de Dezembro de 2020</strong>
                <br>
                <small>Pontos obtidos com a Documentação:</small>
                <strong>{{ (float) auth('cadastro')->user()->pontuacao_total }}</strong>
                <br>
                <small>Observação:</small>
                Conforme edital, o prazo de solicitação da revisão da pontuação curricular finalizou no dia 07 de Dezembro de 2020
            </p>
            @if($cadastro->pontuacao_prova > 0 || $cadastro->pontuacao_prova_excecao)
            <div class="resultado-box">
                @if($cadastro->pontuacao_prova_excecao)
                <p>
                    <small>Pontos obtidos com a prova:</small>
                    <strong>{{ mb_strtoupper($cadastro->pontuacao_prova_excecao) }}</strong>
                </p>
                @else
                <p>
                    <small>Pontos obtidos com a prova:</small>
                    <strong>{{ (float) $cadastro->pontuacao_prova }}</strong>
                    @if($cadastro->pontuacao_prova_pos_recurso > 0)
                    <br>
                    <small>Pontuação pós recurso:</small>
                    <strong>{{ (float) $cadastro->pontuacao_prova_pos_recurso }}</strong>
                    @endif
                    <br>
                    <small>Pontuação final:</small>
                    <strong>{{ (float) $cadastro->pontuacao_final }}</strong>
                </p>
                @endif
                @if($cadastro->status_avaliacao)
                    <p>
                        <small>Status:</small>
                        <strong>{{ strtoupper($cadastro->status_avaliacao) }}</strong>
                    </p>
                    @if($cadastro->status_avaliacao == 'aprovado')
                        <a href="{{ route('declaracaoDeAprovacao') }}" class="btn-download">
                            DOWNLOAD DA DECLARAÇÃO DE APROVAÇÃO
                        </a>
                    @endif
                @endif
            </div>
            @endif
        </div>

        <div class="main-box">
            <h2>GABARITO OFICIAL</h2>
            <p>Disponível a partir de: 21 de Dezembro de 2020.</p>
            @if(env('LINK_GABARITO'))
                <a href="{{ env('LINK_GABARITO') }}" target="_blank" class="btn-download">
                    FAÇA O DOWNLOAD DO GABARITO OFICIAL AQUI
                </a>
            @endif
        </div>

        @if(env('CADERNOS'))
        <div class="main-box">
            <h2>CADERNOS DE PROVA</h2>
            <a href="{{ env('CADERNOS') }}" target="_blank" class="btn-cadernos">
                CONSULTE AQUI O CADERNO MATRIZ DA PROVA &raquo;
            </a>
        </div>
        @endif

        @if($periodoRecurso->inicio)
        <div class="main-box">
            @if(! $periodoRecurso->fim && ! $respostaGeral->ativo)
            <h2>SOLICITAÇÃO DE RECURSO</h2>
            <p>Para enviar  sua solicitação de recurso preencha todos os campos abaixo, salve para gerar o número de protocolo e então imprima, assine, reconheça firma, escaneie, faça o upload do documento do recurso e do comprovante de pagamento para cada questão a sofrer recurso.</p>
            <form action="{{ route('recursos.store') }}" class="form-protocolo" method="POST">
                {!! csrf_field() !!}
                @if($errors->protocolo->all())
                    <div class="erro">
                        @foreach($errors->protocolo->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif
                {{-- <label>
                    Informe qual tipo de prova você realizou:
                    <select name="prova" required>
                        @foreach(['A', 'B', 'C', 'D'] as $i)
                        <option value="{{ $i }}" @if(old('prova') == $i) selected @endif>{{ $i }}</option>
                        @endforeach
                    </select>
                </label> --}}
                <label>
                    Informe o número da questão presente no caderno Matriz<br>(<strong>observação</strong>: NÃO é o número da questão da sua prova e sim do caderno matriz disponível nesta plataforma)
                    <select name="questao" style="width:200px" required>
                        <option value="">Selecione...</option>
                        @foreach(\App\Models\Recurso::selectQuestao() as $num => $label)
                        <option value="{{ $num }}" @if(old('questao') == $num) selected @endif)>
                            {{ $label }}
                        </option>
                        @endforeach
                    </select>
                </label>
                <textarea name="justificativa" placeholder="Insira a justificativa (até 1500 caracteres)" maxlength="1500" required>{{ old('justificativa') }}</textarea>
                <div class="bibliografia">
                    <p>Bibliografia:</p>
                    <div>
                        <label>
                            <input type="checkbox" name="bibliografia[]" value="1. Braunwald E, Bonow R, Zipes DP. Braunwald, tratado de doenças cardiovasculares. 10. ed. Rio de Janeiro: Elsevier, 2017 (versão português)." {{ (is_array(old('bibliografia')) and in_array('1. Braunwald E, Bonow R, Zipes DP. Braunwald, tratado de doenças cardiovasculares. 10. ed. Rio de Janeiro: Elsevier, 2017 (versão português).', old('bibliografia'))) ? ' checked' : '' }}>
                            1. Braunwald E, Bonow R, Zipes DP. Braunwald, tratado de doenças cardiovasculares. 10. ed. Rio de Janeiro: Elsevier, 2017 (versão português).
                        </label>
                        {{-- <label>
                            <input type="checkbox" name="bibliografia[]" value="2. Moreira MC, Montenegro ST, Paola AA, editores. Livro-texto da Sociedade Brasileira de Cardiologia. 2. ed. Barueri: Manole, 2015." {{ (is_array(old('bibliografia')) and in_array('2. Moreira MC, Montenegro ST, Paola AA, editores. Livro-texto da Sociedade Brasileira de Cardiologia. 2. ed. Barueri: Manole, 2015.', old('bibliografia'))) ? ' checked' : '' }}>
                            2. Moreira MC, Montenegro ST, Paola AA, editores. Livro-texto da Sociedade Brasileira de Cardiologia. 2. ed. Barueri: Manole, 2015.
                        </label>
                        <label>
                            <input type="checkbox" name="bibliografia[]" value="3. Kalil Filho R, Fuster V. Medicina cardiovascular. Reduzindo o impacto das doenças. São Paulo: Atheneu, 2016. Volumes 1 e 2." {{ (is_array(old('bibliografia')) and in_array('3. Kalil Filho R, Fuster V. Medicina cardiovascular. Reduzindo o impacto das doenças. São Paulo: Atheneu, 2016. Volumes 1 e 2.', old('bibliografia'))) ? ' checked' : '' }}>
                            3. Kalil Filho R, Fuster V. Medicina cardiovascular. Reduzindo o impacto das doenças. São Paulo: Atheneu, 2016. Volumes 1 e 2.
                        </label> --}}
                        <label>
                            <input type="checkbox" name="bibliografia[]" value="2. Diretrizes e posicionamentos da SBC (valendo a mais atual com suas respectivas atualizações até 30 de maio de 2020). Sendo obrigatório informar abaixo o tema, ano, número da diretriz e números das páginas:" {{ (is_array(old('bibliografia')) and in_array('2. Diretrizes e posicionamentos da SBC (valendo a mais atual com suas respectivas atualizações até 30 de maio de 2020). Sendo obrigatório informar abaixo o tema, ano, número da diretriz e números das páginas:', old('bibliografia'))) ? ' checked' : '' }}>
                            2. Diretrizes e posicionamentos da SBC (valendo a mais atual com suas respectivas atualizações até 30 de maio de 2020). Sendo obrigatório informar abaixo o tema, ano, número da diretriz e números das páginas:
                        </label>
                        <input type="text" name="bibliografia_complemento" placeholder="(até 200 caracteres)" maxlength="200" value="{{ old('bibliografia_complemento') }}">
                    </div>
                </div>
                <input type="submit" value="SALVAR PARA GERAR PROTOCOLO">
            </form>
            @else
            <h2 class="recursos-alt-title">RECURSOS</h2>
            <style>
                .recursos-alt-title { padding-bottom: 0 !important; }
                .recursos-alt-title:after { display:none; }
            </style>
            @endif

            @if(count($recursos))
            <div class="recursos-lista">
                @foreach($recursos as $recurso)
                @unless($respostaGeral->ativo && ! $recurso->data_envio)
                <div class="recurso" id="recurso-{{ $recurso->id }}">
                    @if(request('adicionado') == $recurso->id)
                    <div class="warning" style="margin-bottom:15px;text-transform:none;text-align:left"><strong>O texto do seu recurso foi salvo e um protocolo foi gerado porém ele ainda NÃO FOI ENVIADO. Continue o processo com a impressão, reconhecimento de firma e anexe os documentos para o envio completo do seu recurso aqui no sistema.</strong></div>
                    @endif
                    <div class="recurso-info">
                        <p>
                            <small>Protocolo:</small>
                            <strong>{{ $recurso->protocolo }}</strong>
                            <br>
                            <small>Gerado em:</small>
                            <strong>{{ Tools::formataDataHorario($recurso->created_at) }}</strong>
                        </p>
                        <a href="{{ route('recursos.show', $recurso) }}">VISUALIZAR E IMPRIMIR</a>
                        @if(! $recurso->data_envio)
                        <p><small>
                            observação: O recurso só é definitavemente enviado quando os comprovantes são anexados e o botão de ENVIAR RECURSO COMPLETO é acionado. A simples geração do número de protocolo NÃO CONFIGURA o envio do recurso à SBC.
                        </small></p>
                        @endif
                    </div>
                    <div class="recurso-envio">
                        @if($recurso->data_envio)
                            <p class="enviado">
                                <strong>RECURSO COMPLETO ENVIADO</strong>
                                EM:<br>
                                {{ Tools::formataDataHorario($recurso->data_envio) }}
                            </p>
                        @elseif(! $periodoRecurso->fim)
                            <form action="{{ route('recursos.enviar', $recurso) }}" method="POST" class="form-recurso-envio">
                                {!! csrf_field() !!}
                                @if($errors->envio->all())
                                    <div class="erro">Adicione todos os comprovantes</div>
                                @endif
                                @foreach([
                                    'documento_do_recurso' => 'Documento do Recurso com assinatura e firma reconhecida:',
                                    'comprovante_de_deposito' => 'Comprovante de depósito da taxa para enviar recurso:'
                                    ] as $input => $label)
                                <div class="col documentacao">
                                    <label>{{ $label }}</label>
                                    <div class="btn-adicionar-comprovante">
                                        <span></span>
                                        Adicionar comprovante
                                        <input type="file" name="arquivo" class="fileupload one-file-hide" data-url="{{ route('recursos.uploadComprovante', $input) }}">
                                    </div>
                                    <div class="files-wrapper"></div>
                                </div>
                                @endforeach
                                <input type="submit" value="ENVIAR RECURSO COMPLETO">
                            </form>
                        @endif
                    </div>
                </div>
                @endunless
                @endforeach
            </div>
            @endif

            <h2 class="recursos-resultados-title">RESULTADOS PÓS-RECURSOS</h2>
            @if(! $respostaGeral->ativo)
                <p>&laquo; aguarde &raquo;</p>
            @else
                @foreach($recursos as $recurso)
                    @if($respostas->has($recurso->questao_matriz) && $respostas[$recurso->questao_matriz])
                    <p style="text-align:left">
                        Protocolo <strong>{{ $recurso->protocolo }}</strong>. Prova <strong>{{ $recurso->prova }}</strong> / Questão <strong>{{ $recurso->questao_bloco }}</strong>:
                        <br>
                        {{ $respostas[$recurso->questao_matriz] }}
                    </p>
                    @endif
                @endforeach
                <hr style="margin:40px 0;border:0;border-top:1px solid rgba(0,0,0,.2)">
                <p style="text-align:left">{!! nl2br($respostaGeral->resposta_geral) !!}</p>
            @endif
        </div>
        @endif

        @if($respostaGeral->ativo)
        <div class="main-box">
            <h2>CERTIFICADOS</h2>
            <p>
                A emissão dos certificados é de responsabilidade da AMB.
            </p>
            <p>
                A Sociedade Brasileira de Cardiologia enviará para a AMB a listagem dos aprovados na prova do Título de Especialista em Cardiologia de 2020 trinta dias após a data da prova e, também, trinta dias após a divulgação divulgação dos recursos.
            </p>
            <p>
                Após a etapa acima um email será enviado aos aprovados com detalhes.
            </p>
            <p>
                O prazo da AMB para a entrega, após o pagamento do boleto, é de 120 dias
            </p>
            <p>
                Informações:
                <a href="https://www.portal.cardiol.br/cjtec" target="_blank" style="text-decoration: underline">
                    https://www.portal.cardiol.br/cjtec
                </a>
            </p>
        </div>
        @endif
    </div>

@endsection

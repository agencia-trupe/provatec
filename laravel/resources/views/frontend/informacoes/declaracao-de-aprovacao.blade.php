@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="voucher interna center">
        <div class="main-box" style="text-align:center">
            <h2>DECLARAÇÃO DE APROVAÇÃO</h2>
            <img src="{{ asset('assets/img/layout/marcas-voucher.png') }}" alt="" style="margin-bottom:2.5em">
            <p><strong>TEC/2020</strong></p>
            <p>Rio de Janeiro, {{ Tools::dataExtenso(\Carbon\Carbon::now()->format('d/m/Y')) }}.</p>
            <p>
                A SOCIEDADE BRASILEIRA DE CARDIOLOGIA declara para os devidos fins que {{ $cadastro->nome }} – CPF {{ $cadastro->cpf }}, obteve o Título de Especialista em Cardiologia, por meio de prova realizada em 20/12/2020 de acordo com as Normas estabelecidas pela Associação Médica Brasileira.
            </p>
            <p>
                Ressaltamos que o prazo para entrega do Diploma pela AMB é de aproximadamente 120 dias, a contar da data do pagamento do boleto.
            </p>
            <img src="{{ asset('assets/img/layout/assinatura-marcus.png') }}" alt="" style="width: 260px; margin:3em auto 1em">
            <p>
                <strong>Marcus Vinícius Santos Andrade</strong><br>
                Coordenador do CJTEC/SBC
            </p>
        </div>
        <p></p>
        <a href="javascript:window.print()">imprimir &raquo;</a>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="voucher interna center">
        <div class="main-box">
            <h2>SOLICITAÇÃO DE RECURSO</h2>
            <img src="{{ asset('assets/img/layout/marcas-voucher.png') }}" alt="">
            <div class="inscricao">
                protocolo: <span>{{ $recurso->protocolo }}</span><br>
                <span style="display:block;font-size:14px;font-weight:normal;margin-top:8px">
                    Registrado em: <strong>{{ Tools::formataDataHorario($recurso->created_at) }}</strong>
                </span>
                <span style="display:block;font-size:15px;font-weight:normal">
                    Recurso impetrado para a questão <strong>{{ $recurso->questao_bloco }}</strong> da prova <strong>{{ $recurso->prova }}</strong>
                </span>
            </div>
            <p><small>
                Justificativa:<br>
                {!! nl2br($recurso->justificativa) !!}
            </small></p>
            <p><small>
                Bibliografia:<br>
                {!! nl2br($recurso->bibliografia) !!}
            </small></p>
            <p style="margin-top:2em">Data: _____/_____/_____</p>
            <p style="margin-top:1.5em">Assinatura: ______________________________________________</p>
        </div>

        <p class="observacao">
            Observação: Reconhecer firma e <strong>enviar via Sistema do Provatec</strong> (www.provatec.com.br) até dia 23 de Dezembro de 2020.<br>
            A simples impressão deste documento <strong>NÃO CONFIGURA O ENVIO DO RECURSO.</strong>
        </p>
        <a href="javascript:window.print()">imprimir &raquo;</a>
    </div>

@endsection

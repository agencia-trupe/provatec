@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="pag-informacoes interna center">
        <div class="main-box">
            <h2>Informações Sobre a Prova</h2>
            <p>O período de inscrições para a Prova Tec 2020 já foi encerrado. Para dúvidas consulte o edital.</p>
        </div>
    </div>

@endsection

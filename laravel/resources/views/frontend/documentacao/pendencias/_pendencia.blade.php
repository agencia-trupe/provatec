@if(isset($pendencia_grupo))
<div class="bloco-pendencias">
    <div class="pendencias-solicitacao">
        <p>{!! App\Models\Campos::getTituloDescritivo($cadastro, $campo, $pendencia_grupo) !!}</p>
        <div class="pendencias-arquivos-anteriores">
            <span>COMPROVANTES ENVIADOS ANTERIORMENTE:</span>
            @foreach($cadastro->getComprovante($campo)[$grupo]['arquivos'] as $arquivo)
                <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
            @endforeach
        </div>
        <div class="solicitacao">{{ $cadastro->getComprovante($campo)[$pendencia_grupo]['msg_solicitacao'] }}</div>
        <textarea name="{{ $campo }}_resposta_{{ $pendencia_grupo }}" placeholder="Resposta do candidato">{{ array_key_exists('resposta', $cadastro->getComprovante($campo)[$pendencia_grupo]) ? $cadastro->getComprovante($campo)[$pendencia_grupo]['resposta'] : '' }}</textarea>
    </div>
    <div class="row">
        <div class="btn-adicionar-comprovante">
            <span></span>
            Adicionar comprovante
            <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovanteSubstituicao', [$campo, $pendencia_grupo]) }}">
        </div>
        <div class="files-wrapper">
            @if(array_key_exists('substituicoes', $cadastro->getComprovante($campo)[$pendencia_grupo]) && is_array($cadastro->getComprovante($campo)[$pendencia_grupo]['substituicoes']))
                @foreach($cadastro->getComprovante($campo)[$pendencia_grupo]['substituicoes'] as $arquivo)
                    <span>
                        <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                        <input type="hidden" name="{{ $campo }}_substituicoes_{{ $pendencia_grupo }}[]" value="{{ $arquivo }}">
                        <a href="#" class="excluir">X</a>
                    </span>
                @endforeach
            @endif
        </div>
        <input type="hidden" name="{{ $campo }}_grupo[]" value="{{ $pendencia_grupo }}">
    </div>
</div>
@else
<div class="bloco-pendencias">
    <div class="pendencias-solicitacao">
        <p>{!! App\Models\Campos::getTituloDescritivo($cadastro, $campo) !!}</p>
        <div class="pendencias-arquivos-anteriores">
            <span>COMPROVANTES ENVIADOS ANTERIORMENTE:</span>
            @foreach($cadastro->getComprovante($campo)->arquivos as $arquivo)
                <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
            @endforeach
        </div>
        <div class="solicitacao">{{ $cadastro->getComprovante($campo)->msg_solicitacao }}</div>
        <textarea name="{{ $campo }}_resposta" placeholder="Resposta do candidato">{{ isset($cadastro->getComprovante($campo)->resposta) ? $cadastro->getComprovante($campo)->resposta : '' }}</textarea>
    </div>
    <div class="row">
        <div class="btn-adicionar-comprovante">
            <span></span>
            Adicionar comprovante
            <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovanteSubstituicao', $campo) }}">
        </div>
        <div class="files-wrapper">
            @if(isset($cadastro->getComprovante($campo)->substituicoes))
                @foreach($cadastro->getComprovante($campo)->substituicoes as $arquivo)
                    <span>
                        <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                        <input type="hidden" name="{{ $campo }}_substituicoes[]" value="{{ $arquivo }}">
                        <a href="#" class="excluir">X</a>
                    </span>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endif

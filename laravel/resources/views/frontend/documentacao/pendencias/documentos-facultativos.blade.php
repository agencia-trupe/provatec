@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="status interna center">
        <div class="main-box">
            <h2>Documentação Enviada</h2>

            <div class="status-box">
                <p>status em {{ Tools::formataData($cadastro->status_data) }}:</p>
                <h3>DOCUMENTAÇÃO COM PENDÊNCIAS</h3>
            </div>

            <p>
                Você realizou o envio da documentação em:
                <span class="data">{{ Tools::formataData($cadastro->data_envio_documentacao) }}</span>
            </p>
        </div>

        @include('frontend.common._pendencias-nav', [
            'active' => 'documentos-facultativos'
        ])
    </div>

    <div class="documentacao interna interna-pendencias center">
    @if($pendenciasValidas)
        @include('frontend.documentacao.pendencias._envio')
    @endif
    @if(Tools::verificaPendencias($pendencias, 'documentos-facultativos'))
        <div class="main-box">
            <h2>Documentos Facultativos</h2>

            <form action="{{ route('documentacao.pendencias.documentosFacultativosPost') }}" method="POST">
                {!! csrf_field() !!}

                @foreach(array_merge(
                    \App\Models\Campos::documentosFacultativos()['experiencia'],
                    \App\Models\Campos::documentosFacultativos()['atualizacao']
                ) as $campo => $dados)
                    @if(array_key_exists($campo, $pendencias))
                        @if($dados['limite_grupo'] == 1)
                            @include('frontend.documentacao.pendencias._pendencia', [
                                'campo' => $campo
                            ])
                        @else
                            @foreach($pendencias[$campo] as $grupo)
                                @include('frontend.documentacao.pendencias._pendencia', [
                                    'campo'           => $campo,
                                    'pendencia_grupo' => $grupo
                                ])
                            @endforeach
                        @endif
                    @endif
                @endforeach

                <div class="submit-group">
                    <input type="submit" value="SALVAR">
                </div>
            </form>
        </div>
    @endif

    @if(!$pendenciasValidas)
        @include('frontend.documentacao.pendencias._envio')
    @endif
    </div>

@endsection

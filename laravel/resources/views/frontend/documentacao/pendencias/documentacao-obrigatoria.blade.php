@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="status interna center">
        <div class="main-box">
            <h2>Documentação Enviada</h2>

            <div class="status-box">
                <p>status em {{ Tools::formataData($cadastro->status_data) }}:</p>
                <h3>DOCUMENTAÇÃO COM PENDÊNCIAS</h3>
            </div>

            <p>
                Você realizou o envio da documentação em:
                <span class="data">{{ Tools::formataData($cadastro->data_envio_documentacao) }}</span>
            </p>
        </div>

        @include('frontend.common._pendencias-nav', [
            'active' => 'documentacao-obrigatoria'
        ])
    </div>

    <div class="documentacao interna interna-pendencias center">
    @if($pendenciasValidas)
        @include('frontend.documentacao.pendencias._envio')
    @endif
    @if(Tools::verificaPendencias($pendencias, 'documentacao-obrigatoria'))
        <div class="main-box">
            <h2>Documentaçao Obrigatória</h2>

            <form action="{{ route('documentacao.pendencias.documentacaoObrigatoriaPost') }}" method="POST">
                {!! csrf_field() !!}

                @if(array_key_exists('crm', $pendencias))
                    @foreach($pendencias['crm'] as $grupo)
                        @include('frontend.documentacao.pendencias._pendencia', [
                            'campo'           => 'crm',
                            'pendencia_grupo' => $grupo
                        ])
                    @endforeach
                @endif

                @if(array_key_exists('socio_amb', $pendencias))
                    @include('frontend.documentacao.pendencias._pendencia', [
                        'campo' => 'socio_amb'
                    ])
                @endif

                @if(array_key_exists('documentacao_obrigatoria', $pendencias))
                    @include('frontend.documentacao.pendencias._pendencia', [
                        'campo' => 'documentacao_obrigatoria'
                    ])
                @endif

                <div class="submit-group">
                    <input type="submit" value="SALVAR">
                </div>
            </form>
        </div>
    @endif

    @if(!$pendenciasValidas)
        @include('frontend.documentacao.pendencias._envio')
    @endif
    </div>

@endsection

<div class="main-box box-envio-documentacao">
    <h2>Envio das Pendências de Documentação</h2>

    <p>Vá para a página de Envio para realizar o envio das pendências da documentação.</p>

    <a href="{{ route('envio') }}" class="btn-enviar-documentacao">IR PARA PÁGINA DE ENVIO</a>
</div>

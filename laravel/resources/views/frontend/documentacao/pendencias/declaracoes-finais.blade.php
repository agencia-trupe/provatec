@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="status interna center">
        <div class="main-box">
            <h2>Documentação Enviada</h2>

            <div class="status-box">
                <p>status em {{ Tools::formataData($cadastro->status_data) }}:</p>
                <h3>DOCUMENTAÇÃO COM PENDÊNCIAS</h3>
            </div>

            <p>
                Você realizou o envio da documentação em:
                <span class="data">{{ Tools::formataData($cadastro->data_envio_documentacao) }}</span>
            </p>
        </div>

        @include('frontend.common._pendencias-nav', [
            'active' => 'declaracoes-finais'
        ])
    </div>

    <div class="documentacao interna interna-pendencias center">
    @if($pendenciasValidas)
        @include('frontend.documentacao.pendencias._envio')
    @endif
    @if(Tools::verificaPendencias($pendencias, 'declaracoes-finais'))
        <div class="main-box">
            <h2>Declarações Finais</h2>

            <form action="{{ route('documentacao.pendencias.declaracoesFinaisPost') }}" method="POST">
                {!! csrf_field() !!}

                @if(array_key_exists('declaracao_veracidade_informacoes', $pendencias))
                    @include('frontend.documentacao.pendencias._pendencia', [
                        'campo' => 'declaracao_veracidade_informacoes'
                    ])
                @endif

                @if(array_key_exists('declaracao_pessoa_portadora_deficiencia', $pendencias))
                    @include('frontend.documentacao.pendencias._pendencia', [
                        'campo' => 'declaracao_pessoa_portadora_deficiencia'
                    ])
                @endif

                <div class="submit-group">
                    <input type="submit" value="SALVAR">
                </div>
            </form>
        </div>
    @endif

    @if(!$pendenciasValidas)
        @include('frontend.documentacao.pendencias._envio')
    @endif
    </div>

@endsection

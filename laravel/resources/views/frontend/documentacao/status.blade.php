@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="status interna center">
        <div class="main-box">
            <h2>Documentação Enviada</h2>

            @if($cadastro->status == 'enviado' || $cadastro->status == 'andamento' || $cadastro->status == 'substituido')
                @if($cadastro->status == 'substituido')
                <p>
                    Você enviou as substituições da documentação em:
                    <span class="data">{{ Tools::formataDataHorario($cadastro->data_envio_substituicao) }}</span>
                </p>
                <p>
                    Seu envio inicial de documentação ocorreu em:
                    {{ Tools::formataData($cadastro->data_envio_documentacao) }}
                </p>
                @else
                <p>
                    Você realizou o envio da documentação em:
                    <span class="data">{{ Tools::formataData($cadastro->data_envio_documentacao) }}</span>
                </p>
                @endif
                <h4>AGUARDE A AVALIAÇÃO DA SUA DOCUMENTAÇÃO PARA OBTER O LINK DE PAGAMENTO DA TAXA DE INSCRIÇÃO.</h4>
                <p>Caberá ao candidato a responsabilidade de acompanhar as pendências nesta página de inscrição do candidato e dar a continuidade ao processo.</p>
                <p>Eventuais emails podem ser enviados para alertá-los de Documentação pendente/ Pagamento/ Cancelamento (atente-se para o fato de recebimento na caixa de lixo eletrônico). Solicitamos que veja frequentemente o sistema do candidato para o acompanhamento.</p>
            @elseif($cadastro->status == 'valido')
                <div class="status-box">
                    <p>status em {{ Tools::formataData($cadastro->status_data) }}:</p>
                    <h3>DOCUMENTAÇÃO VALIDADA - PONTUAÇÃO OBTIDA: <strong>{{ (float)$cadastro->pontuacao_total }} PONTOS</strong></h3>
                    @if($cadastro->cpf && $cadastro->verificaPagamento($cadastro->cpf))
                    <a href="{{ route('pagamento') }}" class="botao-verificar">VERIFICAR STATUS DO PAGAMENTO DA TAXA DE INSCRIÇÃO &raquo;</a>
                    @else
                    <a href="{{ route('pagamento') }}" class="botao-instrucoes">INSTRUÇÕES PARA O PAGAMENTO DA TAXA DE INSCRIÇÃO &raquo;</a>
                    @endif
                </div>
                <p>
                    Você realizou o envio da documentação em:
                    <span class="data">{{ Tools::formataData($cadastro->data_envio_documentacao) }}</span>
                </p>
            @elseif($cadastro->status == 'invalido')
                <p>
                    Você realizou o envio da documentação em:
                    <span class="data">{{ Tools::formataData($cadastro->data_envio_documentacao) }}</span>
                </p>
                <div class="status-box">
                    <p>status em {{ Tools::formataData($cadastro->status_data) }}:</p>
                    <h3><strong>DOCUMENTAÇÃO INDEFERIDA</strong></h3>
                    <p>Sua inscrição foi RECUSADA em nosso sistema.</p>
                </div>
            @endif

        </div>

        @if($cadastro->status == 'valido')
            @include('frontend.documentacao._descritivo-pontuacao')

            @if(env('SOLICITACAO_DE_REVISAO'))
            <div class="main-box revisao-pontuacao">
                <h2>Solicitar revisão de pontuação</h2>
                @if($cadastro->revisao_envio)
                    <p>
                        Você enviou um pedido de solicitação de revisão da sua pontuação em
                        <strong>{{ Tools::formataData($cadastro->revisao_envio) }}</strong>.
                        <br>
                        Aguarde ser contatado pela equipe da SBC com o retorno da sua solicitação via e-mail.
                    </p>
                @else
                    <p>
                        De acordo com o item 1.3 da cláusula ‘X – DAS ETAPAS DA PROVA’ do Edital é possível solicitar revisão da pontuação obtida com o envio da documentação. Se optar por solicitar a revisão, descreva abaixo quais itens ou documentos não estão de acordo com o status de INVALIDADO na sua opinião para que a banca examinadora revise seus conteúdos. Justifique.
                    </p>

                    <form action="{{ route('documentacao.revisaoPost') }}" method="POST">
                        {!! csrf_field() !!}
                        <textarea name="revisao_mensagem" required></textarea>
                        <input type="submit" value="ENVIAR SOLICITAÇÃO DE REVISÃO">
                    </form>
                @endif
            </div>
            @endif
        @endif
    </div>

@endsection

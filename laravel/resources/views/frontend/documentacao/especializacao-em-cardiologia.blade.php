@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')
    @include('frontend.common._documentacao-nav')

    <div class="documentacao interna center">
        <div class="main-box">
            <h2>Especialização em Cardiologia e Residência Médica &middot; Quesito Formação</h2>

            <form action="{{ route('documentacao.especializacaoEmCardiologiaPost') }}" method="POST">
                {!! csrf_field() !!}

                <p class="descricao">Número máximo de pontos a acumular neste quesito: <strong>10</strong></p>
                <p class="descricao">DOCUMENTAÇÃO OBRIGATÓRIA: consultar Capítulo VI do Edital.</p>
                <p class="descricao">
                    <strong>ATENÇÃO: Os comprovantes de Residência Médica precisam ser anexados com frente e verso do documento.</strong>
                </p>

                @foreach($campos as $input => $dados)
                    @if($input == 'estagio_2_anos_clinica_medica')
                        <h4>DOCUMENTOS COMPROBATÓRIOS DE CLÍNICA MÉDICA</h4>
                    @endif
                    @if($input == 'estagio_2_anos_cardiologia')
                        <div style="border-top:4px solid #00B0DC;margin-top:3em"></div>
                        <h4>DOCUMENTOS COMPROBATÓRIOS DE CARDIOLOGIA</h4>
                    @endif
                    @if($input == 'comprovante_capacitacao_clinica_medica')
                        <h4 class="divider">OU</h4>
                        <h4>DOCUMENTOS COMPROBATÓRIOS DE ATUAÇÃO PRÁTICO PROFISSIONAL - PARA CLÍNICA MÉDICA</h4>
                    @endif
                    @if($input == 'comprovante_atuacao_cardiologia')
                        <h4 class="divider">OU</h4>
                        <h4>DOCUMENTOS COMPROBATÓRIOS DE ATUAÇÃO PRÁTICO PROFISSIONAL - PARA CARDIOLOGIA</h4>
                    @endif

                    <div class="row" data-campo="{{ $input }}">
                        <label>{!! $dados['label'] !!}</label>
                        @if(in_array($input, $excedentes) && $cadastro->getComprovante($input))
                        <div class="limpar-comprovante-especializacao-wrapper" data-campo="{{ $input }}">
                            <a href="#" class="limpar-comprovante-especializacao">excluir comprovante excedente</a>
                        </div>
                        @endif
                        <div class="flex-uf-instituicao">
                            <select name="uf_{{ $input }}">
                                <option value="">UF</option>
                                @foreach(array_keys(Tools::instituicoes()) as $uf)
                                <option value="{{ $uf }}" @if($cadastro->getComprovante($input) && $cadastro->getComprovante($input)->uf == $uf) selected @endif>{{ $uf }}</option>
                                @endforeach
                            </select>
                            <select name="instituicao_{{ $input }}" class="select-outra" @if(!$cadastro->getComprovante($input) || !$cadastro->getComprovante($input)->uf) disabled @endif>
                                <option value="">Instituição (selecionar)</option>
                                @if($cadastro->getComprovante($input) && $cadastro->getComprovante($input)->uf)
                                    @foreach(array_merge(Tools::instituicoes()[$cadastro->getComprovante($input)->uf], ['OUTRA']) as $i)
                                    <option value="{{ $i }}" @if($cadastro->getComprovante($input)->instituicao == $i) selected @endif>{{ $i }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row row-outra" data-campo="{{ $input }}" @if(!$cadastro->getComprovante($input) || $cadastro->getComprovante($input)->instituicao != 'OUTRA') style="display:none;" @endif>
                        <span>Informe a instituição:</span>
                        <input type="text" name="instituicao_{{ $input }}_outra" @if($cadastro->getComprovante($input)) value="{{ $cadastro->getComprovante($input)->instituicao_outra }}" @endif>
                    </div>
                    <div class="row row-margin" data-campo="{{ $input }}">
                        <select name="ano_{{ $input }}">
                            <option value="" @if($cadastro->getComprovante($input) && !$cadastro->getComprovante($input)->ano) selected @endif>Ano de Conclusão</option>
                            @foreach(Tools::faixaAnos(null, array_key_exists('ano_max', $dados) ? $dados['ano_max'] : null) as $ano)
                                <option value="{{ $ano }}" @if($cadastro->getComprovante($input) && $cadastro->getComprovante($input)->ano == $ano) selected @endif>{{ $ano }}</option>
                            @endforeach
                        </select>
                        <div class="btn-adicionar-comprovante">
                            <span></span>
                            Adicionar comprovante
                            <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovante', $input) }}">
                        </div>
                        <div class="files-wrapper">
                            @if($cadastro->getComprovante($input) && count($cadastro->getComprovante($input)->arquivos))
                                @foreach($cadastro->getComprovante($input)->arquivos as $arquivo)
                                    <span>
                                        <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                        <input type="hidden" name="{{ $input }}_arquivos[]" value="{{ $arquivo }}">
                                        <a href="#" class="excluir">X</a>
                                    </span>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach

                <div class="submit-group">
                    <input type="submit" value="SALVAR PARA CONTINUAR">
                    @if($cadastro->especializacao_cardiologia_status == 'pronto para envio')
                    <input type="submit" class="salvo-envio" formaction="{{ route('documentacao.especializacaoEmCardiologiaPost', ['concluir' => 1]) }}" value="SALVO PARA ENVIO">
                    @else
                    <input type="submit" formaction="{{ route('documentacao.especializacaoEmCardiologiaPost', ['concluir' => 1]) }}" value="SALVAR PARA CONCLUIR">
                    @endif
                </div>
            </form>
        </div>
    </div>

    <script>
        var PROVATEC_INSTITUICOES = {!! json_encode(Tools::instituicoes()) !!}
    </script>

@endsection

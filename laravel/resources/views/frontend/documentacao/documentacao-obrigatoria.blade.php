@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')
    @include('frontend.common._documentacao-nav')

    <div class="documentacao interna center">
        <div class="main-box">
            <h2>Documentação Obrigatória</h2>

            <form action="{{ route('documentacao.documentacaoObrigatoriaPost') }}" method="POST">
                {!! csrf_field() !!}

                <p class="atencao">Deve estar formado até 31 de outubro de 2016.</p>

                <div class="row">
                    <select name="graduacao" class="select-outra">
                        <option value="" @if($cadastro->getComprovante('documentacao_obrigatoria') && !$cadastro->getComprovante('documentacao_obrigatoria')->graduacao) selected @endif>Graduado em: Faculdade de Medicina (selecionar)</option>
                        @foreach(Tools::faculdades() as $faculdade)
                        <option value="{{ $faculdade }}" @if($cadastro->getComprovante('documentacao_obrigatoria') && $cadastro->getComprovante('documentacao_obrigatoria')->graduacao == $faculdade) selected @endif>{{ $faculdade }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row row-outra" @if(!$cadastro->getComprovante('documentacao_obrigatoria') || $cadastro->getComprovante('documentacao_obrigatoria')->graduacao != 'OUTRA') style="display:none;" @endif>
                    <span>Informe a instituição:</span>
                    <input type="text" name="graduacao_outra" @if($cadastro->getComprovante('documentacao_obrigatoria')) value="{{ $cadastro->getComprovante('documentacao_obrigatoria')->graduacao_outra }}" @endif>
                </div>
                <div class="row">
                    <select name="ano_graduacao">
                        <option value="" @if($cadastro->getComprovante('documentacao_obrigatoria') && !$cadastro->getComprovante('documentacao_obrigatoria')->ano_graduacao) selected @endif>Ano de Graduação</option>
                        @foreach(Tools::faixaAnos(null, 2016) as $ano)
                            <option value="{{ $ano }}" @if($cadastro->getComprovante('documentacao_obrigatoria') && $cadastro->getComprovante('documentacao_obrigatoria')->ano_graduacao == $ano) selected @endif>{{ $ano }}</option>
                        @endforeach
                    </select>
                    <input type="text" name="uf" maxlength="2" @if($cadastro->getComprovante('documentacao_obrigatoria')) value="{{ $cadastro->getComprovante('documentacao_obrigatoria')->uf }}" @endif placeholder="UF">
                </div>
                <div class="row">
                    <div class="btn-adicionar-comprovante">
                        <span></span>
                        Adicionar comprovante
                        <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovante', 'documentacao_obrigatoria') }}">
                    </div>
                    <div class="files-wrapper">
                        @if($cadastro->getComprovante('documentacao_obrigatoria') && count($cadastro->getComprovante('documentacao_obrigatoria')->arquivos))
                            @foreach($cadastro->getComprovante('documentacao_obrigatoria')->arquivos as $arquivo)
                                <span>
                                    <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                    <input type="hidden" name="documentacao_obrigatoria_arquivos[]" value="{{ $arquivo }}">
                                    <a href="#" class="excluir">X</a>
                                </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="submit-group">
                    <input type="submit" value="SALVAR PARA CONTINUAR">
                    @if($cadastro->documentacao_obrigatoria_status == 'pronto para envio')
                    <input type="submit" class="salvo-envio" formaction="{{ route('documentacao.documentacaoObrigatoriaPost', ['concluir' => 1]) }}" value="SALVO PARA ENVIO">
                    @else
                    <input type="submit" formaction="{{ route('documentacao.documentacaoObrigatoriaPost', ['concluir' => 1]) }}" value="SALVAR PARA CONCLUIR">
                    @endif
                </div>
            </form>
        </div>
    </div>

@endsection

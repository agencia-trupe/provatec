<div class="main-box">
    <h2>Descritivo da Pontuação</h2>
    <div class="descritivo-pontuacao">
        <table>
        @foreach(App\Models\Campos::especializacaoEmCardiologia() as $campo => $dados)
            @if($cadastro->getComprovante($campo) && count($cadastro->getComprovante($campo)->arquivos))
            <tr>
                <td style="width:60%;vertical-align:top;"><p style="color:#00b0dc;font-weight:700;">{!! $dados['label'] !!}:</p></td>
                <td style="padding-left:20px;vertical-align:top;">
                    <p>
                        {{ $cadastro->getComprovante($campo)->uf }} |
                        {{ $cadastro->getComprovante($campo)->ano }}
                        @if($cadastro->getComprovanteStatus($campo) == 'valido')
                            <strong>[VALIDADO]</strong>
                        @elseif($cadastro->getComprovanteStatus($campo) == 'invalido')
                            <strong>[INVALIDADO]</strong>
                        @else
                            <strong>[NÃO AVALIADO]</strong>
                        @endif
                    </p>
                </td>
            </tr>
            @endif
        @endforeach
            <tr>
                <td colspan="2">
                    <p><span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:1em">
                        TOTAL ESPECIALIZAÇÃO = {{ (float)$cadastro->pontuacao_especializacao }} pontos
                    </span></p>
                </td>
            </tr>

            <tr>
                <td colspan="2"><div style="width:100%;height:1px;background:#D2DFDB;margin:20px 0;"></div></td>
            </tr>

        @foreach(App\Models\Campos::documentosFacultativos()['experiencia'] as $campo => $dados)
            @if($cadastro->getComprovante($campo) && count($cadastro->getComprovante($campo)->arquivos))
            <tr>
                <td style="width:60%;vertical-align:top;"><p style="color:#00b0dc;font-weight:700;">{!! $dados['label'] !!}:</p></td>
                <td style="padding-left:20px;vertical-align:top;">
                    <p>
                        {{ $cadastro->getComprovante($campo)->ano }}
                        @if($cadastro->getComprovanteStatus($campo) == 'valido')
                            <strong>[VALIDADO]</strong>
                        @elseif($cadastro->getComprovanteStatus($campo) == 'invalido')
                            <strong>[INVALIDADO]</strong>
                        @else
                            <strong>[NÃO AVALIADO]</strong>
                        @endif
                    </p>
                </td>
            </tr>
            @if($cadastro->getComprovanteStatus($campo) == 'valido' && $cadastro->pontuacao_facultativa == 30)
            <tr>
                <td colspan="2">
                    <p><span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:1em">
                        TOTAL EXPERIÊNCIA PROFISSIONAL = 30 pontos
                    </span></p>
                    <div class="warning" style="text-transform:none">
                        A pontuação máxima foi obtida com o quesito experiência profissional. Outras pontuações eventualmente listadas não são adicionadas à pontuação máxima, que é de 30 pontos.
                    </div>
                </td>
            </tr>
            @endif

            <tr>
                <td colspan="2"><div style="width:100%;height:1px;background:#D2DFDB;margin:20px 0;"></div></td>
            </tr>
            @endif
        @endforeach

        @foreach(App\Models\Campos::documentosFacultativos()['atualizacao'] as $campo => $dados)
            @if($dados['limite_grupo'] == 0)
                @if($cadastro->getComprovante($campo))
                <tr>
                    <td style="width:60%;vertical-align:top;"><p style="color:#00b0dc;font-weight:700;">{!! $dados['label'] !!}:</p></td>
                    <td style="padding-left:20px;vertical-align:top;">
                        <p>
                            {{ $cadastro->getComprovante($campo)->ano }}
                            @if($cadastro->getComprovanteStatus($campo) == 'valido')
                                <strong>[VALIDADO]</strong>
                            @elseif($cadastro->getComprovanteStatus($campo) == 'invalido')
                                <strong>[INVALIDADO]</strong>
                            @else
                                <strong>[NÃO AVALIADO]</strong>
                            @endif
                        </p>
                    </td>
                </tr>
                @endif
            @elseif($dados['limite_grupo'] == 1)
                @if($cadastro->getComprovante($campo) && count($cadastro->getComprovante($campo)->arquivos))
                <tr>
                    <td style="width:60%;vertical-align:top;"><p style="color:#00b0dc;font-weight:700;">{!! $dados['label'] !!}:</p></td>
                    <td style="padding-left:20px;vertical-align:top;">
                        <p>
                            {{ $cadastro->getComprovante($campo)->ano }}
                            @if($cadastro->getComprovanteStatus($campo) == 'valido')
                                <strong>[VALIDADO]</strong>
                            @elseif($cadastro->getComprovanteStatus($campo) == 'invalido')
                                <strong>[INVALIDADO]</strong>
                            @else
                                <strong>[NÃO AVALIADO]</strong>
                            @endif
                        </p>
                    </td>
                </tr>
                @endif
            @else
                @if($cadastro->getComprovante($campo))
                <tr>
                    <td style="width:60%;vertical-align:top;"><p style="color:#00b0dc;font-weight:700;">{!! $dados['label'] !!}:</p></td>
                    <td style="padding-left:20px;vertical-align:top;">
                        <p>
                        @foreach($cadastro->getComprovante($campo) as $key => $comprovante)
                            {{ $comprovante['ano'] }}
                            @if($cadastro->getComprovanteStatus($campo, $key) == 'valido')
                                <strong>[VALIDADO]</strong>
                            @elseif($cadastro->getComprovanteStatus($campo, $key) == 'invalido')
                                <strong>[INVALIDADO]</strong>
                            @else
                                <strong>[NÃO AVALIADO]</strong>
                            @endif
                            <br>
                        @endforeach
                            <span style="margin-top:4px;display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:.8em">
                                TOTAL = {{ $cadastro->pontuacaoDocumentoFacultativo($campo) }}
                            </span>
                        </p>
                    </td>
                </tr>
                @endif
            @endif

            @if($campo == 'doutorado_livre_docencia_cardiologia')
                <tr><td colspan="2">
                    <p><span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:1em">
                        TOTAL B1 = {{ $cadastro->pontuacaoSecoesDocumentosFacultativos()['b1'] }}
                    </span></p>
                </td></tr>
                <tr>
                    <td colspan="2"><div style="width:100%;height:1px;background:#D2DFDB;margin:20px 0;"></div></td>
                </tr>
            @endif
            @if($campo == 'curso_online_captec')
                <tr><td colspan="2">
                    <p><span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:1em">
                        TOTAL B2 = {{ $cadastro->pontuacaoSecoesDocumentosFacultativos()['b2'] }}
                    </span></p>
                </td></tr>
                <tr>
                    <td colspan="2"><div style="width:100%;height:1px;background:#D2DFDB;margin:20px 0;"></div></td>
                </tr>
            @endif
            @if($campo == 'participacao_registros_brasileiros')
                <td colspan="2">
                    <p><span style="display:inline-block;background:rgba(0,0,0,.1);padding:6px;border-radius:4px;font-weight:700;font-size:1em">
                        TOTAL B3 = {{ $cadastro->pontuacaoSecoesDocumentosFacultativos()['b3'] }}
                    </span></p>
                </td>
            @endif
        @endforeach
        </table>
    </div>
</div>

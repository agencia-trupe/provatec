@extends('frontend.common.template')

@section('content')

    @if(session('erros-documentacao'))
        <div class="center erros-documentacao">
            <h2>Erro ao enviar documentação</h2>
            @if(count(session('erros-documentacao')['obrigatorio']))
                <p>
                    Preencha os seguintes campos obrigatórios:
                    <ul>
                        @foreach(session('erros-documentacao')['obrigatorio'] as $campo)
                        <li>&middot; {{ $campo }}</li>
                        @endforeach
                    </ul>
                </p>
            @endif
            @if(count(session('erros-documentacao')['comprovantes']))
                @foreach(session('erros-documentacao')['comprovantes'] as $erro)
                <p>{{ $erro }}</p>
                @endforeach
            @endif
            @if(session('erros-documentacao')['especializacao'])
                <p>Preencha ao menos um dos campos de Especialização em Cardiologia.</p>
            @endif
        </div>
    @endif

    @include('frontend.common._main-nav')
    @include('frontend.common._documentacao-nav')

    <div class="documentacao interna center">
        <div class="main-box">
            <h2>Declarações Finais</h2>

            <form action="{{ route('documentacao.declaracoesFinaisPost') }}" method="POST">
                {!! csrf_field() !!}

                <div class="row row-margin">
                    <div class="warning" style="margin-bottom:10px">
                        Seu número de inscrição é: <strong>{{ $cadastro->id }}</strong>
                    </div>
                    <label><strong style="font-size:1.1em">Declaração de Veracidade das Informações (obrigatória) – Anexo II / Edital</strong></label>
                    <div class="btn-adicionar-comprovante">
                        <span></span>
                        Adicionar declaração
                        <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovante', 'declaracao_veracidade_informacoes') }}">
                    </div>
                    <div class="files-wrapper">
                        @if($cadastro->getComprovante('declaracao_veracidade_informacoes') && count($cadastro->getComprovante('declaracao_veracidade_informacoes')->arquivos))
                            @foreach($cadastro->getComprovante('declaracao_veracidade_informacoes')->arquivos as $arquivo)
                                <span>
                                    <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                    <input type="hidden" name="declaracao_veracidade_informacoes_arquivos[]" value="{{ $arquivo }}">
                                    <a href="#" class="excluir">X</a>
                                </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-divider" style="margin:25px auto"></div>
                <div class="row row-margin">
                    <label>Declaração de Pessoa Portadora de Deficiência – Anexo II / Edital</label>
                    <div class="btn-adicionar-comprovante">
                        <span></span>
                        Adicionar declaração
                        <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovante', 'declaracao_pessoa_portadora_deficiencia') }}">
                    </div>
                    <div class="files-wrapper">
                        @if($cadastro->getComprovante('declaracao_pessoa_portadora_deficiencia') && count($cadastro->getComprovante('declaracao_pessoa_portadora_deficiencia')->arquivos))
                            @foreach($cadastro->getComprovante('declaracao_pessoa_portadora_deficiencia')->arquivos as $arquivo)
                                <span>
                                    <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                    <input type="hidden" name="declaracao_pessoa_portadora_deficiencia_arquivos[]" value="{{ $arquivo }}">
                                    <a href="#" class="excluir">X</a>
                                </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="submit-group">
                    <input type="submit" value="SALVAR PARA CONTINUAR">
                    @if($cadastro->declaracoes_finais_status == 'pronto para envio')
                    <input type="submit" class="salvo-envio" formaction="{{ route('documentacao.declaracoesFinaisPost', ['concluir' => 1]) }}" value="SALVO PARA ENVIO">
                    @else
                    <input type="submit" formaction="{{ route('documentacao.declaracoesFinaisPost', ['concluir' => 1]) }}" value="SALVAR PARA CONCLUIR">
                    @endif
                </div>
            </form>
        </div>

        <div class="main-box box-envio-documentacao">
            <h2>Envio da Documentação Completa</h2>

            <p>Vá para a página de Envio para realizar o envio da documentação.</p>

            <a href="{{ route('envio') }}" class="btn-enviar-documentacao">IR PARA PÁGINA DE ENVIO</a>
        </div>
    </div>

@endsection

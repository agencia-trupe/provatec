@if($input == 'cursos_presenciais_reciclagem')
    <h2>Documentos Facultativos &middot; Quesito Atualização [B.2]</h2>
    <p class="descricao">Número máximo de pontos a acumular neste quesito: <strong>4</strong></p>
@endif
@if($input == 'publicacao_artigo_original')
    <h2>Documentos Facultativos &middot; Quesito Atualização [B.3]</h2>
    <p class="descricao">Número máximo de pontos a acumular neste quesito: <strong>2</strong></p>
@endif

@if($dados['limite_grupo'] == 0)
    <div class="row row-margin">
        <label>{!! $dados['label'] !!}</label>
        <select name="ano_{{ $input }}">
            <option value="" @if($cadastro->getComprovante($input) && !$cadastro->getComprovante($input)->ano) selected @endif>{{ $dados['ano_placeholder'] }}</option>
            @foreach(Tools::faixaAnos(array_key_exists('limite_anos', $dados) ? $dados['limite_anos'] : null) as $ano)
                <option value="{{ $ano }}" @if($cadastro->getComprovante($input) && $cadastro->getComprovante($input)->ano == $ano) selected @endif>{{ $ano }}</option>
            @endforeach
        </select>
    </div>
@elseif($dados['limite_grupo'] == 1)
    <div class="row row-margin">
        <label>{!! $dados['label'] !!}</label>
        <select name="ano_{{ $input }}">
            <option value="" @if($cadastro->getComprovante($input) && !$cadastro->getComprovante($input)->ano) selected @endif>{{ $dados['ano_placeholder'] }}</option>
            @foreach(Tools::faixaAnos(array_key_exists('limite_anos', $dados) ? $dados['limite_anos'] : null) as $ano)
                <option value="{{ $ano }}" @if($cadastro->getComprovante($input) && $cadastro->getComprovante($input)->ano == $ano) selected @endif>{{ $ano }}</option>
            @endforeach
        </select>
        <div class="btn-adicionar-comprovante">
            <span></span>
            Adicionar comprovante
            <input type="file" name="arquivo" class="fileupload" multiple data-url="{{ route('documentacao.uploadComprovante', $input) }}">
        </div>
        <div class="files-wrapper">
            @if($cadastro->getComprovante($input) && count($cadastro->getComprovante($input)->arquivos))
                @foreach($cadastro->getComprovante($input)->arquivos as $arquivo)
                    <span>
                        <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                        <input type="hidden" name="{{ $input }}_arquivos[]" value="{{ $arquivo }}">
                        <a href="#" class="excluir">X</a>
                    </span>
                @endforeach
            @endif
        </div>
    </div>
@else
    <div class="row">
        <label>{!! $dados['label'] !!}</label>
        @if(!$cadastro->getComprovante($input))
            <input type="hidden" name="{{ $input }}_grupo[]" value="1">
            <select name="ano_{{ $input }}_1">
                <option value="" selected>{{ $dados['ano_placeholder'] }}</option>
                @foreach(Tools::faixaAnos(array_key_exists('limite_anos', $dados) ? $dados['limite_anos'] : null) as $ano)
                    <option value="{{ $ano }}">{{ $ano }}</option>
                @endforeach
            </select>
            <div class="btn-adicionar-comprovante">
                <span></span>
                Adicionar comprovante
                <input type="file" name="arquivo" class="fileupload one-file-hide" data-url="{{ route('documentacao.uploadComprovante', [$input, 1]) }}">
            </div>
            <div class="files-wrapper"></div>
        @else
            @foreach($cadastro->getComprovante($input) as $key => $value)
                <div class="row">
                    <input type="hidden" name="{{ $input }}_grupo[]" value="{{ $key }}">
                    <select name="ano_{{ $input }}_{{ $key }}">
                        <option value="" @if(!$cadastro->getComprovante($input)) selected @endif>{{ $dados['ano_placeholder'] }}</option>
                        @foreach(Tools::faixaAnos(array_key_exists('limite_anos', $dados) ? $dados['limite_anos'] : null) as $ano)
                            <option value="{{ $ano }}" @if($cadastro->getComprovante($input)[$key]['ano'] == $ano) selected @endif>{{ $ano }}</option>
                        @endforeach
                    </select>
                    <div class="btn-adicionar-comprovante" @if(count($cadastro->getComprovante($input)[$key]['arquivos'])) style="display:none" @endif>
                        <span></span>
                        Adicionar comprovante
                        <input type="file" name="arquivo" class="fileupload one-file-hide" data-url="{{ route('documentacao.uploadComprovante', [$input, $key]) }}">
                    </div>
                    <div class="files-wrapper">
                        @if(count($cadastro->getComprovante($input)[$key]['arquivos']))
                            @foreach($cadastro->getComprovante($input)[$key]['arquivos'] as $arquivo)
                                <span>
                                    <a href="{{ asset('comprovantes/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                                    <input type="hidden" name="{{ $input }}_arquivos_{{ $key }}[]" value="{{ $arquivo }}">
                                    <a href="#" class="excluir">X</a>
                                </span>
                            @endforeach
                        @endif
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="row row-margin">
        @if(count($cadastro->getComprovante($input)) < $dados['limite_grupo'])
        <a href="#" class="btn-add-row" data-input="{{ $input }}" data-url="{{ route('documentacao.uploadComprovante', $input) }}" data-limite="{{ $dados['limite_grupo'] }}" data-ano-placeholder="{{ $dados['ano_placeholder'] }}">
            <span></span>
            @if($input == 'curso_online_captec')
            Para pontuar, adicione o ano de conclusão e um certificado correspondente para cada módulo.<br>{{ $dados['btn_grupo'] }} (até {{ $dados['limite_grupo'] }})
            @else
            Para pontuar adicione um ano de formação e um comprovante para cada item clicando aqui.<br>{{ $dados['btn_grupo'] }} (até {{ $dados['limite_grupo'] }})
            @endif
        </a>
        @endif
    </div>
@endif

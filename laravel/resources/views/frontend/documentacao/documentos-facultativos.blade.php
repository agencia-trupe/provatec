@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')
    @include('frontend.common._documentacao-nav')

    <div class="documentacao interna center">
        <div class="main-box">
            <h2>Documentos Facultativos &middot; Quesito Atualização [B.1]</h2>

            <form action="{{ route('documentacao.documentosFacultativosPost') }}" method="POST">
                {!! csrf_field() !!}

                <p class="descricao">Número máximo de pontos a acumular neste quesito: <strong>4</strong></p>

                <p class="descricao">
                    <small>
                        (*) Considera-se como nos últimos 2 anos: junho de 2018 a outubro de 2020, até o prazo final do período de inscrição.
                    </small>
                    <br>
                    <small>
                        (**) Considera-se como nos últimos 3 anos: junho de 2017 a outubro de 2020, até o prazo final do período de inscrição.
                    </small>
                </p>

                @foreach($campos['atualizacao'] as $input => $dados)
                   @include('frontend.documentacao._campo-facultativo', [
                       'input' => $input,
                       'dados' => $dados
                   ])
                @endforeach

                <h2 class="segundo-titulo">Documentos Facultativos &middot; Quesito Experiência Profissional</h2>
                <p class="descricao">Número máximo de pontos a acumular neste quesito: <strong>30</strong></p>
                <p class="atencao">Importante: o candidato que se adequar ao QUESITO EXPERIÊNCIA PROFISSIONAL, totaliza 30 pontos de currículo e, não tem direito a pontuação adicional pela análise curricular (quesito atualização). Para validação deste item é obrigatório apresentar pelo menos 4 anos de atuação em clínica médica e, também, o documento comprobatório de 15 anos de formado com atuação em cardiologia (documentação conforme edital) + doutorado stricto sensu ou livre docência em cardiologia.</p>

                @foreach($campos['experiencia'] as $input => $dados)
                    @include('frontend.documentacao._campo-facultativo', [
                        'input' => $input,
                        'dados' => $dados
                    ])
                @endforeach

                <div class="submit-group">
                    <input type="submit" value="SALVAR PARA CONTINUAR">
                    @if($cadastro->documentos_facultativos_status == 'pronto para envio')
                    <input type="submit" class="salvo-envio" formaction="{{ route('documentacao.documentosFacultativosPost', ['concluir' => 1]) }}" value="SALVO PARA ENVIO">
                    @else
                    <input type="submit" formaction="{{ route('documentacao.documentosFacultativosPost', ['concluir' => 1]) }}" value="SALVAR PARA CONCLUIR">
                    @endif
                </div>
            </form>
        </div>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    <div class="home center">
        <h4>Sistema de Inscrição e acompanhamento para o exame em Cardiologia (TEC)</h4>
        <p>
            Data da Prova: 20/12/2020<br>
            Local da Prova: Ambiente on-line<br>
            Verifique o edital para mais informações
        </p>

        <div class="box-links">
            <a href="{{ url('anexos/EDITAL_CJTEC-2020.pdf') }}" target="_blank">Download do Edital de Convocação</a>
            <a href="{{ route('cadastro.login') }}">Inscrições</a>
        </div>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    <div class="sobre-a-prova interna center">
        {{-- @if(!Auth::guard('cadastro')->user()->data_envio_documentacao || Auth::guard('cadastro')->user()->status != 'valido') --}}
        <div class="main-box">
            <h2>Regulamento e Anexos Para Download</h2>
            <div class="downloads">
                <a href="{{ url('anexos/EDITAL_CJTEC-2020.pdf') }}" style="margin-bottom:6px" target="_blank">EDITAL DE CONVOCAÇÃO PARA A PROVA DE TÍTULO DE ESPECIALISTA EM CARDIOLOGIA</a>
                <a href="{{ url('anexos/ERRATA_1_EDITAL_CJTEC-2020.pdf') }}" target="_blank">1ª Errata ao Edital de Convocação para a Prova de Título de Especialista em Cardiologia 2020</a>
            </div>
        </div>
        {{-- @endif --}}

        <div class="main-box">
            <h2>Informações Sobre a Prova</h2>
            <div class="informacoes">
                <p>
                    <span>DATA:</span>
                    <span>20 de Dezembro de 2020 - domingo</span>
                </p>
                <p>
                    <span>HORÁRIO:</span>
                    <span>13h00 às 18h15 (horário de Brasília)</span>
                </p>
                <p>
                    <span>LOCAL:</span>
                    <span>Ambiente on-line</span>
                </p>
                <p>
                    <span>CONTATO:</span>
                    <span>
                        <a href="https://api.whatsapp.com/send?phone=5521992069519" class="whatsapp">(21) 99206 9519</a> (segunda a sexta das 8h às 17h)<br>
                        <a href="mailto:agendatec@cardiol.br">agendatec@cardiol.br</a>
                    </span>
                </p>
            </div>

            @if(Auth::guard('cadastro')->user()->status == 'valido' && Auth::guard('cadastro')->user()->verificaPagamento())
            <div class="prova-online">
                <p>
                    Caro(a) candidato(a)!
                </p>
                <p>
                    Para realização da prova online contaremos com a equipe Fundep/eduCAT na disponibilização do ambiente de prova, bem como, no auxílio às dúvidas da instalação do browser de prova.
                </p>
                <p>
                    Para auxiliá-lo, a equipe Fundep/eduCAT disponibiliza, os manuais do candidato e de instalação do browser. Leia-os com atenção!
                </p>
                <p>
                    Ao acessar o link de instalação, baixe o browser, instale o ambiente da prova e responda o exame que estará disponível na sua área de provas, chamado: TESTE DE INSTALAÇÃO DO BROWSER DA PROVA. Para facilitar a localização e por segurança, após baixado salve-o, preferencialmente, em sua Área de Trabalho.
                </p>
                <p>
                    É de extrema importância realizar essa instalação a fim de que falhas de conexão, equipamento ou navegação sejam identificadas, no browser.
                </p>
                <p>
                    Feito isso, é necessária a sua participação no pré-teste, onde você será monitorado por um aplicador humano e ele validará o funcionamento da câmera e microfone de seu computador, assim como a eficiência de sua internet. Para tanto, é necessário que todas as etapas da referida instalação e de tarefas previstas nos manuais anexos, sejam observadas e concluídas.
                </p>
                <p>
                    A Fundep/eduCAT disponibilizará em seu Instagram @educatbh, datas de pré-testes extras nas quais, a sua participação não é obrigatória. Eles foram criados para que você tenha mais datas disponíveis, para se familiarizar com a plataforma de provas. É necessário se inscrever previamente.
                </p>
                <p>
                    Para sua tranquilidade no dia do exame, é necessário que você participe com sucesso de pelo menos um pré-teste, portanto acompanhe as datas disponibilizadas pela Educat, instale o browser com antecedência e participe o quanto antes!
                </p>
                <p>
                    Tempo médio de duração do pré-teste: 10 minutos
                </p>
                <p>
                    A equipe Fundep/eduCAT está à sua disposição para apoiá-lo nesse processo.
                </p>
                <p>
                    Se precisar, fale conosco por meio destes canais:
                </p>
                <p>
                    (31) 3409-6755 (Fundep)<br>
                    (31) 99991-7595 (Educat)<br>
                    <a href="mailto:concurso@fundep.com.br">
                        concurso@fundep.com.br
                    </a><br>
                    <a href="https://www.instagram.com/provasonlinefundep" target="_blank">
                        https://www.instagram.com/provasonlinefundep
                    </a><br>
                    <a href="https://www.instagram.com/educatbh">
                        https://www.instagram.com/educatbh
                    </a>
                </p>

                <div class="downloads">
                    <a href="{{ url('anexos/Manual_Candidato_CJTEC.pdf?1') }}" target="_blank">MANUAL DO CANDIDATO CITEC</a>
                    <a href="{{ url('anexos/Manual_Browser_Educat-SBC.pdf') }}" target="_blank">MANUAL BROWSER EDUCAT - SBC</a>
                </div>

                <video src="{{ url('anexos/Tutorial-Instalacao.mp4') }}" controls></video>
            </div>

            <style>
                .prova-online {
                    margin-top: 40px;
                    font-size: 16px;
                    font-family: Exo\ 2,Helvetica,Arial,sans-serif;
                    font-weight: normal;
                    color: #1a345d;
                }

                @media (max-width: 768px) {
                    .prova-online {
                        font-size: 14px;
                    }
                }

                .prova-online p {
                    margin: 0;
                }

                .prova-online p a {
                    font-weight: bold;
                    word-break: break-word;
                }

                .prova-online p a:hover {
                    text-decoration: underline;
                }

                .prova-online p:not(:first-child) {
                    margin-top: 1em;
                }

                .prova-online .downloads {
                    margin-top: 30px;
                }

                .prova-online .downloads a:first-child {
                    margin-bottom: 10px;
                }

                .prova-online video {
                    width: 100%;
                    height: auto;
                    margin-top: 20px;
                }
            </style>
            @endif
        </div>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    @include('frontend.common._main-nav')

    @if(Tools::inscricoesEncerradas() && $cadastro->status != 'solicitado')
        <div class="documentacao interna center"></div>
    @else
        @if(session('erros-documentacao'))
            <div class="center erros-documentacao">
                @if(count(session('erros-documentacao')['obrigatorio']))
                    <p>
                        Preencha os seguintes campos obrigatórios:
                        <ul>
                            @foreach(session('erros-documentacao')['obrigatorio'] as $campo)
                            <li>&middot; {{ $campo }}</li>
                            @endforeach
                        </ul>
                    </p>
                @endif
                @if(count(session('erros-documentacao')['comprovantes']))
                    @foreach(session('erros-documentacao')['comprovantes'] as $erro)
                    <p>{!! $erro !!}</p>
                    @endforeach
                @endif
                @if(session('erros-documentacao')['minimo-clinica-medica'])
                    <p>Insira ao menos um documento comprobatório de Especialização em Cliníca Médica.</p>
                @endif
                @if(session('erros-documentacao')['excedente-clinica-medica'])
                    <p>O limite de comprovantes de Especialização em Clínica Médica foi excedido. Favor remover os comprovantes excedentes.</p>
                @endif
                @if(session('erros-documentacao')['minimo-cardiologia'])
                    <p>Insira ao menos um documento comprobatório de Especialização em Cardiologia.</p>
                @endif
                @if(session('erros-documentacao')['excedente-cardiologia'])
                    <p>O limite de comprovantes de Especialização em Cardiologia foi excedido. Favor remover os comprovantes excedentes.</p>
                @endif
                @if(session('erros-documentacao')['experiencia'])
                    <p>É necessário ter dois comprovantes no quesito experiência profissional: 1) Anexo I-C e 2) Comprovante de mestrado ou doutorado/livre-docência.</p>
                @endif
            </div>
        @endif

        @if(session('erros-pendencias'))
            <div class="center erros-documentacao">
                <p>
                    Adicione comprovantes para os seguintes campos:
                    <ul>
                        @foreach(session('erros-pendencias') as $campo)
                        <li>&middot; {!! $campo !!}</li>
                        @endforeach
                    </ul>
                </p>
            </div>
        @endif

        <div class="documentacao interna center">
            @if($cadastro->status == 'solicitado')
                <div class="main-box box-envio-documentacao">
                    <h2>Envio das Pendências de Documentação</h2>

                    <p>Após preenchimento e revisão de toda a documentação necessária utilize o botão abaixo para fazer o envio à SBC. Após esse envio não será mais possível fazer alterações no sistema.</p>

                    <a href="{{ route('documentacao.enviarPendencias') }}" class="btn-enviar-documentacao">ENVIAR SUBSTITUIÇÕES DE PENDÊNCIAS</a>
                </div>
            @else
                <div class="main-box box-envio-documentacao">
                    <h2>Envio da Documentação Completa</h2>

                    <p>Após preenchimento e revisão de toda a documentação necessária utilize o botão abaixo para fazer o envio à SBC. Após esse envio não será mais possível fazer alterações no sistema.</p>
                    <p>Caberá ao candidato a responsabilidade de acompanhar as pendências nesta página de inscrição do candidato e dar a continuidade ao processo.</p>
                    <p>Eventuais emails podem ser enviados para alertá-los de Documentação pendente/ Pagamento/ Cancelamento (atente-se para o fato de recebimento na caixa de lixo eletrônico). Solicitamos que veja frequentemente o sistema do candidato para o acompanhamento.</p>

                    <a href="{{ route('documentacao.enviarDocumentacao') }}" class="btn-enviar-documentacao">ENVIAR DOCUMENTAÇÃO</a>
                </div>
            @endif
        </div>
    @endif

@endsection

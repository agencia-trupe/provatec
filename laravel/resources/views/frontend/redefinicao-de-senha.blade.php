@extends('frontend.common.template')

@section('content')

    <div class="login center">
        <div class="main-box">
            <h2>Redefinição de Senha</h2>

            <form action="{{ route('cadastro.redefinir') }}" method="POST">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erro">
                        @foreach($errors->all() as $error)
                        {{ $error }}<br>
                        @endforeach
                    </div>
                @endif

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}" required>
                <input type="password" name="password" placeholder="senha" required>
                <input type="password" name="password_confirmation" placeholder="repetir senha" required>
                <input type="submit" value="REDEFINIR">
            </form>
        </div>
    </div>

@endsection

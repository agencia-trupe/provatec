<!DOCTYPE html>
<html>
<head>
    <title>Sua documentação foi enviada para a SBC – Inscrição para a Prova de Título de Especialista em Cardiologia 2020</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        Você realizou o envio da documentação em {{ Tools::formataData($data) }} e gerou o número de inscrição {{ $inscricao }}.
    </p>
    <p>Aguarde a avaliação da sua documentação.</p>
    <p><strong>Pendência de documentação:</strong> Caso se identifique alguma pendência na documentação o candidato será informado por meio deste sistema de inscrição online e terá 10 (dez) dias corridos para regularizar todas as pendências. Em caso de não regularização e findo o prazo estipulado, a inscrição será automaticamente cancelada.</p>
    <p><strong>Concorrente/aguardando pagamento:</strong> Caso a documentação esteja de acordo com o descrito em edital haverá a informação no sistema de aguardando pagamento em que será disponibilizado o link para a efetivação deste em até 5 (cinco) dias úteis. Com a efetivação, o candidato passará para o status: Concorrente/Inscrição Paga.</p>
    <p><strong>Inscrição indefirida:</strong> Cancelamento por não enviar os documentos obrigatórios, conforme descrito em edital ou não enviar no período estipulado no edital.</p>
    <p><em>Caberá ao candidato a responsabilidade de acompanhar essas pendências no sistema de inscrição do candidato.</em></p>
    <p>Favor verificar o edital para mais informações.</p>
    <p>Contato: <a href="mailto:agendatec@cardiol.br">agendatec@cardiol.br</a></p>
</body>
</html>

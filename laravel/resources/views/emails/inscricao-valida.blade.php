<!DOCTYPE html>
<html>
<head>
    <title>Prova do TEC 2020 – Concorrente | Aguardando pagamento</title>
    <meta charset="utf-8">
</head>
<body>
    <p>Prezado(a) candidato(a),</p>
    <p>Sua documentação foi analisada e não existem pendências.</p>
    <p>
        Acesse o sistema provatec.com.br para acessar o link de pagamento de sua inscrição e acompanhe seu status no mesmo sistema.
    </p>
    <p>
        Candidatos sócios adimplentes da SBC, por favor utilizem o seu email @cardiol para o pagamento do valor com desconto para sócios da SBC. Para informações sobre como se tornar associado ou quitar sua anuidade verifique os prazos e os passos no edital.
    </p>
    <p>
        <em>Equipe de análise da documentação da Prova do TEC 2020</em><br>
        Dúvidas: <a href="mailto:agendatec@cardiol.br">agendatec@cardiol.br</a>
    </p>
</body>
</html>

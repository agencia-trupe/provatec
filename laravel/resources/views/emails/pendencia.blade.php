<!DOCTYPE html>
<html>
<head>
    <title>Prova do TEC 2020 - Pendência de documentação</title>
    <meta charset="utf-8">
</head>
<body>
    <p>Prezado(a) candidato(a),</p>
    <p>
        Sua documentação foi analisada e existem pendências. Acesse o sistema para resolvê-las em até 10 dias.
    </p>
    <p>
        <em>Equipe de análise da documentação da Prova do TEC 2020</em><br>
        Dúvidas: <a href="mailto:agendatec@cardiol.br">agendatec@cardiol.br</a>
    </p>
</body>
</html>

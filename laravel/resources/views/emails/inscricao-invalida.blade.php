<!DOCTYPE html>
<html>
<head>
    <title>Prova do TEC 2020 - Inscrição Indeferida</title>
    <meta charset="utf-8">
</head>
<body>
    <p>Prezado(a) candidato(a),</p>
    <p>
        Sua inscrição foi indeferida. Acesse o sistema do candidato para mais informações. <a href="http://www.provatec.com.br">www.provatec.com.br</a>
    </p>
    <p>
        <em>Equipe de análise da documentação da Prova do TEC 2020</em><br>
        Dúvidas: <a href="mailto:agendatec@cardiol.br">agendatec@cardiol.br</a>
    </p>
</body>
</html>

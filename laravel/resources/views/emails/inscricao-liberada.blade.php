<!DOCTYPE html>
<html>
<head>
    <title>Prova do TEC 2020 - Revalidação em sua inscrição</title>
    <meta charset="utf-8">
</head>
<body>
    <p>Prezado(a) candidato(a),</p>
    <p>
        Sua inscrição foi reaberta. Acesse o sistema do candidato para rever suas documentações e alterar o que for necessário dentro do prazo previsto em edital.
    </p>
    <p>
        Página do candidato:
        <a href="http://www.provatec.com.br">www.provatec.com.br</a>
    </p>
    <p>
        <em>Equipe de análise da documentação da Prova do TEC 2020</em><br>
        Dúvidas: <a href="mailto:agendatec@cardiol.br">agendatec@cardiol.br</a>
    </p>
</body>
</html>

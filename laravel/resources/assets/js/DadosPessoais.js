export default function DadosPessoais() {
    var initFileUploadDadosPessoais = function() {
        $('.dados-pessoais .btn-adicionar-comprovante input').each(function() {
            var errors;

            $(this).fileupload({
                dataType: 'json',
                start: function(e, data) {
                    errors = [];
                    $(this).parent().addClass('loading');
                    $('.dados-pessoais input[type=submit]').prop('disabled', true);
                },
                done: function(e, data) {
                    $(this).parent().next('.files-wrapper').append(
                        `
                            <span>
                                <a href="${$('base').attr('href')}/comprovantes/${data.result.filename}" target="_blank">${data.result.filename}</a>
                                <input type="hidden" name="${data.result.inputname}[]" value="${data.result.filename}">
                                <a href="#" class="excluir">X</a>
                            </span>
                        `
                    );
                },
                stop: function() {
                    if (errors.length) {
                        alert(errors.join('\n'));
                    }
                    $(this).parent().removeClass('loading');
                    if (!$('.loading').length) {
                        $('.dados-pessoais input[type=submit]').prop('disabled', false);
                    }
                },
                fail: function(e, data) {
                    var status       = data.jqXHR.status,
                        errorMessage = (status == '422' ? 'O arquivo deve ser uma imagem ou PDF.' : 'Erro interno do servidor.'),
                        response     = data.files[0].name + ' - Erro: ' + errorMessage;

                    errors.push(response);
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    };

    initFileUploadDadosPessoais();

    $(document).on('click', '.files-wrapper span .excluir', function(event) {
        event.preventDefault();

        var $row                   = $(this).parent().parent().parent();
        var wrapper                = $(this).parent().parent();
        var permiteUmArquivoPorVez = !!$row.find('.one-file-hide').length;

        $(this).parent().fadeOut().remove();

        if (permiteUmArquivoPorVez && wrapper.children().length == 0) {
            $row.find('.btn-adicionar-comprovante').show();
        }
    });

    $('.dados-pessoais .btn-add-row').click(function(event) {
        event.preventDefault();

        var inputName = $(this).data('input'),
            formUrl   = $(this).data('url');

        var nextIndex = Number($('input[name^=' + inputName + '_grupo]').last().val()) + 1;

        var template = `
            <div class="row">
                <input type="hidden" name="crm_grupo[]" value="${nextIndex}">
                <input type="text" name="crm_numero_${nextIndex}" placeholder="CRM">
                <div class="btn-adicionar-comprovante">
                    <span></span>
                    Adicionar comprovante
                    <input type="file" name="arquivo" class="fileupload" multiple data-url="${formUrl}/${nextIndex}">
                </div>
                <div class="files-wrapper"></div>
            </div>
        `;

        $(template).insertBefore($(this));

        initFileUploadDadosPessoais();
    });
};

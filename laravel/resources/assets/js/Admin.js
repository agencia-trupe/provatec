export default function Admin() {
    $('.table-link').click(function(event) {
        window.location = $(this).attr('href');
    });

    $('.admin-open-substituicao').click(function(event) {
        event.preventDefault();
        $(this).addClass('active').parent().next().slideDown();
    });

    $('.btn-reavaliar a').click(function(event) {
        event.preventDefault();
        $(this).addClass('active').parent().next().slideDown();
    });
};

export default function Documentacao() {
    const initFileUploadDocumentacao = function () {
        $('.documentacao .btn-adicionar-comprovante input').each(function () {
            let errors;

            $(this).fileupload({
                dataType: 'json',
                start(e, data) {
                    errors = [];
                    $(this).parent().addClass('loading');
                    $('.documentacao input[type=submit]').prop('disabled', true);
                },
                done(e, data) {
                    $(this).parent().next('.files-wrapper').append(
                        `
                            <span>
                                <a href="${$('base').attr('href')}/comprovantes/${data.result.filename}" target="_blank">${data.result.filename}</a>
                                <input type="hidden" name="${data.result.inputname}[]" value="${data.result.filename}">
                                <a href="#" class="excluir">X</a>
                            </span>
                        `,
                    );

                    if ($(this).hasClass('one-file-hide')) {
                        if ($(this).parent().next('.files-wrapper').children().length >= 1) {
                            $(this).parent().hide();
                        }
                    }
                },
                stop() {
                    if (errors.length) {
                        alert(errors.join('\n'));
                    }
                    $(this).parent().removeClass('loading');
                    if (!$('.loading').length) {
                        $('.documentacao input[type=submit]').prop('disabled', false);
                    }
                },
                fail(e, data) {
                    let status = data.jqXHR.status,
                        errorMessage = (status == '422' ? 'O arquivo deve ser uma imagem ou PDF.' : 'Erro interno do servidor.'),
                        response = `${data.files[0].name} - Erro: ${errorMessage}`;

                    errors.push(response);
                },
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    };

    initFileUploadDocumentacao();

    $('.documentacao .btn-add-row').click(function (event) {
        event.preventDefault();

        let inputName = $(this).data('input'),
            formUrl = $(this).data('url'),
            limite = $(this).data('limite'),
            anoPlaceholder = $(this).data('ano-placeholder');

        if ($(`input[name^=${inputName}_grupo]`).length >= limite) return;

        const nextIndex = Number($(`input[name^=${inputName}_grupo]`).last().val()) + 1;

        let templateOptions = '';

        $(this).parent().prev().find('select').first().find('option:not(:first)').contents().map((el, data) => {
            templateOptions += `<option value="${data.data}">${data.data}</option>`;
        });

        const template = `
            <div class="row">
                <input type="hidden" name="${inputName}_grupo[]" value="${nextIndex}">
                <select name="ano_${inputName}_${nextIndex}">
                    <option value="" selected>${anoPlaceholder}</option>
                    ${templateOptions}
                </select>
                <div class="btn-adicionar-comprovante">
                    <span></span>
                    Adicionar comprovante
                    <input type="file" name="arquivo" class="fileupload one-file-hide" data-url="${formUrl}/${nextIndex}">
                </div>
                <div class="files-wrapper"></div>
            </div>
        `;

        $(template).insertBefore($(this));

        if ($(`input[name^=${inputName}_grupo]`).length >= limite) {
            $(this).fadeOut();
        }

        initFileUploadDocumentacao();
    });

    $('.limpar-comprovante-especializacao').click(function (e) {
        e.preventDefault();

        const campoKey = $(this).parent().data('campo');

        $(`.row[data-campo="${campoKey}"]`).find('select, input').val('').trigger('change');
        $(`.row[data-campo="${campoKey}"]`).find('.files-wrapper').empty();
        $(this).remove();
    });
}

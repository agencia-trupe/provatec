import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Mascaras from './Mascaras';
import CEP from './CEP';
import DadosPessoais from './DadosPessoais';
import Documentacao from './Documentacao';
import Admin from './Admin';
import Instituicoes from './Instituicoes';

AjaxSetup();
MobileToggle();
Mascaras();
CEP();
DadosPessoais();
Documentacao();
Admin();
Instituicoes();

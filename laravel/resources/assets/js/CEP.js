export default function CEP() {
    function limpaInputsCEP() {
        $('input[name=endereco]').val('');
        $('input[name=bairro]').val('');
        $('input[name=cidade]').val('');
        $('input[name=uf]').val('');
    }

    $('input[name=CEP]').blur(function() {
        var cep = $(this).val().replace(/\D/g, '');

        if (cep != '') {
            var validacaoCep = /^[0-9]{8}$/;

            if(validacaoCep.test(cep)) {
                $('input[name=endereco]').attr('disabled', true);
                $('input[name=bairro]').attr('disabled', true);
                $('input[name=cidade]').attr('disabled', true);
                $('input[name=uf]').attr('disabled', true);

                $.getJSON('https://viacep.com.br/ws/'+ cep +'/json/?callback=?', function(dados) {
                    if (!('erro' in dados)) {
                        $('input[name=endereco]').attr('disabled', false).val(dados.logradouro);
                        $('input[name=bairro]').attr('disabled', false).val(dados.bairro);
                        $('input[name=cidade]').attr('disabled', false).val(dados.localidade);
                        $('input[name=uf]').attr('disabled', false).val(dados.uf);
                        $('input[name=numero]').focus();
                    } else {
                        limpaInputsCEP();
                        $('input[name=endereco]').attr('disabled', false);
                        $('input[name=bairro]').attr('disabled', false);
                        $('input[name=cidade]').attr('disabled', false);
                        $('input[name=uf]').attr('disabled', false);
                        alert('CEP não encontrado.');
                    }
                });
            } else {
                limpaInputsCEP();
                alert('CEP inválido.');
            }
        } else {
            limpaInputsCEP();
        }
    });
};

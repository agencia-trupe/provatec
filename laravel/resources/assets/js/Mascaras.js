export default function Mascaras() {
    Inputmask.extendDefaults({
        jitMasking: true
    });

    $('.mask-cpf').inputmask('999.999.999-99');

    $('.mask-cep').inputmask('99999-999');

    $('.mask-telefone').inputmask({
        mask: ['(99) 9999-9999', '(99) 99999-9999'],
        keepStatic: true
    });

    $('.mask-nascimento').inputmask('99/99/9999');
};

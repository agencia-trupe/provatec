export default function Instituicoes() {
    $('.select-outra').change(function () {
        const $this = $(this);
        const $outra = $this.closest('.row').next('.row-outra');

        if ($(this).val() === 'OUTRA') {
            return $outra.show().find('input').val('');
        }

        return $outra.hide();
    });

    $('.flex-uf-instituicao select[name^=uf_]').change(function () {
        const $this = $(this);
        const $instituicoes = $this.next('select[name^=instituicao_]');
        const INSTITUICOES = window.PROVATEC_INSTITUICOES;

        const $options = [
            'Instituição (selecionar)',
            ...(INSTITUICOES[$this.val()] || []),
            'OUTRA',
        ].map(option => `<option value="${option}">${option}</option>`);

        $instituicoes
            .attr('disabled', !$this.val())
            .html($options)
            .trigger('change');
    });
}

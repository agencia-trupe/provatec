<?php

namespace App\Helpers;

use Carbon\Carbon;

use App\Models\Campos;
use App\Models\CadastroExcecao;

class Tools
{
    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        if (!$data) {
            return;
        }

        return Carbon::parse($data)->format('d/m/Y');
    }

    public static function formataDataHorario($data)
    {
        if (!$data) {
            return;
        }

        return str_replace('-', '&middot;', Carbon::parse($data)->format('d/m/Y - H:i:s'));
    }

    public static function dataExtenso($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' de ' . $meses[(int) $mes - 1] . ' de ' . $ano;
    }

    public static function faixaAnos($limite = null, $max = null)
    {
        $max = $max ?: date('Y');

        if ($limite || $limite === 0) {
            return array_reverse(range($max - $limite, $max));
        }

        return array_reverse(range(1970, $max));
    }

    public static function faculdades()
    {
        return [
            'Centro de Educação Superior de Guanambi – Cesg',
            'Centro de Ensino Superior de Valença – Cesva',
            'Centro Universitário Assis Gurgacz – FAG',
            'Centro Universitário Barão de Mauá – CBM',
            'Centro Universitário Católica Salesiano Auxilium- Unisalesiano',
            'Centro Universitário Cesmac',
            'Centro Universitário Ceuni – Fametro',
            'Centro Universitário Christus – Unichristus',
            'Centro Universitário das Faculdades Associadas de Ensino – FAE – Unifae',
            'Centro Universitário de Adamantina – FAI',
            'Centro Universitário de Anápolis – Unievangélica',
            'Centro Universitário de Belo Horizonte – UNI-BH',
            'Centro Universitário De Brasília – UNICEUB',
            'Centro Universitário de Caratinga – Unec',
            'Centro Universitário de Gurupi – Unirg',
            'Centro Universitário de João Pessoa – Unipê',
            'Centro Universitário de Maringá – Unicesumar',
            'Centro Universitário de Mineiros – Unifimes',
            'Centro Universitário de Patos de Minas – Unipam',
            'Centro Universitário de Votuporanga – Unifev',
            'Centro Universitário de Várzea Grande – Univag',
            'Centro Universitário do Espírito Santo – Unesc',
            'Centro Universitário do Estado do Pará – Cesupa',
            'Centro Universitário e Volta Redonda – Unifoa',
            'Centro Universitário Estácio de Ribeirão Preto – Estácio Ribeirão PRE',
            'Centro Universitário Faminas – Unifaminas',
            'Centro Universitário Franciscano – Unifra',
            'Centro Universitário Ingá',
            'Centro Universitário Inta – Uninta',
            'Centro Universitário Integrado de Campo Mourão',
            'Centro Universitário Lusíada – Unilus',
            'Centro universitário Mauricio de Nassau – Uninassau',
            'Centro Universitário Municipal de Franca – Uni-Facef',
            'Centro Universitário Padre Albino',
            'Centro Universitário Presidente Tancredo de Almeida Neves – Uniptan',
            'Centro Universitário Redentor – Facredentor',
            'Centro Universitário Serra dos Órgãos – Unifeso',
            'Centro Universitário São Camilo – São Camilo',
            'Centro Universitário São Lucas –Unisl',
            'Centro Universitário Tiradentes – FITS',
            'Centro Universitário Tocantinense Presidente Antônio Carlos – Unitpac',
            'Centro Universitário Uninovafapi',
            'Claretiano – Faculdade – Claretianorc',
            'Escola Bahiana de Medicina e Saúde Pública – EBMSP',
            'Escola de Medicina Souza Marques da Fundação Técnico-Educacional Souza Marques – EMSM',
            'Escola Superior de Ciências da Santa Casa de Misericórdia de Vitória – Emescam',
            'Escola Superior De Ciências Da Saúde – ESCS',
            'Faculdade Ages de Medicina',
            'Faculdade Alfredo Nasser – FAN',
            'Faculdade Atenas Sete Lagoas',
            'Faculdade Atenas',
            'Faculdade Barão do Rio Branco –AC',
            'Faculdade Brasileira – Multivix Vitória',
            'Faculdade Campo Real – Campo Real',
            'Faculdade Ceres – Faceres',
            'Faculdade das Américas – FAM',
            'Faculdade de Ciências Agrárias e da Saúde – FAS',
            'Faculdade de Ciências Biomédicas de Cacoal – Facimed',
            'Faculdade de Ciências da Saúde de Barretos Dr. Paulo Prata – FCSB',
            'Faculdade de Ciências Gerenciais de Manhuaçu – Facig',
            'Faculdade de Ciências Humanas, Econômicas e da Saúde – Fahesa/ITPAC Palmas',
            'Faculdade de Ciências Humanas, Exatas e da Saúde do Piauí – Fahesp',
            'Faculdade de Ciências Médicas da Paraíba – FCM-PB',
            'Faculdade de Ciências Médicas da Santa Casa de São Paulo – FCMSCSP',
            'Faculdade de Ciências Médicas de Campina Grande – FCM',
            'Faculdade de Ciências Médicas de Minas Gerais – FCMMG',
            'Faculdade de Ciências Médicas de São José dos Campos',
            'Faculdade de Ciências Médicas e da Saúde de Juiz de Fora – FCMS/JF',
            'Faculdade de Medicina de Barbacena – Fame',
            'Faculdade de Medicina de Campos – FMC',
            'Faculdade de Medicina de Itajubá – FMIT',
            'Faculdade de Medicina de Jundiaí – FMJ',
            'Faculdade de Medicina de Marília – Famema',
            'Faculdade de Medicina de Olinda – FMO',
            'Faculdade de Medicina de Petrópolis – FMP',
            'Faculdade de Medicina de São José do Rio Preto – Famerp',
            'Faculdade de Medicina do ABC – FMABC',
            'Faculdade de Medicina Estácio de Juazeiro do Norte – Estácio FMJ',
            'Faculdade de Medicina Nova Esperança – Famene',
            'Faculdade de medicina – ITPAC',
            'Faculdade de Minas BH – Faminas-BH',
            'Faculdade de Minas – Faminas',
            'Faculdade de Pato Branco – Fadep',
            'Faculdade de Saúde e Ecologia Humana – Faseh',
            'Faculdade de Saúde Santo Agostinho de Vitória da Conquista – Fasa',
            'Faculdade de tecnologia e ciências – FTC Salvador',
            'Faculdade Dinâmica do Vale do Piranga – Fadip',
            'Faculdade Evangélica do Paraná – Fepar',
            'Faculdade Integral Diferencial – Facid',
            'Faculdade Israelita de Ciências da Saúde Albert Einstein – Ficsae',
            'Faculdade Meridional – Imed',
            'Faculdade Metropolitana da Amazônia – Famaz',
            'Faculdade Mineirense – Fama',
            'Faculdade Pernambucana de Saúde – FPS',
            'Faculdade Presidente Antônio Carlos – Fapac',
            'Faculdade Santa Marcelina – FASM',
            'Faculdade Santa Maria – FSM',
            'Faculdade São Francisco de Barreiras – Fasb',
            'Faculdade São Leopoldo mandic de araras – Slmandic-Araras',
            'Faculdade São Leopoldo Mandic',
            'Faculdade São Lucas – FSL',
            'Faculdade Ubaense Ozanam Coelho – Fagoc',
            'Faculdades Integradas Aparício Carvalho – Fimca',
            'Faculdades Integradas da União Educacional do Planalto Central – FACIPLAC',
            'Faculdades Integradas de Patos – FIP',
            'Faculdades Integradas Padre Albino – FIPA',
            'Faculdades Integradas Pitágoras – FIP-MOC',
            'Faculdades Pequeno Príncipe – FPP',
            'Fundação Educacional D. André Arcoverde – CESVA',
            'Fundação Universidade Federal da Grande Dourados – UFGD',
            'Fundação Universidade Federal de Ciências da Saúde de Porto Alegre – UFCSPA',
            'Fundação Universidade Federal de Rondônia – Unir',
            'Fundação Universidade Federal do Pampa – Unipampa',
            'Fundação Universidade Federal do Tocantins – UFT',
            'Fundação Universidade Federal do Vale do São Francisco – Univasf',
            'Hospital do Grande Rio (Unigranrio) Mario Lioni',
            'Instituto de Ciências da Saúde – ICS',
            'Instituto Metropolitano de Ensino Superior – Imes',
            'Instituto Municipal de Ensino Superior de Assis – Imesa',
            'Instituto Máster de Ensino Presidente Antônio Carlos – Imepac',
            'Instituto Presidente Tancredo Neves – IPTAN',
            'Pontifícia Universidade Católica de Campinas – PUC-Campinas',
            'Pontifícia Universidade Católica de Goiás – PUC Goiás',
            'Pontifícia Universidade Católica de Minas Gerais – PUC Minas',
            'Pontifícia Universidade Católica de São Paulo – PUCSP',
            'Pontifícia Universidade Católica do Paraná – PUCPR',
            'Pontifícia Universidade Católica do Rio Grande do Sul – PUCRS',
            'União das Faculdades dos Grandes Lagos – Unilago',
            'Universidade Anhanguera – Uniderp',
            'Universidade Anhembi Morumbi – UAM',
            'Universidade Brasil',
            'Universidade Camilo Castelo Branco – UNICASTELO',
            'Universidade Castelo Branco – UCB',
            'Universidade Católica de Pelotas – Ucpel',
            'Universidade Católica de Pernambuco – Unicap',
            'Universidade Cidade de São Paulo – Unicid',
            'Universidade Comunitária da Região de Chapecó – Unichapecó',
            'Universidade da Região de Joinville – Univille',
            'Universidade de Araraquara – Uniara',
            'Universidade De Brasília – UNB',
            'Universidade de Caxias do Sul – UCS',
            'Universidade de Cuiabá – UNIC/Unime',
            'Universidade de Fortaleza – Unifor',
            'Universidade de Franca – Unifran',
            'Universidade de Itauna – UI',
            'Universidade de Marília – Unimar',
            'Universidade de Mogi das Cruzes – UMC',
            'Universidade de Oeste Paulista – Unoeste',
            'Universidade de Passo Fundo – UPF',
            'Universidade de Pernambuco – UPE',
            'Universidade de Ribeirão Preto – Unaerp',
            'Universidade de Rio Verde – Fesurv/Unirv',
            'Universidade de Santa Cruz do Sul – Unisc',
            'Universidade de São Paulo – USP',
            'Universidade de Taubaté – Unitau',
            'Universidade de Uberaba – Uniube',
            'Universidade de Vassouras',
            'Universidade do Ceuma – Uniceuma',
            'Universidade do Contestado – UNC',
            'Universidade do Estado da Bahia – Uneb',
            'Universidade do Estado de Minas Gerais – UEMG',
            'Universidade do Estado do Amazonas – UEA',
            'Universidade do Estado do Mato Grosso –Unemat',
            'Universidade do Estado do Pará – Uepa',
            'Universidade do Estado do Rio de Janeiro – UERJ',
            'Universidade do Estado do Rio Grande do Norte – UERN',
            'Universidade do Extremo Sul Catarinense – Unesc',
            'Universidade do Grande Rio Professor José de Souza Herdy – Unigranrio',
            'Universidade do Oeste de Santa Catarina – Unoesc',
            'Universidade do Planalto Catarinense – Uniplac',
            'Universidade do Sul de Santa Catarina – Unisul',
            'Universidade do Vale do Itajaí – Univali',
            'Universidade do Vale do Rio dos Sinos – Unisinos',
            'Universidade do Vale do Sapucaí – Univás',
            'Universidade do Vale do Taquari – Univates',
            'Universidade Estadual de Campinas – Unicamp',
            'Universidade Estadual de Ciências da Saúde de Alagoas – Uncisal',
            'Universidade Estadual De Feira De Santana – UEFS',
            'Universidade Estadual de Londrina – UEL',
            'Universidade Estadual de Maringá – UEM',
            'Universidade Estadual de Mato Grosso do Sul – UEMS',
            'Universidade Estadual de Montes Claros – Unimontes',
            'Universidade Estadual de Ponta Grossa – EUPG',
            'Universidade Estadual De Ponta Grossa – UEPG',
            'Universidade Estadual de Santa Cruz – UESC',
            'Universidade Estadual do Ceará – UECE',
            'Universidade Estadual do Maranhão – UEMA',
            'Universidade Estadual do Oeste do Paraná – Unioeste',
            'Universidade Estadual do Piauí – Uespi',
            'Universidade Estadual do Sudoeste da Bahia – UESB',
            'Universidade Estadual Paulista Júlio de Mesquita Filho – Unesp',
            'Universidade Estácio de Sá – Unesa',
            'Universidade Federal da Bahia – UFBA',
            'Universidade Federal da Fronteira Sul – UFFS',
            'Universidade Federal da Integração Latino-Americana – Unila',
            'Universidade Federal da Paraíba – UFPB',
            'Universidade Federal de Alagoas – Ufal',
            'Universidade Federal de Alfenas – Unifal-MG',
            'Universidade Federal de Campina Grande – UFCG',
            'Universidade Federal de Goiás – UFG',
            'Universidade Federal de Juiz de Fora – UFJF',
            'Universidade Federal de Lavras – Ufla',
            'Universidade Federal de Mato Grosso do Sul – UFMS',
            'Universidade Federal de Mato Grosso – UFMT',
            'Universidade Federal de Minas Gerais – UFMG',
            'Universidade Federal de Ouro Preto – Ufop',
            'Universidade Federal de Pelotas – UFPEL',
            'Universidade Federal de Pernambuco – UFPE',
            'Universidade Federal de Roraima – UFRR',
            'Universidade Federal de Santa Catarina – UFSC',
            'Universidade Federal de Santa Maria – UFSM',
            'Universidade Federal de Sergipe –UFS',
            'Universidade Federal de São Carlos – Ufscar',
            'Universidade Federal de São João Del Rei – UFSJ',
            'Universidade Federal De São Paulo – UNIFESP',
            'Universidade Federal de Uberlândia – UFU',
            'Universidade Federal de Viçosa – UFV',
            'Universidade Federal do Acre – UFAC',
            'Universidade Federal do Amapá – Unifap',
            'Universidade Federal do Amazonas –Ufam',
            'Universidade Federal do Cariri – UFCA',
            'Universidade Federal do Ceará – UFC',
            'Universidade Federal do Espírito Santo – Ufes',
            'Universidade Federal do Estado do Rio de Janeiro – Unirio',
            'Universidade Federal do Maranhão – UFMA',
            'Universidade Federal do Oeste da Bahia – UFOB',
            'Universidade Federal do Paraná – UFPR',
            'Universidade Federal do Pará – UFPA',
            'Universidade Federal do Piauí – UFPI',
            'Universidade Federal do Recôncavo da Bahia – UFRB',
            'Universidade Federal do Rio de Janeiro – UFRJ',
            'Universidade Federal do Rio Grande do Norte – UFRN',
            'Universidade Federal do Rio Grande do Sul – UFRGS',
            'Universidade Federal do Rio Grande – FURG',
            'Universidade Federal do Sul da Bahia – Ufesba',
            'Universidade Federal do Triângulo Mineiro – UFTM',
            'Universidade Federal dos Vales do Jequitinhonha e Mucuri – UFVJM',
            'Universidade Federal Fluminense – UFF',
            'Universidade Federal Rural do Semi-Árido – Ufersa',
            'Universidade Feevale',
            'Universidade Iguaçu – Unig',
            'Universidade José do Rosário Vellano – Unifenas',
            'Universidade Luterana do Brasil – Ulbra',
            'Universidade Metropolitana De Santos – UNIMES',
            'Universidade Municipal De São Caetano Do Sul – USCS',
            'Universidade Nilton Lins – Uniniltonlins',
            'Universidade Nove de Julho – Uninove',
            'Universidade Paranaense – Unipar',
            'Universidade Positivo – UP',
            'Universidade Potiguar – UNP',
            'Universidade Presidente Antônio Carlos – Unipac',
            'Universidade Regional de Blumenau – FURB',
            'Universidade Regional Integrada do Alto Uruguai e das Missões – URI',
            'Universidade Salvador – Unifacs',
            'Universidade Santo Amaro – Unisa',
            'Universidade Severino Sombra – USS',
            'Universidade São Francisco – USF ',
            'Universidade Tiradentes – Unit',
            'Universidade Vale do Rio Doce – Univale',
            'Universidade Vale do Rio Verde – Unincor',
            'Universidade Vila Velha – UVV',
            'OUTRA'
        ];
    }

    public static function instituicoes()
    {
        return [
            'AC' => [
                "FUNDACAO EDUCACIONAL JAYME DE ALTAVILA-FEJAL",
                "HOSPITAL DO CORAÇÃO DE ALAGOAS",
                "SANTA CASA DE MISERICÓRDIA DE MACEIÓ",
            ],
            'AL' => [],
            'AP' => [],
            'AM' => [
                "HOSPITAL UNIVERSITÁRIO GETÚLIO VARGAS FACULDADE DE MEDICINA UFAM",
            ],
            'BA' => [
                "COMPLEXO HOSPITALAR UNIVERSITÁRIO PROF EDGARD SANTOS UFBA",
                "FUNDAÇÃO BAHIANA DE CARDIOLOGIA",
                "FUNDACAO HOSPITALAR DE FEIRA DE SANTANA",
                "HOSPITAL ANA NERI",
                "HOSPITAL ANA NERY",
                "HOSPITAL SANTA IZABEL SANTA CASA DE MISERICORDIA DA BAHIA",
                "HOSPITAL SÃO RAFAEL BA",
                "HOSPITAL SÃO RAFAEL",
                "HOSPITAL UNIVERSITÁRIO PROFESSOR EDGAR SANTOS",
                "MUNICIPIO DE SALVADOR",
                "REAL SOCIEDADE PORTUGUESA DE BENEF 16 DE SETEMBRO",
                "SANTA CASA DE MISERICORDIA DE VITORIA DA CONQUISTA",
                "UNIVERSIDADE FEDERAL DA BAHIA",
                "UNIVERSIDADE FEDERAL DO RECONCAVO DA BAHIA - UFRB",
            ],
            'CE' => [
                "FUNDACAO UNIVERSIDADE ESTADUAL DO CEARA FUNECE",
                "HOSPITAL DE MESSEJANA DR CARLOS ALBERTO STUDART GOMES",
                "HOSPITAL UNIVERSITÁRIO WALTER CANTIDIO UFC",
                "SECRETARIA DA SAUDE DO ESTADO DO CEARA",
                "UNIVERSIDADE REGIONAL DO CARIRI URCA",
            ],
            'DF' => [
                "DISTRITO FEDERAL SECRETARIA DE SAUDE",
                "DISTRITO FEDERAL SECRETARIA DE SAÚDE",
                "FUNDACAO HOSPITALAR DO DISTRITO FEDERAL",
                "FUNDAÇÃO OSWALDO CRUZ",
                "FUNDACAO UNIVERSITARIA DE CARDIOLOGIA",
                "HOSPITAL DAS FORÇAS ARMADAS BRASILIA DF",
                "HOSPITAL DE BASE DO DISTRITO FEDERAL",
                "HOSPITAL UNIVERSITÁRIO DE BRASÍLIA HUB \ UNB",
                "ISMEP - INSTITUTO SANTA MARTA DE ENSINO E PESQUISA",
            ],
            'ES' => [
                "ASSOCIACAO EVANGELICA BENEFICENTE ESPIRITO-SANTENSE - AEBES - HOSPITAL EVANGÉLICO DE VILA VELHA",
                "CENTRO DE CIÊNCIAS DA SAÚDE DA UFES HOSPITAL UNIVERSITÁRIO CASSIANO ANTONIO MORAES",
                "FUNDACAO BENEFICENTE RIO DOCE",
                "HOSPITAL EVANGELICO DE CACHOEIRO DE ITAPEMIRIM",
                "HOSPITAL EVANGÉLICO DE CACHOEIRO DE ITAPEMIRIM",
                "HOSPITAL METROPOLITANO S/A",
                "HOSPITAL METROPOLITANO",
                "IRMANDADE DA SANTA CASA DE MISERICORDIA DE VITORIA",
                "SOC EDUC DO ESP SANTO UNIDADE DE V VELHA ENSINO SUPERIO",
                "UNIAO DE EDUCACAO E CULTURA GILDASIO AMADO",
                "UNIÃO DE EDUCACAO E CULTURA GILDASIO AMADO",
            ],
            'GO' => [
                "ASSOCIACAO EDUCATIVA EVANGELICA",
                "HOSPITAL DAS CLÍNICAS DA UFG GOIÂNIA GO",
                "HOSPITAL DE OLHOS DE APARECIDA DE GOIANIA LTDA",
                "HOSPITAL DE URGÊNCIAS DE GOIÂNIA - HUGO",
                "HOSPITAL ESTADUAL GERAL DE GOIÂNIA DR. ALBERTO RASSI",
                "HOSPITAL SAO BERNARDO LTDA",
                "SANTA CASA DE MISERICÓRDIA DE GOIÂNIA",
                "UNI EVANGÉLICA DE ANÁPOLIS",
            ],
            'MA' => [
                "HOSPITAL UNIVERSITÁRIO DA UNIVERSIDADE FEDERAL DO MARANHÃO",
                "UDI HOSPITAL - EMPREENDIMENTOS MEDICO HOSPITALARES DO MARANHAO LTDA.",
            ],
            'MG' => [
                "ASSOCIACAO EVANGELICA BENEFICENTE DE MINAS GERAIS",
                "CENTRO BARBACENENSE DE ASSISTENCIA MEDICA E SOCIAL",
                "FACULDADE DE CIÊNCIAS MÉDICAS E DA SAÚDE DE JUIZ DE FORA",
                "FACULDADE DE MEDICINA DA UNIVERSIDADE FEDERAL DE UBERLÂNDIA",
                "FACULDADE DE MEDICINA DE ITAJUBÁ",
                "FUNDACAO DE ENSINO SUPERIOR DO VALE DO SAPUCAI",
                "FUNDACAO FELICE ROSSO",
                "FUNDACAO GERALDO CORREA",
                "FUNDAÇÃO HOSPITALAR DE MONTES CLAROS – HOSPITAL AROLDO TOURINHO",
                "FUNDAÇAO HOSPITALAR DE MONTES CLAROS HOSPITAL AROLDO TOURINHO",
                "FUNDACAO HOSPITALAR DO ESTADO DE MINAS GERAIS",
                "FUNDACAO HOSPITALAR SAO FRANCISCO DE ASSIS - FHSFA",
                "FUNDAÇÃO HOSPITALAR SÃO FRANCISCO DE ASSIS",
                "FUNDACAO JOSE BONIFACIO LAFAYETTE DE ANDRADA",
                "FUNDO MUNICIPAL DE SAUDE DE GOVERNADOR VALADARES",
                "HOSPITAL BIOCOR INSTITUTO",
                "HOSPITAL BOM SAMARITANO",
                "HOSPITAL DAS CLINICAS DA UFMG",
                "HOSPITAL DAS CLÍNICAS SAMUEL LIBANIO POUSO ALEGRE MG",
                "HOSPITAL DAS CLÍNICAS SAMUEL LIBÂNIO",
                "HOSPITAL E MATERNIDADE SAO JOSE",
                "HOSPITAL E MATERNIDADE THEREZINHA DE JESUS",
                "HOSPITAL FELICIO ROCHO MG (MANTENEDORA FUNDAÇÃO FELICE ROSSO)",
                "HOSPITAL FELICIO ROCHO",
                "HOSPITAL GOVERNADOR ISRAEL PINHEIRO IPSEMG HGIP",
                "HOSPITAL LIFECENTER",
                "HOSPITAL LUXEMBURGO - INSTITUTO MÁRIO PENNA",
                "HOSPITAL MADRE TERESA",
                "HOSPITAL MARCIO CUNHA - MG - FUNDAÇÃO SÃO FRANCISCO XAVIER",
                "HOSPITAL MATER DEI SA - UNIDADE SANTO AGOSTINHO",
                "HOSPITAL MATER DEI",
                "HOSPITAL MATERNIDADE E PRONTO SOCORRO SANTA LÚCIA / HOSPITAL DO CORAÇÃO DE POÇOS DE CALDAS",
                "HOSPITAL SANTA CATARINA",
                "HOSPITAL SÃO JOÃO DE DEUS",
                "HOSPITAL SOCOR",
                "HOSPITAL UNIVERSITÁRIO DA UNIVERSIDADE FEDERAL DE JUIZ DE FORA - HU/UFJF",
                "HOSPITAL VERA CRUZ MG",
                "HOSPITAL VERA CRUZ S/A",
                "INTITUTO MATERNO INFANTIL DE MINAS GERAIS",
                "IRMANDADE DE NOSSA SENHORA DA SAUDE",
                "IRMANDADE NOSSA SENHORA DAS MERCES DE MONTES CLAROS",
                "IRMANDADE NOSSA SENHORA DAS MERCÊS E MONTES CLAROS",
                "LIFECENTER SISTEMA DE SAUDE S/A",
                "SANTA CASA DE MISERICÓRDIA DE BELO HORIZONTE",
                "SANTA CASA DE MISERICÓRDIA DE JUIZ DE FORA MG",
                "UNIVERSIDADE FEDERAL DE OURO PRETO",
                "UNIVERSIDADE FEDERAL DE UBERLÂNDIA",
                "UNIVERSIDADE FEDERAL DO TRIÂNGULO MINEIRO",
            ],
            'MS' => [
                "ASSOCIACAO BENEFICENTE SANTA CASA DE CAMPO GRANDE",
                "FUNDACAO SERVICOS DE SAUDE DE MATO GROSSO DO SUL-SAUDE-MS",
                "FUNDAÇÃO SERVIÇOS DE SAÚDE DE MATO GROSSO DO SUL-SAÚDE-MS",
                "FUNDAÇÃO UNIVERSIDADE FEDERAL DE MATO GROSSO DO SUL",
                "FUNDO MUNICIPAL DE SAUDE",
                "HOSPITAL DO CORAÇÃO DE MATO GROSSO DO SUL",
                "HOSPITAL REGIONAL DO MATO GROSSO DO SUL ROSA PEDROSSIAN",
                "HOSPITAL UNIVERSITÁRIO MARIA APARECIDA PEDROSSIAN UFMS",
            ],
            'MT' => [
                "ASSOCIACAO DE PROTECAO A MATERNIDADE E A INFANCIA DE CUIABA",
                "MUNICIPIO DE SINOP",
                "SOCIEDADE BENEFICIENTE DA SANTA CASA DE MISERICORDIA",
            ],
            'PA' => [
                "FUNDAÇÃO HOSPITAL DE CLÍNICAS GASPAR VIANNA",
                "HOSPITAL DO CORAÇÃO DO PARÁ",
                "UNIVERSIDADE DO ESTADO DO PARA UEP",
            ],
            'PE' => [
                "FACULDADE DE CIÊNCIAS MÉDICAS DE PERNAMBUCO / UPE",
                "FUNCORDIS - FUNDAÇÃO PARA O INCENTIVO AO ENSINO E PESQUISA DA CARDIOLOGIA",
                "FUNDACAO DE SAUDE AMAURY DE MEDEIROS",
                "FUNDACAO PROFESSOR MARTINIANO FERNANDES - IMIP HOSPITALAR",
                "FUNDACAO UNIVERSIDADE DE PERNAMBUCO",
                "HOSPITAL AGAMENON MAGALHÃES - PE",
                "HOSPITAL DAS CLÍNICAS DA UNIVERSIDADE FEDERAL DE PERNAMBUCO - SERVIÇO DE CARDIOLOGIA",
                "INSTITUTO DE MEDICINA INTEGRAL PROFESSOR FERNANDO FIGUEIRA - IMIP",
                "INSTITUTO DE MEDICINA INTEGRAL PROFESSOR FERNANDO FIGUEIRA IMIP",
                "PROCAPE - UNIVERSIDADE DE PERNAMBUCO",
                "REAL HOSPITAL PORTUGUES DE BENEFICENCIA EM PERNAMBUCO",
                "UNIVERSIDADE FEDERAL DO VALE DO SAO FRANCISCO",
            ],
            'PI' => [
                "EMPRESA BRASILEIRA DE SERVIÇOS HOSPITALARES - EBSERH",
                "HOSPITAL SANTA MARIA LTDA",
                "HOSPITAL UNIVERSITÁRIO DA UNIVERSIDADE FEDERAL DO PIAUÍ (HU-UFPI)",
            ],
            'PB' => [],
            'PR' => [
                "ASSOCIACAO BENEFICENTE BOM SAMARITANO",
                "ASSOCIACAO BENEFICENTE DE SAUDE DO NOROESTE DO PARANA",
                "ASSOCIAÇÃO BOM SAMARITANO HOSPITAL SANTA RITA",
                "CENTRO DE DOENÇA DO CORAÇÃO",
                "CENTRO UNIVERSITARIO DE MARINGA",
                "CRUZ VERMELHA BRASILEIRA - FILIAL DO ESTADO DO PARANA",
                "FUNDACAO DE SAUDE ITAIGUAPY",
                "FUNDO MUNICIPAL DE SAUDE DE PONTA GROSSA",
                "HOFTALON CENTRO DE ESTUDO E PESQUISA DA VISAO",
                "HONPAR _ HOSPITAL NORTE PARANAENSE",
                "HOSPITAL CARDIOLÓGICO COSTANTINI LTDA",
                "HOSPITAL CARIDADE PR IRMANDADE DA SANTA CASA DE MISERICÓRDIA",
                "HOSPITAL DA CRUZ VERMELHA BRASILEIRA FILIAL DO ESTADO DO PARANÁ",
                "HOSPITAL DAS CLÍNICAS DA UFPR",
                "HOSPITAL EVANGELICO DE CURITIBA",
                "HOSPITAL NOSSA SENHORA DAS GRAÇAS CURITIBA PR",
                "HOSPITAL NOSSA SENHORA DAS GRACAS",
                "HOSPITAL SANTA CASA DE CURITIBA",
                "HOSPITAL SAO LUCAS DE CASCAVEL LTDA",
                "HOSPITAL UNIVERSITÁRIO DO OESTE DO PARANÁ",
                "HOSPITAL UNIVERSITARIO REGIONAL DO NORTE DO PARANA/UNIVERSIDADE ESTADUAL DE LONDRINA",
                "INSTITUTO DE CARDIOLOGIA ECOVILLE LTDA",
                "INSTITUTO DE NEUROLOGIA DE CURITIBA",
                "IRMANDADE DA SANTA CASA DE LONDRINA",
                "IRMANDADE SANTA CASA DE MISERICORDIA DE MARINGA",
                "MUNICIPIO DE FRANCISCO BELTRAO",
                "REDE DE ASSISTENCIA A SAUDE METROPOLITANA",
                "SANTA CASA DE PARANAVAI",
                "SECRETARIA DE ESTADO DA SAUDE",
                "SOCIEDADE HOSPITALAR ANGELINA CARON",
                "UNIVERSIDADE ESTADUAL DO CENTRO OESTE",
                "UNIVERSIDADE FEDERAL DO PARANÁ",
            ],
            'RJ' => [
                "FUNDACAO EDUCACIONAL D ANDRE ARCOVERDE",
                "FUNDACAO EDUCACIONAL SEVERINO SOMBRA",
                "HOSPITAL AMERICAS MEDICAL CITY-HOSP.SAMARITANO",
                "HOSPITAL CENTRAL DO EXÉRCITO",
                "HOSPITAL DO GRANDE RIO (UNIGRANRIO) MARIO LIONI",
                "HOSPITAL NAVAL MARCÍLIO DIAS",
                "HOSPITAL PRÓ CARDÍACO",
                "HOSPITAL SAMARITANO",
                "HOSPITAL SÃO JOSÉ DO AVAI ITAPERUNA RJ",
                "HOSPITAL SÃO JOSÉ DO AVAÍ",
                "HOSPITAL UNIMED-RIO",
                "HOSPITAL UNIVERSITÁRIO ANTONIO PEDRO UNIVERSIDADE FEDERAL FLUMINENSE RJ",
                "HOSPITAL UNIVERSITÁRIO CLEMENTINO FRAGA FILHO UFRJ",
                "HOSPITAL UNIVERSITÁRIO PEDRO ERNESTO UERJ",
                "INCL - INSTITUTO NACIONAL DE CARDIOLOGIA DE LARANJEIRAS",
                "INSTITUTO D'OR DE PESQUISA E ENSINO",
                "INSTITUTO DE PÓS GRADUAÇÃO MÉDICA DO RIO DE JANEIRO",
                "INSTITUTO NACIONAL DE CARDIOLOGIA",
                "MUNICIPIO DE PETROPOLIS",
                "SANTA CASA DE MISERICORDIA DE BARRA MANSA",
                "SANTA CASA DE MISERICÓRDIA DO RIO DE JANEIRO",
                "SERVICO AUTONOMO HOSPITALAR",
                "UNIVERSIDADE DO GRANDE RIO_ HOSPITAL DE CLINICAS MÁRIO LIONI",
                "UNIVERSIDADE FEDERAL RURAL DO RIO DE JANEIRO",
            ],
            'RN' => [
                "HOSPITAL DO CORACAO DE NATAL",
                "HOSPITAL UNIVERSITÁRIO ONOFRE LOPES DA UFRN",
                "UNIVERSIDADE FEDERAL DO RIO GRANDE DO NORTE",
            ],
            'RO' => [
                "FUNDACAO UNIVERSIDADE FEDERAL DE RONDONIA",
                "RONDONIA SECRETARIA DE ESTADO DA SAÚDE",
            ],
            'RS' => [
                "ASSOCIACAO FRANCISCANA DE ASSISTENCIA A SAUDE",
                "ASSOCIACAO HOSPITAL DE CARIDADE IJUI",
                "ASSOCIACAO HOSPITALAR MOINHOS DE VENTO",
                "FUNDAÇÃO FACULDADE FEDERAL DE CIÊNCIAS MÉDICAS DE PORTO ALEGRE",
                "FUNDACAO UNIVERSIDADE DE PASSO FUNDO",
                "FUNDACAO VALE DO TAQUARI DE EDUCACAO E DESENVOLVIMENTO SOCIAL - FUVATES",
                "HOSPITAL DAS CLÍNICAS DE PORTO ALEGRE",
                "HOSPITAL GERAL DE CAXIAS DO SUL RS",
                "HOSPITAL MAE DE DEUS PORTO ALEGRE RS",
                "HOSPITAL MOINHOS DE VENTO - RS",
                "HOSPITAL NOSSA SENHORA DA CONCEIÇÃO RS",
                "HOSPITAL SÃO LUCAS DA PUCRS",
                "IMV- INSTITUTO DE MEDICINA VASCULAR (ASSOCIAÇÃO EDUCADORA SÃO CARLOS - HOSPITAL MÃE DE DEUS)",
                "INSTITUTO DE CARDIOLOGIA DO RIO GRANDE DO SUL FUNDAÇÃO UNIVERSITÁRIA DE CARDIOLOGIA RS",
                "IRMANDADE DA SANTA CASA DE MISERICÓRDIA PORTO ALEGRE",
                "UNIVERSIDADE FEDERAL DA FRONTEIRA SUL - UFFS",
            ],
            'RR' => [],
            'SC' => [
                "ASSOCIACAO CONGREGACAO DE SANTA CATARINA - HOSPITAL SANTA ISABEL",
                "ASSOCIACAO EDUCACIONAL E CARITATIVA",
                "CENTRO DE ESTUDOS MIGUEL SALLES CAVALCANTI",
                "FUNDACAO EDUCACIONAL DE CRICIUMA",
                "FUNDACAO HOSPITALAR DE BLUMENAU",
                "FUNDO MUNICIPAL DE SAUDE DE JARAGUA DO SUL",
                "HOSPITAL MUNICIPAL SAO JOSE",
                "HOSPITAL REGIONAL HANS DIETER SCHMIDT",
                "INSTITUTO DAS PEQUENAS MISSIONARIAS DE MARIA IMACULADA",
                "INSTITUTO DE CARDIOLOGIA DE SANTA CATARINA SES",
            ],
            'SE' => [
                "HOSPITAL DE CIRURGIA",
                "HOSPITAL SÃO LUCAS HSL",
                "UNIDADE CARDIOTORÁCICA - FUNDAÇÃO BENEFICÊNCIA H. CIRURGIA",
            ],
            'SP' => [
                "ANGIOCORPORE",
                "ASSOCIACAO BENEFICENTE HOSPITAL UNIVERSITARIO - UNIMAR",
                "ASSOCIACAO LAR SAO FRANCISCO DE ASSIS NA PROVIDENCIA DE DEUS",
                "ASSOCIAÇÃO PORTUGUESA DE BENEFICENCIA DE SÃO JOSÉ DO RIO PRETO - HOSPITAL INFANTE D. HENRIQUE",
                "CASA DE SAUDE SANTA MARCELINA",
                "FACULDADE DE CIÊNCIAS MÉDICAS DA UNICAMP",
                "FACULDADE DE MEDICINA DA USP",
                "FACULDADE DE MEDICINA DE MARILIA - FAMEMA",
                "FACULDADE DE MEDICINA DE SAO JOSE DO RIO PRETO",
                "FACULDADE DE MEDICINA DO ABC",
                "FUNDAÇÃO FACULDADE DE MEDICINA DE SÃO JOSÉ DO RIO PRETO",
                "FUNDACAO LEONOR DE BARROS CAMARGO",
                "FUNDACAO PADRE ALBINO",
                "FUNDACAO PIO XII",
                "FUNDACAO SANTA CASA DE MISERICORDIA DE FRANCA",
                "FUNDACAO SAO FRANCISCO XAVIER",
                "FUNDACAO SAO PAULO",
                "HOSPITAL ANA COSTA",
                "HOSPITAL DANTE PAZZANEZE",
                "HOSPITAL DAS CLINICAS DA FACULDADE DE MEDICINA DA U S P",
                "HOSPITAL DAS CLÍNICAS DA FACULDADE DE MEDICINA DE RIBEIRÃO PRETO USP",
                "HOSPITAL DE CARIDADE SÃO VICENTE DE PAULO",
                "HOSPITAL DO CORAÇÃO ASSOCIAÇÃO DO SANATÓRIO SÍRIO",
                "HOSPITAL DO SERVIDOR PÚBLICO ESTADUAL FRANCISCO MORATO OLIVEIRA SP",
                "HOSPITAL E MATERNIDADE DR CELSO PIERRO PUC CAMPINAS SP",
                "HOSPITAL ISRAELITA ALBERT EINSTEIN",
                "HOSPITAL REGIONAL DE PRESIDENTE PRUDENTE",
                "HOSPITAL SANTA MARCELINA",
                "HOSPITAL SÃO FRANCISCO SOCIEDADE EMPRESÁRIA LTDA",
                "HOSPITAL SÃO JOAQUIM REAL E BENEMÉRITA ASSOC PORT BENEFICÊNCIA SP",
                "HOSPITAL SIRIO LIBANES",
                "HOSPITAL TOTAL COR",
                "HOSPITAL UNIVERSITÁRIO SÃO FRANCISCO NA PROVIDÊNCIA DE DEUS",
                "IBEPEGE - INSTITUTO BRASILEIRO DE ESTUDOS E PESQUISAS DE GASTROENTEROLOGIA E OUTRAS ESPECIALIDADES",
                "ICM - INSTITUTO DO CORAÇÃO DE MARÍLIA",
                "INSTITUTO DANTE PAZZANESE DE CARDIOLOGIA SP",
                "INSTITUTO DE MOLÉSTIAS CARDIOVASCULARES - IMC - EQUIPAMENTOS CARDIOVASCULARES RIO PRETO LTDA",
                "INSTITUTO DO CORAÇÃO / FUNDAÇÃO ZERBINI/ INCOR",
                "INSTITUTO DO CORAÇÃO RIO PRETO LTDA",
                "INSTITUTO POLICLIN DE ENSINO E PESQUISA (IPEP) - HOSPITAL POLICLIN 09 DE JULHO",
                "INSTITUTO PREVENT SENIOR - IPS",
                "IRMANDADE DA SANTA CASA DE MISERICÓRDIA DE LIMEIRA SP",
                "IRMANDADE DA SANTA CASA DE MISERICÓRDIA DE MARÍLIA",
                "IRMANDADE DA SANTA CASA DE MISERICORDIA DE SAO PAULO",
                "IRMANDADE DA SANTA CASA DE MISERICÓRDIA DE SÃO PAULO",
                "MUNICIPIO DE MORRO AGUDO",
                "MUNICIPIO DE SANTOS",
                "PONTIFÍCIA UNIVERSIDADE CATÓLICA DE CAMPINAS - HOSPITAL E MATERNIDADE CELSO PIERRO",
                "REAL E BENEMÉRITA ASSOCIAÇÃO PORTUGUESA DE BENEFICÊNCIA",
                "SANTA CASA DE MISERICORDIA DE BARRETOS",
                "SANTA CASA DE MISERICÓRDIA DE RIBEIRÃO PRETO",
                "SANTA CASA DE MISERICÓRDIA DE SANTOS",
                "SANTA CASA DE MISERICÓRDIA DE SÃO JOSÉ DO RIO PRETO",
                "SOCIEDADE PORTUGUESA DE BENEFICENCIA",
                "UNIVERSIDADE DE RIBEIRÃO PRETO",
                "UNIVERSIDADE DE SAO PAULO",
                "UNIVERSIDADE DE TAUBATE",
                "UNIVERSIDADE ESTADUAL PAULISTA JÚLIO DE MESQUITA FILHO UNESP FACULDADE DE MEDICINA DE BOTUCATU SP",
                "UNIVERSIDADE FED.SÃO PAULO - ESCOLA PAULISTA DE MEDICINA",
                "UNIVERSIDADE FEDERAL DE SAO PAULO - UNIFESP",
                "UNIVERSIDADE MUNICIPAL DE SAO CAETANO DO SUL",
                "UNOESTE - HOSPITAL REGIONAL DE PRESIDENTE PRUDENTE - ANTIGO HOSPITAL UNIVERSITÁRIO",
            ],
            'TO' => [],
        ];
    }

    public static function updateStatusDocumentacao($concluir)
    {
        return $concluir ? 'pronto para envio' : 'parcialmente preenchido';
    }

    public static function validaCPF($cpf)
    {
        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

    public static function verificaPendencias($pendencias, $tipo)
    {
        $tipos = [
            'documentacao-obrigatoria' => [
                'crm',
                'socio_amb',
                'documentacao_obrigatoria'
            ],
            'especializacao-em-cardiologia' => array_keys(Campos::especializacaoEmCardiologia()),
            'documentos-facultativos' => array_merge(
                array_keys(Campos::documentosFacultativos()['atualizacao']),
                array_keys(Campos::documentosFacultativos()['experiencia'])
            ),
            'declaracoes-finais' => [
                'declaracao_pessoa_portadora_deficiencia',
                'declaracao_veracidade_informacoes'
            ]
        ];

        if (!array_key_exists($tipo, $tipos)) {
            return false;
        }

        return !!array_intersect(array_keys($pendencias), $tipos[$tipo]);
    }

    public static function inscricoesEncerradas()
    {
        $excecoes = CadastroExcecao::pluck('cadastro_id')->toArray();

        if (\Auth::guard('cadastro')->check() && in_array(\Auth::guard('cadastro')->user()->id, $excecoes)) {
            return false;
        }

        $dataEncerramento = Carbon::createFromFormat(
            'd/m/Y H:i:s',
            env('DATA_ENCERRAMENTO')
        );

        return Carbon::now()->gt($dataEncerramento);
    }

    public static function voucherDisponivel()
    {
        $dataDisponibilizacao = Carbon::createFromFormat(
            'd/m/Y H:i:s',
            env('DATA_VOUCHER')
        );

        return Carbon::now()->gt($dataDisponibilizacao);
    }

    public static function getDataNomeArquivo($arquivo)
    {
        try {
            $nomeSemExtensao = pathinfo($arquivo)['filename'];
            $dataString      = substr($nomeSemExtensao, -25, 14);

            if (! preg_match('/\d{14}/', $dataString)) {
                throw new \Exception('Data não identificada.');
            }

            return Carbon::parse($dataString)->format('d/m/Y - H:i:s');
        } catch(\Exception $e) {
            return '(erro ao identificar data)';
        }
    }

    public static function getOutra($comprovante, $campo) {
        return $comprovante->{$campo} == 'OUTRA'
            ? $comprovante->{$campo.'_outra'}
            : $comprovante->{$campo};
    }

    public static function periodoRecurso()
    {
        $dataInicio = Carbon::createFromFormat(
            'd/m/Y H:i:s',
            env('DATA_INICIO_RECURSO')
        );
        $dataFim = Carbon::createFromFormat(
            'd/m/Y H:i:s',
            env('DATA_FIM_RECURSO')
        );

        $response = new \stdClass;
        $response->inicio = Carbon::now()->gt($dataInicio);
        $response->fim    = Carbon::now()->gt($dataFim);

        return $response;
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class InformacoesMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!env('PROVA_REALIZADA')) {
            abort('404');
        }

        $user = auth('cadastro')->user();

        if ($user->status != 'valido' || ! $user->verificaPagamento()) {
            return response(view('frontend.informacoes.cadastro-invalido'));
        }

        return $next($request);
    }
}

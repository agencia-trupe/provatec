<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Tools;
use Carbon\Carbon;

class Liberacao
{
    public function handle($request, Closure $next)
    {
        $dataLiberacao = Carbon::createFromFormat(
            'd/m/Y H:i:s',
            '31/05/2019 00:01:00'
        );

        $liberado = Carbon::now()->gt($dataLiberacao);

        if (! $liberado && ! Tools::isActive('painel*') && ! Tools::isActive('admin*') && ! Tools::isActive('aguarde')) {
            return redirect()->route('aguarde');
        } elseif ($liberado && Tools::isActive('aguarde')) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}

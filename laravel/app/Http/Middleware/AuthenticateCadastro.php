<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class AuthenticateCadastro
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'cadastro')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }

        // verifica se o status é solicitado e se já passaram 10 dias da data soclicitada
        // caso tenha passado define o status da inscrição como inválida
        $cadastro = Auth::guard($guard)->user();
        if ($cadastro->status == 'solicitado') {
            $dataSolicitacao = Carbon::parse($cadastro->status_data)->startOfDay();
            $dataAtual       = Carbon::now()->startOfDay();

            if ($dataSolicitacao->diffInDays($dataAtual) > 10) {
                $cadastro->status = 'invalido';
                $cadastro->status_data = (string)Carbon::now();
                $cadastro->save();
            }
        }

        return $next($request);
    }
}

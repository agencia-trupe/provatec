<?php

namespace App\Http\Middleware;

use Closure;

class PreRealizacaoProvaMiddleware
{
    public function handle($request, Closure $next)
    {
        if (env('PROVA_REALIZADA')) {
            abort('404');
        }

        return $next($request);
    }
}

<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('aguarde', function() {
        return view('frontend.aguarde');
    })->name('aguarde');

    Route::group([
        'middleware' => ['auth.cadastro']
    ], function() {
        // dados pessoais
        Route::get('dados-pessoais', 'HomeController@dadosPessoais')->name('dadosPessoais');
        Route::post('dados-pessoais', 'HomeController@dadosPessoaisPost')->name('dadosPessoais.post');
        Route::post('dados-pessoais/upload-comprovante/{tipo}/{grupo?}', 'HomeController@uploadComprovante')->name('uploadComprovanteDadosPessoais');

        Route::group(['middleware' => ['prePublicacaoGabarito']], function() {
            // sobre a prova
            Route::get('sobre-a-prova', 'HomeController@sobreAProva')->name('sobreAProva');

            // documentacao
            Route::get('documentacao', 'DocumentacaoController@index')->name('documentacao');
            Route::get('documentacao/documentacao-obrigatoria', 'DocumentacaoController@documentacaoObrigatoria')->name('documentacao.documentacaoObrigatoria');
            Route::get('documentacao/pendencias/documentacao-obrigatoria', 'DocumentacaoController@pendenciasDocumentacaoObrigatoria')->name('documentacao.pendencias.documentacaoObrigatoria');
            Route::post('documentacao/documentacao-obrigatoria', 'DocumentacaoController@documentacaoObrigatoriaPost')->name('documentacao.documentacaoObrigatoriaPost');
            Route::post('documentacao/pendencias/documentacao-obrigatoria', 'DocumentacaoController@pendenciasDocumentacaoObrigatoriaPost')->name('documentacao.pendencias.documentacaoObrigatoriaPost');
            Route::get('documentacao/especializacao-em-cardiologia', 'DocumentacaoController@especializacaoEmCardiologia')->name('documentacao.especializacaoEmCardiologia');
            Route::get('documentacao/pendencias/especializacao-em-cardiologia', 'DocumentacaoController@pendenciasEspecializacaoEmCardiologia')->name('documentacao.pendencias.especializacaoEmCardiologia');
            Route::post('documentacao/especializacao-em-cardiologia', 'DocumentacaoController@especializacaoEmCardiologiaPost')->name('documentacao.especializacaoEmCardiologiaPost');
            Route::post('documentacao/pendencias/especializacao-em-cardiologia', 'DocumentacaoController@pendenciasEspecializacaoEmCardiologiaPost')->name('documentacao.pendencias.especializacaoEmCardiologiaPost');
            Route::get('documentacao/documentos-facultativos', 'DocumentacaoController@documentosFacultativos')->name('documentacao.documentosFacultativos');
            Route::get('documentacao/pendencias/documentos-facultativos', 'DocumentacaoController@pendenciasDocumentosFacultativos')->name('documentacao.pendencias.documentosFacultativos');
            Route::post('documentacao/documentos-facultativos', 'DocumentacaoController@documentosFacultativosPost')->name('documentacao.documentosFacultativosPost');
            Route::post('documentacao/pendencias/documentos-facultativos', 'DocumentacaoController@pendenciasDocumentosFacultativosPost')->name('documentacao.pendencias.documentosFacultativosPost');
            Route::get('documentacao/declaracoes-finais', 'DocumentacaoController@declaracoesFinais')->name('documentacao.declaracoesFinais');
            Route::get('documentacao/pendencias/declaracoes-finais', 'DocumentacaoController@pendenciasDeclaracoesFinais')->name('documentacao.pendencias.declaracoesFinais');
            Route::post('documentacao/declaracoes-finais', 'DocumentacaoController@declaracoesFinaisPost')->name('documentacao.declaracoesFinaisPost');
            Route::post('documentacao/pendencias/declaracoes-finais', 'DocumentacaoController@pendenciasDeclaracoesFinaisPost')->name('documentacao.pendencias.declaracoesFinaisPost');
            Route::post('documentacao/upload-comprovante/{input}/{grupo?}', 'DocumentacaoController@uploadComprovante')->name('documentacao.uploadComprovante');
            Route::post('documentacao/upload-comprovante-substituicao/{input}/{grupo?}', 'DocumentacaoController@uploadComprovanteSubstituicao')->name('documentacao.uploadComprovanteSubstituicao');
            Route::get('documentacao/enviar-documentacao', 'DocumentacaoController@enviarDocumentacao')->name('documentacao.enviarDocumentacao');
            Route::get('documentacao/enviar-pendencias', 'DocumentacaoController@enviarPendencias')->name('documentacao.enviarPendencias');
            Route::get('envio', 'DocumentacaoController@envio')->name('envio');

            // revisao
            Route::post('documentacao/solicitacao-de-revisao', 'DocumentacaoController@revisaoPost')->name('documentacao.revisaoPost');

            // pagamento
            Route::get('pagamento', 'PagamentoController@index')->name('pagamento');

            // voucher
            Route::get('voucher', 'HomeController@voucher')->name('voucher');

            // carta de confirmação
            Route::get('carta-de-confirmacao', 'HomeController@cartaDeConfirmacao')->name('cartaDeConfirmacao');
        });

        Route::group(['middleware' => ['informacoes']], function() {
            Route::get('informacoes', 'InformacoesController@index')->name('informacoes');
            Route::post('recursos', 'RecursosController@store')->name('recursos.store');
            Route::get('recursos/{id}', 'RecursosController@show')->name('recursos.show');
            Route::post('recursos/{id}/enviar', 'RecursosController@enviar')->name('recursos.enviar');
            Route::post('recursos/upload-comprovante/{input}', 'RecursosController@uploadComprovante')->name('recursos.uploadComprovante');
            Route::get('declaracao-de-aprovacao', 'InformacoesController@declaracaoDeAprovacao')->name('declaracaoDeAprovacao');
        });
    });

    // Cadastro
    Route::get('login', 'HomeController@login')->name('cadastro.login');
    Route::get('logout', 'CadastroAuth\AuthController@logout')->name('cadastro.logout');
    Route::get('esqueci-minha-senha', 'HomeController@esqueci')->name('cadastro.esqueci');
    Route::get('redefinicao-de-senha/{token}', 'CadastroAuth\PasswordController@showResetForm');
    Route::post('cadastro', 'CadastroAuth\AuthController@create')->name('cadastro.cadastroPost');
    Route::post('login', 'CadastroAuth\AuthController@login')->name('cadastro.loginPost');
    Route::post('redefinicao-de-senha', 'CadastroAuth\PasswordController@sendResetLinkEmail')->name('cadastro.redefinicao');
    Route::post('redefinir-senha', 'CadastroAuth\PasswordController@reset')->name('cadastro.redefinir');

    // Administração
    Route::group([
        'middleware' => ['auth.admin'],
        'prefix'     => 'admin'
    ], function() {
        Route::get('/', 'AdminController@index')->name('admin');
        Route::get('busca', 'AdminController@busca')->name('admin.busca');
        Route::get('novas-inscricoes', 'AdminController@novasInscricoes')->name('admin.novasInscricoes');
        Route::get('nao-enviadas', 'AdminController@naoEnviadas')->name('admin.naoEnviadas');
        Route::get('validacoes-em-andamento', 'AdminController@validacoesEmAndamento')->name('admin.validacoesEmAndamento');
        Route::get('substituicoes-solicitadas', 'AdminController@substituicoesSolicitadas')->name('admin.substituicoesSolicitadas');
        Route::get('substituicoes-recebidas', 'AdminController@substituicoesRecebidas')->name('admin.substituicoesRecebidas');
        Route::get('inscricoes-validadas', 'AdminController@inscricoesValidadas')->name('admin.inscricoesValidadas');
        Route::get('inscricoes-validadas/extrair', 'AdminController@inscricoesValidadasExtrair')->name('admin.inscricoesValidadas.extrair');
        Route::get('inscricoes-invalidadas', 'AdminController@inscricoesInvalidadas')->name('admin.inscricoesInvalidadas');
        Route::get('solicitacoes-de-revisao', 'AdminController@solicitacoesDeRevisao')->name('admin.solicitacoesDeRevisao');
        Route::get('inscricao/{id}/concluir-revisao', 'AdminController@concluirRevisao')->name('admin.concluirRevisao');
        Route::get('inscricao/{id}/validar', 'AdminController@inscricaoValidar')->name('admin.inscricao.validar');
        Route::get('inscricao/{id}/invalidar', 'AdminController@inscricaoInvalidar')->name('admin.inscricao.invalidar');
        Route::get('inscricao/{id}/solicitar-substituicoes', 'AdminController@inscricaoSolicitarSubstituicoes')->name('admin.inscricao.solicitarSubstituicoes');
        Route::get('inscricao/{id}/{secao?}', 'AdminController@inscricao')->name('admin.inscricao');
        Route::get('inscricao/{id}/comprovante/validar/{comprovante}/{grupo?}', 'AdminController@comprovanteValidar')->name('admin.comprovante.validar');
        Route::get('inscricao/{id}/comprovante/invalidar/{comprovante}/{grupo?}', 'AdminController@comprovanteInvalidar')->name('admin.comprovante.invalidar');
        Route::post('inscricao/{id}/comprovante/solicitar-substituicao/{comprovante}/{grupo?}', 'AdminController@comprovanteSolicitarSubstituicao')->name('admin.comprovante.solicitarSubstituicao');
        Route::get('inscricao/{id}/comprovante/{comprovante}/{grupo?}', 'AdminController@comprovante')->name('admin.comprovante');
        Route::post('inscricao/{id}/liberar', 'AdminController@liberarInscricao')->name('admin.inscricao.liberar');

        Route::get('recursos', 'AdminController@recursos')->name('admin.recursos');
        Route::get('recursos/extrair', 'AdminController@recursosExtrair')->name('admin.recursos.extrair');
        Route::get('recursos/extrair-emails', 'AdminController@recursosExtrairEmails')->name('admin.recursos.extrairEmails');
        Route::get('recursos/{id}', 'AdminController@recursosShow')->name('admin.recursos.show');
        Route::get('recursos/exibir/{boolean}', 'AdminController@recursosExibir')->name('admin.recursos.exibir');
        Route::post('recursos/upload-comprovante/{input}', 'AdminController@recursosUploadComprovante')->name('admin.recursos.uploadComprovante');
        Route::post('recursos/respostas', 'AdminController@recursosRespostas')->name('admin.recursos.respostas');
        Route::post('recursos/resposta-geral', 'AdminController@recursosRespostaGeral')->name('admin.recursos.respostaGeral');
        Route::post('recursos/{id}', 'AdminController@recursosAlterar')->name('admin.recursos.alterar');

        Route::get('excecoes', 'AdminController@excecoes')->name('admin.excecoes');
        Route::post('excecoes/adicionar', 'AdminController@excecoesAdicionar')->name('admin.excecoes.adicionar');
        Route::get('excecoes/{id}/excluir', 'AdminController@excecoesExcluir')->name('admin.excecoes.excluir');

        Route::get('liberacoes', 'AdminController@liberacoes')->name('admin.liberacoes');
    });

    Route::group([
        'prefix' => 'admin'
    ], function() {
        Route::get('login', 'AdminController@login')->name('admin.login');
        Route::get('logout', 'AdminAuth\AuthController@logout')->name('admin.logout');
        Route::post('login', 'AdminAuth\AuthController@login')->name('admin.loginPost');
    });


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('administradores', 'AdministradoresController');
        Route::resource('usuarios', 'UsuariosController');

        Route::get('atualiza-notas', 'PainelController@atualizaNotas');

        Route::get('excel-inscricoes', 'PainelController@excelInscricoes');
        Route::get('excel-substituicoes', 'PainelController@excelSubstituicoes');
        Route::get('excel-rascunho', 'PainelController@excelRascunho');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');

        Route::get('impersonate/{id}', function($id) {
            $user = \App\Models\Cadastro::findOrFail($id);
            auth('cadastro')->login($user);
            return redirect()->route('home');
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });

    // Extração inscrições validadas (pública)
    Route::get('extrair-inscricoes', function() {
        if (!\Hash::check(request('key'), '$2y$10$hfa/eIEk4F8XunlgiIYDYedxOdKFbF3ObjW.MKnv6Csioiun5PRKW')) {
            return redirect()->route('home');
        }

        $campos = [
            'updated_at'                  => 'Última atualização',
            'id'                          => 'Nº de inscrição',
            'nome'                        => 'Nome',
            'cpf'                         => 'CPF',
            'sexo'                        => 'Sexo',
            'nascimento'                  => 'Data de nascimento',
            'email'                       => 'E-mail',
            'telefone_celular'            => 'Telefone celular',
            'telefone_residencial'        => 'Telefone residencial',
            'telefone_comercial'          => 'Telefone comercial',
            'endereco'                    => 'Endereço',
            'numero'                      => 'Número',
            'complemento'                 => 'Complemento',
            'cep'                         => 'CEP',
            'bairro'                      => 'Bairro',
            'cidade'                      => 'Cidade',
            'uf'                          => 'UF',
        ];

        $cadastros = \App\Models\Cadastro::where('status', 'valido')
            ->orderBy('updated_at', 'DESC')
            ->get(array_keys($campos))
            ->map(function($c) {
                $arr = $c->toArray();
                $arr['updated_at'] = $c->updated_at->format('d/m/Y H:i');
                return $arr;
            })->prepend(array_values($campos));

        \Excel::create('Provatec_Inscricoes_'.date('d-m-Y'), function ($excel) use ($cadastros) {
            $excel->sheet('inscricoes', function ($sheet) use ($cadastros) {
                $sheet->fromArray($cadastros, null, 'A1', false, false);
                $sheet->setAllBorders('thin');
                $sheet->row(1, function($row) {
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A1:AB1')->getFont()->setBold(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'center',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    })->middleware('throttle:3,5');
});

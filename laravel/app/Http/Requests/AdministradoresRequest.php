<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Administrador;

class AdministradoresRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome'  => 'required',
            'email' => 'required|email|max:255|unique:administradores',
            'senha' => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email|max:255|unique:administradores,email,'.$this->route('administradores')->id;
            $rules['senha'] = 'confirmed|min:6';
        }

        return $rules;
    }
}

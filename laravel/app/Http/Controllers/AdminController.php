<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Cadastro;
use App\Models\Campos;

use Carbon\Carbon;
use Auth;
use App\Models\Recurso;
use App\Helpers\Tools;
use App\Http\Requests\ComprovantesRequest;
use App\Models\RespostaGeralRecursos;
use App\Models\CadastroExcecao;
use App\Models\HistoricoLiberacao;
use App\Models\RecursoResposta;

class AdminController extends Controller
{
    public function __construct()
    {
        $totalInscricoesEnviadas = Cadastro::where('status', '!=', 'novo')->count();

        view()->share('totalInscricoesEnviadas', $totalInscricoesEnviadas);
    }

    public function login()
    {
        if (\Auth::guard('admin')->check()) {
            return redirect()->route('admin');
        }

        return view('admin.login');
    }

    public function busca(Request $request)
    {
        if (!$request->get('nome') && !$request->get('inscricao')) {
            return redirect()->route('admin');
        }

        $inscricoes = Cadastro::orderBy('id', 'ASC')->where('status', '!=', 'novo');

        if ($request->get('inscricao')) {
            $inscricoes = $inscricoes->where('id', $request->get('inscricao'));
        }
        if ($request->get('nome')) {
            $inscricoes = $inscricoes->where('nome', 'LIKE', "%{$request->get('nome')}%");
        }

        $inscricoes = $inscricoes->paginate(20);

        return view('admin.busca', compact('inscricoes'));
    }

    public function index()
    {
        $inscricoes = Cadastro::all();

        $resumo = [
            'Inscrições em Rascunho' =>
                $inscricoes->where('status', 'novo')->count(),
            'Inscrições Recebidas' =>
                $inscricoes->filter(function($i) {
                    return $i->status != 'novo';
                })->count(),
            'Inscrições Validadas' =>
                $inscricoes->where('status', 'valido')->count(),
            'Inscrições Invalidadas' =>
                $inscricoes->where('status', 'invalido')->count(),
            'Inscrições para Analisar' =>
                $inscricoes->whereIn('status', [
                    'enviado', 'andamento', 'solicitado', 'substituido'
                ])->count(),
            'Inscrições Pagas' =>
                $inscricoes->filter(function($i) {
                    return $i->pago;
                })->count(),
        ];

        return view('admin.index', compact('resumo'));
    }

    public function novasInscricoes()
    {
        $inscricoes = Cadastro::whereStatus('enviado')
            ->orderBy('data_envio_documentacao', 'DESC')->paginate(20);

        return view('admin.novas-inscricoes', compact('inscricoes'));
    }

    public function naoEnviadas()
    {
        $inscricoes = Cadastro::whereStatus('novo')
            ->orderBy('id', 'DESC')->paginate(20);

        return view('admin.nao-enviadas', compact('inscricoes'));
    }

    public function validacoesEmAndamento()
    {
        $inscricoes = Cadastro::whereStatus('andamento')
            ->orderBy('status_data', 'DESC')->paginate(20);

        return view('admin.em-andamento', compact('inscricoes'));
    }

    public function substituicoesSolicitadas()
    {
        $inscricoes = Cadastro::whereStatus('solicitado')
            ->orderBy('status_data', 'DESC')->paginate(20);

        return view('admin.substituicoes-solicitadas', compact('inscricoes'));
    }

    public function substituicoesRecebidas()
    {
        $inscricoes = Cadastro::whereStatus('substituido')
            ->orderBy('status_data', 'DESC')->paginate(20);

        return view('admin.substituicoes-recebidas', compact('inscricoes'));
    }

    public function inscricoesValidadas()
    {
        $inscricoes = Cadastro::whereStatus('valido')
            ->orderBy('status_data', 'DESC')->paginate(20);

        return view('admin.inscricoes-validadas', compact('inscricoes'));
    }

    public function inscricoesValidadasExtrair()
    {
        $campos = [
            'id'                          => 'Nº de inscrição',
            'nome'                        => 'Nome',
            'nome_certificado'            => 'Nome para o certificado',
            'email'                       => 'E-mail',
            'cpf'                         => 'CPF',
            'crm'                         => 'CRM',
            'cep'                         => 'CEP',
            'endereco'                    => 'Endereço',
            'numero'                      => 'Número',
            'complemento'                 => 'Complemento',
            'bairro'                      => 'Bairro',
            'cidade'                      => 'Cidade',
            'uf'                          => 'UF',
            'telefone_residencial'        => 'Telefone residencial',
            'telefone_comercial'          => 'Telefone comercial',
            'telefone_celular'            => 'Telefone celular',
            'nascimento'                  => 'Data de nascimento',
            'sexo'                        => 'Sexo',
            'lateralidade'                => 'Lateralidade',
            'socio_amb'                   => 'Sócio AMB',
            'documentacao_obrigatoria'    => 'Graduação'
        ];

        $pontuacoes = [
            'pontuacao_especializacao'    => 'Pontuação especialização',
            'pontuacao_facultativa'       => 'Pontuação facultativa',
            'pontuacao_total'             => 'Pontuação total documentação',
            'pago'                        => 'Pago',
            'pontuacao_prova'             => 'Pontuação prova',
            'pontuacao_prova_pos_recurso' => 'Pontuação prova pós-recurso',
            'status_avaliacao'            => 'Avaliação'
        ];

        $campos = array_merge(
            $campos,
            array_map(function($c) { return $c['label']; }, Campos::especializacaoEmCardiologia()),
            ['curso_online_captec' => 'CAPTEC'],
            $pontuacoes
        );

        $cadastros = Cadastro::where('status', 'valido')
            ->get(array_keys($campos))
            ->map(function($c) {
                $arr = $c->toArray();

                $crms = [];
                foreach($c->getComprovante('crm') as $key => $value) {
                    $crms[] = $c->getComprovante('crm')[$key]['numero'];
                }
                $arr['crm'] = join(' / ', $crms);

                $arr['documentacao_obrigatoria'] = Tools::getOutra($c->getComprovante('documentacao_obrigatoria'), 'graduacao') .' | '. $c->getComprovante('documentacao_obrigatoria')->ano_graduacao .' | '. $c->getComprovante('documentacao_obrigatoria')->uf;

                $arr['socio_amb'] = $c->getComprovante('socio_amb')->select;
                $arr['pago'] = $c->verificaPagamento() ? 'Sim' : 'Não';

                $arr['pontuacao_prova'] = $c->pontuacao_prova < 0 ? 0 : $c->pontuacao_prova;

                $arr['status_avaliacao'] = $c->status_avaliacao ?: '';
                $arr['status_avaliacao'] = mb_strtoupper($c->status_avaliacao);

                $arr['pontuacao_final'] = str_replace(',', '.', $c->pontuacao_final);

                foreach (array_keys(Campos::especializacaoEmCardiologia()) as $key) {
                    if ($comp = $c->getComprovante($key)) {
                        $arr[$key] = Tools::getOutra($comp, 'instituicao').' | '.$comp->ano.' | '.$comp->uf;
                    }
                }

                $comprovanteCAPTEC = $c->getComprovante('curso_online_captec') ?: [];
                $modulosCAPTEC     = count(array_filter(
                    $comprovanteCAPTEC,
                    function($c) {
                        return $c['status'] == 'valido';
                    })
                );

                if (!$modulosCAPTEC) {
                    $arr['curso_online_captec'] = 'Não';
                } else if ($modulosCAPTEC == 1) {
                    $arr['curso_online_captec'] = '1 módulo';
                } else {
                    $arr['curso_online_captec'] = $modulosCAPTEC.' módulos';
                }

                return $arr;
            })->prepend(array_merge(array_values($campos), ['pontuacao_final' => 'Pontuação final']));

        \Excel::create('Provatec_Inscricoes_'.date('d-m-Y'), function ($excel) use ($cadastros) {
            $excel->sheet('inscricoes', function ($sheet) use ($cadastros) {
                $sheet->fromArray($cadastros, null, 'A1', false, false);
                $sheet->setAllBorders('thin');
                $sheet->row(1, function($row) {
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A1:AN1')->getFont()->setBold(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'center',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    }

    public function inscricoesInvalidadas()
    {
        $inscricoes = Cadastro::whereStatus('invalido')
            ->orderBy('status_data', 'DESC')->paginate(20);

        return view('admin.inscricoes-invalidadas', compact('inscricoes'));
    }

    public function solicitacoesDeRevisao()
    {
        $inscricoes = Cadastro::whereStatus('valido')
            ->whereNotNull('revisao_envio')
            ->orderBy('revisao_envio', 'ASC')->paginate(20);

        return view('admin.solicitacoes-de-revisao', compact('inscricoes'));
    }

    public function concluirRevisao($id)
    {
        $inscricao = Cadastro::findOrFail($id);

        if ($inscricao->revisao_conclusao) {
            abort('404');
        }

        $inscricao->update([
            'revisao_conclusao'  => (string)Carbon::now(),
        ]);

        return redirect()->route('admin.inscricao', $id);
    }

    public function inscricao($id, $secao = 'dados-pessoais')
    {
        $inscricao = Cadastro::findOrFail($id);

        if ($inscricao->status == 'novo') abort('404');

        $secoes = [
            'dados-pessoais',
            'documentacao-obrigatoria',
            'especializacao',
            'documentacao-facultativa',
            'pagamento',
            'liberacoes'
        ];

        if (!in_array($secao, $secoes)) {
            return redirect()->route('admin.inscricao', [$id, 'dados-pessoais']);
        }

        $possuiSubstituicoes = (boolean)count($inscricao->substituicoesSolicitadas());

        return view('admin.inscricao', compact('inscricao', 'secao', 'possuiSubstituicoes'));
    }

    public function comprovante($id, $comprovante, $grupo = null)
    {
        $inscricao = Cadastro::findOrFail($id);

        $campo = $inscricao->getComprovante($comprovante);

        if (!$campo) abort('404');

        if (is_array($campo)) {
            if (!$grupo || !array_key_exists($grupo, $campo)) abort('404');
            $arquivos = $campo[$grupo]['arquivos'];
        } else {
            $arquivos = $campo->arquivos;
        }

        return view('admin.comprovante', compact('inscricao', 'comprovante', 'campo', 'grupo', 'arquivos'));
    }

    protected function redirectComprovanteRoute($comprovante = null)
    {
        if (array_key_exists($comprovante, Campos::especializacaoEmCardiologia())) {
            return 'especializacao';
        } elseif (array_key_exists($comprovante, array_merge(
            Campos::documentosFacultativos()['atualizacao'],
            Campos::documentosFacultativos()['experiencia']
        ))) {
            return 'documentacao-facultativa';
        } elseif ($comprovante == 'crm' || $comprovante == 'socio_amb') {
            return 'dados-pessoais';
        } else {
            return 'documentacao-obrigatoria';
        }
    }

    protected function mudaStatusComprovante($inscricao, $comprovante, $grupo, $status, $solicitacao = null)
    {
        $campo = $inscricao->getComprovante($comprovante);
        if (!$campo) abort('404');

        if ($inscricao->status == 'enviado') {
            $inscricao->update([
                'status'          => 'andamento',
                'status_data'     => (string)Carbon::now(),
                'status_admin'    => explode(' ', Auth::guard('admin')->user()->nome)[0],
                'status_admin_id' => Auth::guard('admin')->user()->id
            ]);
        }

        if (is_array($campo)) {
            if (!$grupo || !array_key_exists($grupo, $campo)) abort('404');

            $campo[$grupo]['status'] = $status;
            $campo[$grupo]['status_data'] = (string)Carbon::now();
            $campo[$grupo]['status_admin'] = explode(' ', Auth::guard('admin')->user()->nome)[0];
            $campo[$grupo]['status_admin_id'] = Auth::guard('admin')->user()->id;
            $campo[$grupo]['msg_solicitacao'] = $solicitacao ?: '';
            if ($status == 'solicitado') {
                $campo[$grupo]['resposta'] = '';
            }

            $inscricao->update([$comprovante => json_encode($campo)]);
        } else {
            $campo->status = $status;
            $campo->status_data = (string)Carbon::now();
            $campo->status_admin = explode(' ', Auth::guard('admin')->user()->nome)[0];
            $campo->status_admin_id = Auth::guard('admin')->user()->id;
            $campo->msg_solicitacao = $solicitacao ?: '';
            if ($status == 'solicitado') {
                $campo->resposta = '';
            }

            $inscricao->update([$comprovante => json_encode($campo)]);
        }

        $inscricao->atualizaPontuacao();
    }

    public function comprovanteValidar($id, $comprovante, $grupo = null)
    {
        $inscricao = Cadastro::findOrFail($id);

        $this->mudaStatusComprovante($inscricao, $comprovante, $grupo, 'valido');

        return redirect()->route('admin.inscricao', [$id, $this->redirectComprovanteRoute($comprovante)]);
    }

    public function comprovanteInvalidar($id, $comprovante, $grupo = null)
    {
        $inscricao = Cadastro::findOrFail($id);

        $this->mudaStatusComprovante($inscricao, $comprovante, $grupo, 'invalido');

        return redirect()->route('admin.inscricao', [$id, $this->redirectComprovanteRoute($comprovante)]);
    }

    public function comprovanteSolicitarSubstituicao(Request $request, $id, $comprovante, $grupo = null)
    {
        $this->validate($request, [
            'solicitacao' => 'required'
        ]);

        $inscricao = Cadastro::findOrFail($id);

        $solicitacao = $request->get('solicitacao');

        if (! $grupo && ! count($inscricao->getComprovante($comprovante)->arquivos)) {
            return redirect()->back()->withErrors(['Não é possível solicitar substituição de um campo que não possui arquivos.']);
        }

        $this->mudaStatusComprovante($inscricao, $comprovante, $grupo, 'solicitado', $solicitacao);

        return redirect()->route('admin.inscricao', [$id, $this->redirectComprovanteRoute($comprovante)]);
    }

    public function inscricaoValidar($id)
    {
        $inscricao = Cadastro::findOrFail($id);

        // tem documentos obrigatórios que nao foram validados
        if (!$inscricao->verificaValidadeDocumentacaoObrigatoria($inscricao)) {
            session()->flash('erro', 'Essa inscrição possui documentos obrigatórios que não foram validados. Favor revisar a inscrição.');
        // tem pendências, não validar e mostrar erro
        } elseif (count($inscricao->substituicoesSolicitadas())) {
            session()->flash('erro', 'Essa inscrição possui solicitações de substituição. Clique no botão "Solicitar Substituições" ou revise a inscrição.');
        } else {
            $inscricao->update([
                'status'          => 'valido',
                'status_data'     => (string)Carbon::now(),
                'status_admin'    => explode(' ', Auth::guard('admin')->user()->nome)[0],
                'status_admin_id' => Auth::guard('admin')->user()->id
            ]);

            try {
                \Mail::send('emails.inscricao-valida', [], function ($message) use ($inscricao) {
                    $message->to($inscricao->email, $inscricao->nome)
                            ->subject('Prova do TEC 2020 – Concorrente | Aguardando pagamento')
                            ->replyTo('sbc@provatec.com.br', 'Sociedade Brasileira de Cardiologia');
                });
            } catch(\Exception $e) {}
        }

        return redirect()->route('admin.inscricao', $id);
    }

    public function inscricaoInvalidar($id)
    {
        $inscricao = Cadastro::findOrFail($id);

        // tem pendências, não validar e mostrar erro
        if (count($inscricao->substituicoesSolicitadas())) {
            session()->flash('erro', 'Essa inscrição possui solicitações de substituição. Clique no botão "Solicitar Substituições" ou revise a inscrição');
        } else {
            $inscricao->update([
                'status'          => 'invalido',
                'status_data'     => (string)Carbon::now(),
                'status_admin'    => explode(' ', Auth::guard('admin')->user()->nome)[0],
                'status_admin_id' => Auth::guard('admin')->user()->id
            ]);

            try {
                \Mail::send('emails.inscricao-invalida', [], function ($message) use ($inscricao) {
                    $message->to($inscricao->email, $inscricao->nome)
                            ->subject('Prova do TEC 2020 - Inscrição Indeferida')
                            ->replyTo('sbc@provatec.com.br', 'Sociedade Brasileira de Cardiologia');
                });
            } catch(\Exception $e) {}
        }

        return redirect()->route('admin.inscricao', $id);
    }

    public function inscricaoSolicitarSubstituicoes($id)
    {
        $inscricao = Cadastro::findOrFail($id);

        $inscricao->update([
            'status'          => 'solicitado',
            'status_data'     => (string)Carbon::now(),
            'status_admin'    => explode(' ', Auth::guard('admin')->user()->nome)[0],
            'status_admin_id' => Auth::guard('admin')->user()->id
        ]);

        try {
            \Mail::send('emails.pendencia', ['nome' => $inscricao->nome], function ($message) use ($inscricao) {
                $message->to($inscricao->email, $inscricao->nome)
                        ->subject('Prova do TEC 2020 - Pendência de documentação')
                        ->replyTo('sbc@provatec.com.br', 'Sociedade Brasileira de Cardiologia');
            });
        } catch(\Exception $e) {}

        return redirect()->route('admin.inscricao', $id);
    }

    public function recursos()
    {
        $recursos = Recurso::whereNotNull('data_envio')
            ->orderBy('data_envio', 'ASC');
        $total = $recursos->count();

        $respostaGeral = RespostaGeralRecursos::first();

        // filtra as respostas pelas questões dos recursos ENVIADOS
        $questoes  = $recursos->pluck('questao_matriz')->unique();
        $respostas = RecursoResposta::orderBy('questao_matriz', 'ASC')
        ->get()
        ->filter(function($resposta) use ($questoes) {
            return in_array(
                $resposta->questao_matriz,
                $questoes->toArray()
            );
        });

        // filtra recursos por nome
        if ($nome = request('nome_recursos')) {
            $recursos = $recursos->whereHas('cadastro', function($query) use ($nome) {
                $query->where('nome', 'LIKE', "%{$nome}%");
            });
        }

        $recursos = $recursos->get();

        return view('admin.recursos.index', compact('recursos', 'total', 'respostas', 'respostaGeral'));
    }

    public function recursosShow($id)
    {
        $recurso = Recurso::findOrFail($id);

        return view('admin.recursos.show', compact('recurso'));
    }

    public function recursosExtrair()
    {
        $campos = [
            'Nome',
            'Nº de Inscrição',
            'Protocolo',
            'Prova',
            'Questão',
            'Questão Matriz',
            'Justificativa',
            'Bibliografia'
        ];

        $recursos = Recurso::whereNotNull('data_envio')
            ->orderBy('questao_matriz', 'ASC')
            ->get()->map(function($recurso) {
                return [
                    $recurso->cadastro->nome,
                    $recurso->cadastro->id,
                    $recurso->protocolo,
                    $recurso->prova,
                    $recurso->questao,
                    $recurso->questao_bloco,
                    $recurso->justificativa,
                    $recurso->bibliografia
                ];
            })->prepend($campos);

        $file = 'Provatec-Recursos_'.date('d-m-Y_His');
        \Excel::create($file, function ($excel) use ($recursos) {
            $excel->sheet('recursos', function ($sheet) use ($recursos) {
                $sheet->fromArray($recursos, null, 'A1', false, false);
                $sheet->setAllBorders('thin');
                $sheet->getDefaultRowDimension()->setRowHeight(100);
                $sheet->getRowDimension(1)->setRowHeight(-1);
                $sheet->getDefaultStyle()->getAlignment()->setWrapText(true);
                $sheet->row(1, function($row) {
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A1:Z1')->getFont()->setBold(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'center',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    }

    public function recursosExtrairEmails()
    {
        $emails = Recurso::whereNotNull('data_envio')
            ->get()
            ->unique('cadastro_id')
            ->map(function($recurso) {
                return ['email' => $recurso->cadastro->email];
            })->prepend(['email']);

        $file = 'Provatec-Recursos-Emails_'.date('d-m-Y_His');
        \Excel::create($file, function ($excel) use ($emails) {
            $excel->sheet('emails', function ($sheet) use ($emails) {
                $sheet->fromArray($emails, null, 'A1', false, false);
            });
        })->download('xls');
    }

    public function recursosUploadComprovante(ComprovantesRequest $request, $input)
    {
        try {
            $fileName = Cadastro::uploadComprovante();

            return response()->json([
                'filename'  => $fileName,
                'inputname' => $input
            ]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar arquivo: '.$e->getMessage();
        }
    }

    public function recursosAlterar($id)
    {
        $recurso = Recurso::findOrFail($id);

        try {
            if (request('documento_do_recurso')) {
                $recurso->documento_do_recurso = request('documento_do_recurso')[0];
            }
            if (request('comprovante_de_deposito')) {
                $recurso->comprovante_de_deposito = request('comprovante_de_deposito')[0];
            }
            $recurso->save();

            return redirect()->route('admin.recursos.show', $id)->with('success', 'Recurso alterado com sucesso.');
        } catch (\Exception $e) {
            die('Erro ao alterar recurso: '.$e->getMessage());
        }
    }

    public function recursosRespostas()
    {
        foreach(request('respostas') as $id => $resposta) {
            $respostaQuestao = RecursoResposta::find($id);
            if ($respostaQuestao) {
                $respostaQuestao->update(['resposta' => $resposta]);
            }
        }

        return redirect()->route('admin.recursos')->with('success', 'Respostas alteradas com sucesso.');
    }

    public function recursosRespostaGeral()
    {
        RespostaGeralRecursos::first()->update([
            'resposta_geral' => request('resposta_geral') ?: ''
        ]);

        return redirect()->route('admin.recursos')->with('success', 'Resposta geral alterada com sucesso.');
    }

    public function recursosExibir($boolean)
    {
        if ($boolean) {
            RespostaGeralRecursos::first()->update(['ativo' => true]);
            return redirect()->route('admin.recursos')->with('success', 'Respostas exibidas com sucesso.');
        }

        RespostaGeralRecursos::first()->update(['ativo' => false]);
        return redirect()->route('admin.recursos')->with('success', 'Exibição de respostas cancelada com sucesso.');
    }

    public function excecoes()
    {
        $excecoes = CadastroExcecao::orderBy('id', 'ASC')->get();

        return view('admin.excecoes', compact('excecoes'));
    }

    public function excecoesAdicionar(Request $request)
    {
        $this->validate($request, [
            'inscricao' => 'required|integer'
        ]);

        $inscricao = Cadastro::find($request->inscricao);

        if (! $inscricao) {
            return redirect()->route('admin.excecoes')->with('erro', true);
        }

        try {
            if (! CadastroExcecao::where('cadastro_id', $inscricao->id)->count()) {
                CadastroExcecao::create(['cadastro_id' => $inscricao->id]);
            }

            return redirect()->route('admin.excecoes')->with('success', 'Exceção adicionada com sucesso.');
        } catch (\Exception $e) {
            die('Erro ao adicionar exceção: '.$e->getMessage());
        }
    }

    public function excecoesExcluir($id)
    {
        $excecao = CadastroExcecao::findOrFail($id);

        try {
            $excecao->delete();
            return redirect()->route('admin.excecoes')->with('success', 'Exceção excluída com sucesso.');
        } catch (\Exception $e) {
            die('Erro ao excluir exceção: '.$e->getMessage());
        }
    }

    public function liberarInscricao($id)
    {
        $inscricao = Cadastro::findOrFail($id);

        if ($inscricao->status == 'novo') {
            return redirect()->route('admin');
        }

        $inscricao->liberar(
            auth('admin')->user()->id, // admin id
            request('observacoes')     // observações
        );

        try {
            \Mail::send('emails.inscricao-liberada', [], function ($message) use ($inscricao) {
                $message->to($inscricao->email, $inscricao->nome)
                    ->subject('Prova do TEC 2020 - Revalidação em sua inscrição')
                    ->replyTo('sbc@provatec.com.br', 'Sociedade Brasileira de Cardiologia');
            });
        } catch(\Exception $e) {
        }

        return redirect()->route('admin')->with('success', 'Inscrição liberada com sucesso.');
    }

    public function liberacoes()
    {
        $liberacoes = HistoricoLiberacao::orderBy('cadastro_id', 'ASC')
            ->get()->groupBy('cadastro_id');

        return view('admin.liberacoes', compact('liberacoes'));
    }
}

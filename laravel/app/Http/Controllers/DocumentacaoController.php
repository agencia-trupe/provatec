<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ComprovantesRequest;

use App\Models\Cadastro;
use App\Models\Campos;
use App\Models\HistoricoLiberacao;

use App\Helpers\Tools;
use Carbon\Carbon;

class DocumentacaoController extends Controller
{
    public function index()
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            $cadastro = \Auth::guard('cadastro')->user();

            $cadastro->verificaPagamento();

            $pendencias = $cadastro->status == 'solicitado'
                ? $cadastro->substituicoesSolicitadas()
                : [];

            if (count($pendencias)) {
                return redirect()->route('documentacao.pendencias.documentacaoObrigatoria');
            } else {
                return view('frontend.documentacao.status', compact('cadastro'));
            }
        }

        return redirect()->route('documentacao.documentacaoObrigatoria');
    }

    public function documentacaoObrigatoria()
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.documentacao.documentacao-obrigatoria', compact('cadastro'));
    }

    public function pendenciasDocumentacaoObrigatoria()
    {
        $cadastro = \Auth::guard('cadastro')->user();

        $pendencias = $cadastro->status == 'solicitado'
            ? $cadastro->substituicoesSolicitadas()
            : [];

        $pendenciasValidas = count($this->pendenciasNaoPreenchidas($cadastro)) == 0;

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.documentacao.pendencias.documentacao-obrigatoria', compact('cadastro', 'pendencias', 'pendenciasValidas'));
    }

    public function documentacaoObrigatoriaPost(Request $request)
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $input['documentacao_obrigatoria_status'] = Tools::updateStatusDocumentacao($request->get('concluir'));

        $input['documentacao_obrigatoria'] = json_encode([
            'graduacao'       => $request->get('graduacao'),
            'graduacao_outra' => $request->get('graduacao') == 'OUTRA' ? $request->get('graduacao_outra') : null,
            'ano_graduacao'   => $request->get('ano_graduacao'),
            'uf'              => $request->get('uf'),
            'arquivos'        => $request->get('documentacao_obrigatoria_arquivos')
        ]);

        \Auth::guard('cadastro')->user()->update($input);

        if ($request->get('concluir')) {
            return redirect()->route('documentacao.especializacaoEmCardiologia');
        }

        return redirect()->back();
    }

    public function pendenciasDocumentacaoObrigatoriaPost(Request $request)
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $pendencias = $cadastro->substituicoesSolicitadas();
        $input = [];

        if (array_key_exists('crm', $pendencias)) {
            $crm = $cadastro->getComprovante('crm');
            if ($request->get('crm_grupo')) {
                foreach ($request->get('crm_grupo') as $grupoCrm) {
                    if (in_array($grupoCrm, $pendencias['crm'])) {
                        $crm[$grupoCrm]['substituicoes'] = $request->get('crm_substituicoes_'.$grupoCrm);
                        $crm[$grupoCrm]['resposta'] = $request->get('crm_resposta_'.$grupoCrm);
                    }
                }
            }
            $input['crm'] = json_encode($crm);
        }

        if (array_key_exists('documentacao_obrigatoria', $pendencias)) {
            $documentacaoObrigatoria = $cadastro->getComprovante('documentacao_obrigatoria');
            $documentacaoObrigatoria->substituicoes = $request->get('documentacao_obrigatoria_substituicoes');
            $documentacaoObrigatoria->resposta = $request->get('documentacao_obrigatoria_resposta');
            $input['documentacao_obrigatoria'] = json_encode($documentacaoObrigatoria);
        }

        if (array_key_exists('socio_amb', $pendencias)) {
            $documentacaoObrigatoria = $cadastro->getComprovante('socio_amb');
            $documentacaoObrigatoria->substituicoes = $request->get('socio_amb_substituicoes');
            $documentacaoObrigatoria->resposta = $request->get('socio_amb_resposta');
            $input['socio_amb'] = json_encode($documentacaoObrigatoria);
        }

        $cadastro->update($input);

        return redirect()->back();
    }

    public function especializacaoEmCardiologia()
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();
        $campos   = Campos::especializacaoEmCardiologia();

        $excedentes   = [];
        $camposUnicos = [
            'clinica-medica' => [
                'estagio_2_anos_clinica_medica',
                'estagio_2_anos_clinica_medica_nao_credenciado',
                'estagio_2_anos_clinica_medica_e_cardiologia',
                'residencia_2_anos_clinica_medica',
                'comprovante_capacitacao_clinica_medica',
            ],
            'cardiologia'    => [
                'estagio_2_anos_cardiologia',
                'estagio_2_anos_cardiologia_nao_credenciado',
                'residencia_2_anos_cardiologia',
                'residencia_2_anos_cardiologia_exterior',
                'comprovante_atuacao_cardiologia',
            ],
        ];

        foreach($camposUnicos as $key => $arr) {
            $count = 0;
            foreach($arr as $campo) {
                if ($cadastro->{$campo}) $count++;
            }
            if ($count > 1) {
                $excedentes = array_merge($excedentes, $arr);
            }
        }

        return view('frontend.documentacao.especializacao-em-cardiologia', compact('cadastro', 'campos', 'excedentes'));
    }

    public function pendenciasEspecializacaoEmCardiologia()
    {
        $cadastro = \Auth::guard('cadastro')->user();
        $campos   = Campos::especializacaoEmCardiologia();

        $pendencias = $cadastro->status == 'solicitado'
            ? $cadastro->substituicoesSolicitadas()
            : [];

        $pendenciasValidas = count($this->pendenciasNaoPreenchidas($cadastro)) == 0;

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.documentacao.pendencias.especializacao-em-cardiologia', compact('cadastro', 'pendencias', 'campos', 'pendenciasValidas'));
    }

    public function especializacaoEmCardiologiaPost(Request $request)
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $input['especializacao_cardiologia_status'] = Tools::updateStatusDocumentacao($request->get('concluir'));

        foreach (Campos::especializacaoEmCardiologia() as $field => $value) {
            if ($request->get('ano_'.$field)) {
                $input[$field] = json_encode([
                    'uf'                => $request->get('uf_'.$field),
                    'instituicao'       => $request->get('instituicao_'.$field),
                    'instituicao_outra' => $request->get('instituicao_'.$field) == 'OUTRA' ? $request->get('instituicao_'.$field.'_outra'): null,
                    'ano'               => $request->get('ano_'.$field),
                    'arquivos'          => $request->get($field.'_arquivos')
                ]);
            } else {
                $input[$field] = null;
            }
        }

        \Auth::guard('cadastro')->user()->update($input);

        if ($request->get('concluir')) {
            return redirect()->route('documentacao.documentosFacultativos');
        }

        return redirect()->back();
    }

    public function pendenciasEspecializacaoEmCardiologiaPost(Request $request)
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $pendencias = $cadastro->substituicoesSolicitadas();
        $input = [];

        foreach (Campos::especializacaoEmCardiologia() as $field => $value) {
            if (array_key_exists($field, $pendencias)) {
                $campo = $cadastro->getComprovante($field);
                $campo->substituicoes = $request->get($field.'_substituicoes');
                $campo->resposta = $request->get($field.'_resposta');

                $input[$field] = json_encode($campo);
            }
        }

        $cadastro->update($input);

        return redirect()->back();
    }

    public function documentosFacultativos()
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();
        $campos   = Campos::documentosFacultativos();

        return view('frontend.documentacao.documentos-facultativos', compact('cadastro', 'campos'));
    }

    public function pendenciasDocumentosFacultativos()
    {
        $cadastro = \Auth::guard('cadastro')->user();
        $campos   = Campos::documentosFacultativos();

        $pendencias = $cadastro->status == 'solicitado'
            ? $cadastro->substituicoesSolicitadas()
            : [];

        $pendenciasValidas = count($this->pendenciasNaoPreenchidas($cadastro)) == 0;

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.documentacao.pendencias.documentos-facultativos', compact('cadastro', 'pendencias', 'campos', 'pendenciasValidas'));
    }

    public function documentosFacultativosPost(Request $request)
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $campos = Campos::documentosFacultativos();

        $input['documentos_facultativos_status'] = Tools::updateStatusDocumentacao($request->get('concluir'));

        foreach($campos as $tipo => $inputs) {
            foreach($inputs as $field => $dados) {
                if ($dados['limite_grupo'] == 0) {
                    if ($request->get('ano_'.$field)) {
                        $input[$field] = json_encode([
                            'ano'      => $request->get('ano_'.$field),
                            'arquivos' => []
                        ]);
                    } else {
                        $input[$field] = null;
                    }
                } else if ($dados['limite_grupo'] == 1) {
                    if ($request->get('ano_'.$field)) {
                        $input[$field] = json_encode([
                            'ano'      => $request->get('ano_'.$field),
                            'arquivos' => $request->get($field.'_arquivos')
                        ]);
                    } else {
                        $input[$field] = null;
                    }
                } else {
                    $grupos = [];
                    foreach ($request->get($field.'_grupo') as $grupo) {
                        if ($request->get('ano_'.$field.'_'.$grupo)) {
                            $grupos[$grupo] = [
                                'ano'      => $request->get('ano_'.$field.'_'.$grupo),
                                'arquivos' => $request->get($field.'_arquivos_'.$grupo)
                            ];
                        }
                    }
                    $input[$field] = json_encode($grupos);
                }
            }
        }

        \Auth::guard('cadastro')->user()->update($input);

        if ($request->get('concluir')) {
            return redirect()->route('documentacao.declaracoesFinais');
        }

        return redirect()->back();
    }

    public function pendenciasDocumentosFacultativosPost(Request $request)
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $pendencias = $cadastro->substituicoesSolicitadas();
        $input = [];

        foreach(Campos::documentosFacultativos() as $tipo => $inputs) {
            foreach($inputs as $field => $dados) {
                if ($dados['limite_grupo'] == 1) {
                   if (array_key_exists($field, $pendencias)) {
                       $campo = $cadastro->getComprovante($field);
                       $campo->substituicoes = $request->get($field.'_substituicoes');
                       $campo->resposta = $request->get($field.'_resposta');

                       $input[$field] = json_encode($campo);
                   }
                } else {
                    if ($request->get($field.'_grupo')) {
                        $campo = $cadastro->getComprovante($field);

                        foreach ($request->get($field.'_grupo') as $grupo) {
                            if (in_array($grupo, $pendencias[$field])) {
                                $campo[$grupo]['substituicoes'] = $request->get($field.'_substituicoes_'.$grupo);
                                $campo[$grupo]['resposta'] = $request->get($field.'_resposta_'.$grupo);
                                $input[$field] = json_encode($campo);
                            }
                        }
                    }
                }
            }
        }

        $cadastro->update($input);

        return redirect()->back();
    }

    public function declaracoesFinais()
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.documentacao.declaracoes-finais', compact('cadastro'));
    }

    public function pendenciasDeclaracoesFinais()
    {
        $cadastro = \Auth::guard('cadastro')->user();

        $pendencias = $cadastro->status == 'solicitado'
            ? $cadastro->substituicoesSolicitadas()
            : [];

        $pendenciasValidas = count($this->pendenciasNaoPreenchidas($cadastro)) == 0;

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.documentacao.pendencias.declaracoes-finais', compact('cadastro', 'pendencias', 'pendenciasValidas'));
    }

    public function declaracoesFinaisPost(Request $request)
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        $input['declaracoes_finais_status'] = Tools::updateStatusDocumentacao($request->get('concluir'));
        $input['declaracao_pessoa_portadora_deficiencia'] = $request->get('declaracao_pessoa_portadora_deficiencia_arquivos') ? json_encode([
            'arquivos' => $request->get('declaracao_pessoa_portadora_deficiencia_arquivos')
        ]) : null;
        $input['declaracao_veracidade_informacoes'] = $request->get('declaracao_veracidade_informacoes_arquivos') ? json_encode([
            'arquivos' => $request->get('declaracao_veracidade_informacoes_arquivos')
        ]) : null;

        \Auth::guard('cadastro')->user()->update($input);

        return redirect()->back();
    }

    public function pendenciasDeclaracoesFinaisPost(Request $request)
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $pendencias = $cadastro->substituicoesSolicitadas();
        $input = [];

        if (array_key_exists('declaracao_pessoa_portadora_deficiencia', $pendencias)) {
            $documentacaoObrigatoria = $cadastro->getComprovante('declaracao_pessoa_portadora_deficiencia');
            $documentacaoObrigatoria->substituicoes = $request->get('declaracao_pessoa_portadora_deficiencia_substituicoes');
            $documentacaoObrigatoria->resposta = $request->get('declaracao_pessoa_portadora_deficiencia_resposta');
            $input['declaracao_pessoa_portadora_deficiencia'] = json_encode($documentacaoObrigatoria);
        }

        if (array_key_exists('declaracao_veracidade_informacoes', $pendencias)) {
            $documentacaoObrigatoria = $cadastro->getComprovante('declaracao_veracidade_informacoes');
            $documentacaoObrigatoria->substituicoes = $request->get('declaracao_veracidade_informacoes_substituicoes');
            $documentacaoObrigatoria->resposta = $request->get('declaracao_veracidade_informacoes_resposta');
            $input['declaracao_veracidade_informacoes'] = json_encode($documentacaoObrigatoria);
        }

        $cadastro->update($input);

        return redirect()->back();
    }

    public function uploadComprovante(ComprovantesRequest $request, $input, $grupo = null)
    {
        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            return redirect()->route('documentacao');
        }

        try {
            $fileName = Cadastro::uploadComprovante();

            return response()->json([
                'filename'  => $fileName,
                'inputname' => $grupo != null
                    ? $input.'_arquivos_'.$grupo
                    : $input.'_arquivos'
            ]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar arquivo: '.$e->getMessage();
        }
    }

    public function uploadComprovanteSubstituicao(ComprovantesRequest $request, $input, $grupo = null)
    {
        if (\Auth::guard('cadastro')->user()->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        try {
            $fileName = Cadastro::uploadComprovante();

            return response()->json([
                'filename'  => $fileName,
                'inputname' => $grupo != null
                    ? $input.'_substituicoes_'.$grupo
                    : $input.'_substituicoes'
            ]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar arquivo: '.$e->getMessage();
        }
    }

    protected function validaDocumentacao($cadastro)
    {
        $camposObrigatorios = [
            'nome'                              => 'Nome completo',
            'nome_certificado'                  => 'Nome para o Certificado',
            'cpf'                               => 'CPF',
            'crm'                               => 'CRM',
            'cep'                               => 'CEP',
            'endereco'                          => 'Endereço completo',
            'numero'                            => 'Número',
            'bairro'                            => 'Bairro',
            'cidade'                            => 'Cidade',
            'uf'                                => 'UF',
            'telefone_celular'                  => 'Telefone celular',
            'nascimento'                        => 'Data de nascimento',
            'sexo'                              => 'Sexo',
            'lateralidade'                      => 'Destro ou canhoto',
            'socio_amb'                         => 'Sócio da AMB',
            'documentacao_obrigatoria'          => 'Documentação Obrigatória',
            'declaracao_veracidade_informacoes' => 'Declaração de Veracidade das Informações',
        ];

        $erros['obrigatorio'] = [];
        foreach ($camposObrigatorios as $campo => $label) {
            if ($campo == 'documentacao_obrigatoria' && $cadastro->getComprovante('documentacao_obrigatoria')) {
                foreach([
                    'graduacao'     => 'Graduação',
                    'ano_graduacao' => 'Ano de Graduação',
                    'uf'            => 'UF'
                ] as $campoDO => $labelDO) {
                    if (!$cadastro->getComprovante('documentacao_obrigatoria')->{$campoDO}) {
                        $erros['obrigatorio'][] = $label . ' ('. $labelDO . ')';
                    }
                }

                if ($cadastro->getComprovante('documentacao_obrigatoria')->graduacao == 'OUTRA' && !$cadastro->getComprovante('documentacao_obrigatoria')->graduacao_outra) {
                    $erros['obrigatorio'][] = $label . ' (Instituição)';
                }
            } elseif ($campo == 'cpf' && $cadastro->cpf) {
                if (!Tools::validaCPF($cadastro->cpf)) {
                    $erros['obrigatorio'][] = 'CPF inválido';
                }
                $cpfExiste = Cadastro::where('cpf', $cadastro->cpf)
                    ->where('status', '!=', 'novo')
                    ->where('id', '!=', $cadastro->id)->first();
                if ($cpfExiste) {
                    $erros['obrigatorio'][] = 'CPF inserido já existe';
                }
            } else {
                if (!$cadastro->{$campo}) {
                    $erros['obrigatorio'][] = $label;
                }
            }
        }

        $erros['comprovantes'] = [];

        if ($cadastro->getComprovante('crm')) {
            foreach($cadastro->getComprovante('crm') as $key => $crm) {
                if (!count($crm['arquivos'])) {
                    $erros['comprovantes'][] = 'Insira um comprovante para o CRM '.$crm['numero'].'.';
                }
            }
        }

        if ($cadastro->getComprovante('socio_amb') && $cadastro->getComprovante('socio_amb')->select == 'Sim' && !count($cadastro->getComprovante('socio_amb')->arquivos)) {
            $erros['comprovantes'][] = 'Insira um comprovante para Sócio AMB.';
        }

        if ($cadastro->getComprovante('documentacao_obrigatoria') && !count($cadastro->getComprovante('documentacao_obrigatoria')->arquivos)) {
            $erros['comprovantes'][] = 'Insira um comprovante de Graduação em Documentação Obrigatória.';
        }

        $comprovantesClinica = 0;
        foreach([
            'estagio_2_anos_clinica_medica',
            'estagio_2_anos_clinica_medica_nao_credenciado',
            'estagio_2_anos_clinica_medica_e_cardiologia',
            'residencia_2_anos_clinica_medica',
            'comprovante_capacitacao_clinica_medica',
        ] as $campo) {
            if ($cadastro->{$campo}) $comprovantesClinica++;
        }
        $comprovantesCardiologia = 0;
        foreach([
            'estagio_2_anos_cardiologia',
            'estagio_2_anos_cardiologia_nao_credenciado',
            'residencia_2_anos_cardiologia',
            'residencia_2_anos_cardiologia_exterior',
            'comprovante_atuacao_cardiologia',
        ] as $campo) {
            if ($cadastro->{$campo}) $comprovantesCardiologia++;
        }

        $erros['minimo-clinica-medica']    = false;
        $erros['excedente-clinica-medica'] = false;
        $erros['minimo-cardiologia']       = false;
        $erros['excedente-cardiologia']    = false;

        if ($comprovantesClinica == 0) $erros['minimo-clinica-medica'] = true;
        if ($comprovantesClinica > 1) $erros['excedente-clinica-medica'] = true;
        if ($comprovantesCardiologia == 0) $erros['minimo-cardiologia'] = true;
        if ($comprovantesCardiologia > 1) $erros['excedente-cardiologia'] = true;

        // verifica se especialização contém todos campos
        foreach(array_keys(Campos::especializacaoEmCardiologia()) as $campo) {
            if (!$cadastro->getComprovante($campo)) continue;
            if (!count($cadastro->getComprovante($campo)->arquivos)) {
                $erros['comprovantes'][] = 'Insira um comprovante para '.Campos::especializacaoEmCardiologia()[$campo]['label'].'.';
            }
            if (
                !$cadastro->getComprovante($campo)->uf ||
                !$cadastro->getComprovante($campo)->instituicao ||
                ($cadastro->getComprovante($campo)->instituicao == 'OUTRA' && !$cadastro->getComprovante($campo)->instituicao_outra)
            ) {
                $erros['comprovantes'][] = 'Preencha corretamente o UF e Instituição de: '.Campos::especializacaoEmCardiologia()[$campo]['label'].'.';
            }
        }

        foreach(array_merge(
            Campos::documentosFacultativos()['atualizacao'],
            Campos::documentosFacultativos()['experiencia']
        ) as $campo => $dados) {
            if ($cadastro->getComprovante($campo)) {
                if ($dados['limite_grupo'] == 0) {
                    continue;
                } else if ($dados['limite_grupo'] == 1) {
                    if (!count($cadastro->getComprovante($campo)->arquivos)) {
                        $erros['comprovantes'][] = 'Insira um comprovante para '.$dados['label'].'.';
                    }
                } else {
                    foreach($cadastro->getComprovante($campo) as $key => $value) {
                        if (!count($value['arquivos'])) {
                            $erros['comprovantes'][] = 'Insira um comprovante para '.$dados['label'] . ' (' . $value['ano'] .').';
                        }
                    }
                }
            }
        }

        $erros['experiencia'] = false;
        $campoExperiencia15Anos            = $cadastro->getComprovante('experiencia_profissional_15_anos');
        $campoExperienciaMestradoDoutorado = $cadastro->getComprovante('experiencia_profissional_mestrado_doutorado');
        if (($campoExperiencia15Anos && !$campoExperienciaMestradoDoutorado) || ($campoExperienciaMestradoDoutorado && !$campoExperiencia15Anos)) {
            $erros['experiencia'] = true;
        }

        if (
            $erros['obrigatorio'] ||
            $erros['comprovantes'] ||
            $erros['minimo-clinica-medica'] ||
            $erros['excedente-clinica-medica'] ||
            $erros['minimo-cardiologia'] ||
            $erros['excedente-cardiologia'] ||
            $erros['experiencia']
        ) {
            session()->flash('erros-documentacao', $erros);
            return false;
        }

        if (Tools::inscricoesEncerradas()) {
            return false;
        }

        return true;
    }

    public function envio()
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'novo' && $cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        if ($cadastro->status == 'solicitado') {
            $erros = $this->pendenciasNaoPreenchidas($cadastro);
            if (count($erros)) {
                session()->flash('erros-pendencias', $erros);
            }
        } else {
            $this->validaDocumentacao($cadastro);
        }

        return view('frontend.envio', compact('cadastro'));
    }

    public function enviarDocumentacao()
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'novo') {
            return redirect()->route('documentacao');
        }

        if ($this->validaDocumentacao($cadastro)) {
            $cadastro->update([
                'data_envio_documentacao' => (string)Carbon::now(),
                'status'                  => 'enviado',
                'status_data'             => (string)Carbon::now(),
                'data_primeiro_envio'     => $cadastro->data_primeiro_envio ?: (string)Carbon::now()
            ]);

            // se o cadastro tiver sido liberado
            // adiciona log ao histórico
            if ($cadastro->liberado) {
                $cadastro->historicoLiberacoes()->create([
                    'dados' => $cadastro->toJson()
                ]);
            }

            try {
                \Mail::send('emails.confirmacao-envio', ['data' => $cadastro->data_envio_documentacao, 'inscricao' => $cadastro->id], function ($message) use ($cadastro) {
                    $message->to($cadastro->email, $cadastro->nome)
                            ->subject('Sua documentação foi enviada para a SBC – Inscrição para a Prova de Título de Especialista em Cardiologia 2020')
                            ->replyTo('sbc@provatec.com.br', 'Sociedade Brasileira de Cardiologia');
                });
            } catch(\Exception $e) {
            }

            return redirect()->route('documentacao');
        } else {
            return redirect()->route('envio');
        }

    }

    protected function pendenciasNaoPreenchidas($cadastro)
    {
        $pendencias = $cadastro->substituicoesSolicitadas();
        $naoPreenchidas = [];

        foreach($pendencias as $field => $pendencia) {
            $campo = $cadastro->getComprovante($field);
            if (!is_array($pendencia)) {
                if (!isset($campo->substituicoes) || !count($campo->substituicoes)) {
                    $naoPreenchidas[] = Campos::getTituloDescritivo($cadastro, $field);
                }
            } else {
                foreach($pendencia as $grupo) {
                    if (!array_key_exists('substituicoes', $campo[$grupo]) || !count($campo[$grupo]['substituicoes'])) {
                        $naoPreenchidas[] = Campos::getTituloDescritivo($cadastro, $field, $grupo);
                    }
                }
            }
        }

        return $naoPreenchidas;
    }

    protected function substituiComprovantesPendentes($cadastro)
    {
        $pendencias = $cadastro->substituicoesSolicitadas();

        $input = [];

        foreach($pendencias as $field => $pendencia)
        {
            $campo = $cadastro->getComprovante($field);
            if (!is_array($pendencia)) {
                $campo->status = 'substituido';
                $campo->status_data = (string)Carbon::now();
                if (isset($campo->historico)) {
                    $campo->historico->{(string)Carbon::now()} = $campo->arquivos;
                } else {
                    $campo->historico = [
                        (string)Carbon::now() => $campo->arquivos
                    ];
                }
                $campo->arquivos = $campo->substituicoes;
                $campo->substituicoes = [];

                $input[$field] = json_encode($campo);
            } else {
                foreach($pendencia as $grupo) {
                    $campo[$grupo]['status'] = 'substituido';
                    $campo[$grupo]['status_data'] = (string)Carbon::now();
                    $campo[$grupo]['historico'][(string)Carbon::now()] = $campo[$grupo]['arquivos'];
                    $campo[$grupo]['arquivos'] = $campo[$grupo]['substituicoes'];
                    $campo[$grupo]['substituicoes'] = [];

                    $input[$field] = json_encode($campo);
                }
            }
        }

        $cadastro->update($input);
    }

    public function enviarPendencias()
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->status != 'solicitado') {
            return redirect()->route('documentacao');
        }

        $errosPendencias = $this->pendenciasNaoPreenchidas($cadastro);

        if (count($errosPendencias)) {
            return redirect()->route('envio')->with([
                'erros-pendencias' => $errosPendencias
            ]);
        }

        $this->substituiComprovantesPendentes($cadastro);

        $cadastro->update([
            'data_envio_substituicao' => (string)Carbon::now(),
            'status'                  => 'substituido',
            'status_data'             => (string)Carbon::now()
        ]);

        return redirect()->route('documentacao');
    }

    public function revisaoPost(Request $request)
    {
        $this->validate($request, [
            'revisao_mensagem' => 'required'
        ]);

        if ( ! \Auth::guard('cadastro')->user()->revisao_envio) {
            \Auth::guard('cadastro')->user()->update([
                'revisao_mensagem' => $request->get('revisao_mensagem'),
                'revisao_envio'    => (string)Carbon::now()
            ]);
        }

        return redirect()->back();
    }
}

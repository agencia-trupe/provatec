<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Helpers\CropImage;
use App\Models\Cadastro;

class PainelController extends Controller
{

    public function index()
    {
        return view('painel.home');
    }

    public function order(Request $request)
    {
        if (!$request->ajax()) return false;

        $data  = $request->input('data');
        $table = $request->input('table');

        for ($i = 0; $i < count($data); $i++) {
            DB::table($table)->where('id', $data[$i])->update(array('ordem' => $i + 1));
        }

        return json_encode($data);
    }

    private $image_config = [
        'width'  => 980,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/editor/'
    ];

    public function imageUpload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'imagem' => 'image|required'
        ]);

        if ($validator->fails()) {
            $response = [
                'error'   => true,
                'message' => 'O arquivo deve ser uma imagem.'
            ];
        } else {
            $imagem   = CropImage::make('imagem', $this->image_config);
            $response = [
                'filepath' => asset($this->image_config['path'] . $imagem)
            ];
        }

        return response()->json($response);
    }

    public function atualizaNotas()
    {
        // Exceções: "em análise"
        // UPDATE `cadastros` SET pontuacao_prova_excecao = 'em análise' WHERE `cpf` IN ('055.056.899-90','107.409.277-52','395.735.078-62','224.017.028-06','020.124.925-17')
        // SELECT id, cpf, pontuacao_prova, pontuacao_prova_excecao FROM `cadastros` WHERE `cpf` IN ('055.056.899-90','107.409.277-52','395.735.078-62','224.017.028-06','020.124.925-17')

        function cpfToInt($cpf) {
            return (int) preg_replace('/[^0-9]/', '', (string) $cpf);
        }

        if (! file_exists($arquivo = storage_path('app/public/notas.csv'))) {
            throw new \Exception("Arquivo {$arquivo} não encontrado.");
        }

        $rows = \Excel::load($arquivo)->get()->map(function($row) {
            return collect([
                'cpf' => cpfToInt($row->cpf),
                'nota' => $row->nota
            ]);
        });

        $users = Cadastro::where('status', 'valido')->get();

        foreach($rows as $row) {
            $cadastro = $users->first(function($_, $value) use ($row) {
                return cpfToInt($value->cpf) === $row->get('cpf');
            });

            if (!$cadastro) {
                echo $row->get('cpf').' não encontrado<br>';
            } else {
                $pontuacao_prova  = $row->get('nota');
                $status_avaliacao = $pontuacao_prova + $cadastro->pontuacao_total >= 84
                    ? 'aprovado'
                    : 'reprovado';

                $cadastro->update([
                    'pontuacao_prova'  => $pontuacao_prova,
                    'status_avaliacao' => $status_avaliacao
                ]);
            }
        }
    }

    public function extraiemailsRecursos()
    {
        $registros = Recurso::where('comprovante_de_deposito', '!=', '')->orderBy('cadastro_id')->get(['cadastro_id'])->each(function ($model) {
            $model->nome = $model->cadastro->nome;
            $model->email = $model->cadastro->email;
        });

        $registros = $registros->unique('cadastro_id');

        $fileName = 'recursos_'.date('d-m-Y_H-i');

        Excel::create($fileName, function ($excel) use ($registros) {
            $excel->sheet('recursos', function ($sheet) use ($registros) {
                $sheet->fromModel($registros);
            });
        })->download('xls');
    }

    public function atualizaNotasPosRecurso()
    {
        ini_set('memory_limit', '-1');
        $arquivo = public_path('resultados.xls');
        $rows    = Excel::selectSheets('resultado')->load($arquivo, function() {

        })->get();

        foreach($rows as $row) {
            if ($row->totalobj) {
                $cadastro = Cadastro::find($row->numero);
                if (strtolower($cadastro->status_avaliacao) == strtolower($row->situacao)) {
                    echo $cadastro->id.' - '.$cadastro->status_avaliacao.' - '.$row->situacao.'<br>';
                }
                /* atualiza pontuacao pos recursos
                if ($cadastro->pontuacao_prova_pos_recurso == 0.0) {
                    $cadastro->pontuacao_prova_pos_recurso = number_format((float)$row->totalobj, 1, '.', '');
                    $cadastro->save();
                    echo $cadastro->id.' - '.$cadastro->pontuacao_prova.' > '.$cadastro->pontuacao_prova_pos_recurso.'<br>';
                } */
            }
        }
    }

    public function excelRascunho()
    {
        $campos = [
            'id'                          => 'Nº de inscrição',
            'nome'                        => 'Nome',
            'email'                       => 'E-mail',
        ];

        $cadastros = Cadastro::where('status', 'novo')
            ->get(array_keys($campos))
            ->prepend(array_values($campos));

        \Excel::create('Provatec_Rascunhos_'.date('d-m-Y'), function ($excel) use ($cadastros) {
            $excel->sheet('rascunhos', function ($sheet) use ($cadastros) {
                $sheet->fromArray($cadastros, null, 'A1', false, false);
                $sheet->setAllBorders('thin');
                $sheet->row(1, function($row) {
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A1:AB1')->getFont()->setBold(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'center',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    }

    public function excelSubstituicoes()
    {
        $campos = [
            'id'    => 'Nº de inscrição',
            'nome'  => 'Nome',
            'email' => 'E-mail',
        ];

        $cadastros = Cadastro::where('status', 'solicitado')
            ->get(array_keys($campos))
            ->prepend(array_values($campos));

        \Excel::create('Provatec_SubstituicoesSolicitadas_'.date('d-m-Y'), function ($excel) use ($cadastros) {
            $excel->sheet('substituicoes_solicitadas', function ($sheet) use ($cadastros) {
                $sheet->fromArray($cadastros, null, 'A1', false, false);
                $sheet->setAllBorders('thin');
                $sheet->row(1, function($row) {
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A1:AB1')->getFont()->setBold(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'center',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    }

    public function excelInscricoes()
    {
        $campos = [
            'id'     => 'Nº de inscrição',
            'nome'   => 'Nome',
            'email'  => 'E-mail',
            'status' => 'Status',
        ];

        $cadastros = Cadastro::whereIn('status', [
                'enviado',
                'andamento',
                'valido',
                'invalido',
                'solicitado',
                'substituido',
            ])
            ->get(array_keys($campos))
            ->prepend(array_values($campos));

        \Excel::create('Provatec_Inscricoes_'.date('d-m-Y'), function ($excel) use ($cadastros) {
            $excel->sheet('inscricoes', function ($sheet) use ($cadastros) {
                $sheet->fromArray($cadastros, null, 'A1', false, false);
                $sheet->setAllBorders('thin');
                $sheet->row(1, function($row) {
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A1:AB1')->getFont()->setBold(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'center',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    }
}

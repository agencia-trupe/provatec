<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\AdministradoresRequest;

use App\Http\Controllers\Controller;
use App\Models\Administrador;

class AdministradoresController extends Controller
{
    public function index()
    {
        $usuarios = Administrador::all();

        return view('painel.administradores.index', compact('usuarios'));
    }

    public function create()
    {
        return view('painel.administradores.create');
    }

    public function store(AdministradoresRequest $request)
    {
        try {

            $input = $request->all();
            $input['senha'] = bcrypt($input['senha']);

            Administrador::create($input);
            return redirect()->route('painel.administradores.index')->with('success', 'Usuário adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar usuário: '.$e->getMessage()]);

        }
    }

    public function edit(Administrador $usuario)
    {
        return view('painel.administradores.edit', compact('usuario'));
    }

    public function update(AdministradoresRequest $request, Administrador $usuario)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['senha'])) $input['senha'] = bcrypt($input['senha']);

            $usuario->update($input);
            return redirect()->route('painel.administradores.index')->with('success', 'Usuário alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar usuário: '.$e->getMessage()]);

        }
    }

    public function destroy(Administrador $usuario)
    {
        try {

            $usuario->delete();
            return redirect()->route('painel.administradores.index')->with('success', 'Usuário excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir usuário: '.$e->getMessage()]);

        }
    }
}

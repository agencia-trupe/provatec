<?php

namespace App\Http\Controllers\AdminAuth;

use App\Models\Administrador;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\CadastrosRequest;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo          = '/admin/login';
    protected $guard               = 'admin';
    protected $redirectAfterLogout = '/admin/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'logout']);
    }

    protected function login(Request $request) {
        if (Auth::guard('admin')->attempt([
            'email'    => $request->get('email'),
            'password' => $request->get('senha')
        ])) {
            return redirect()->route('admin');
        } else {
            return redirect()->route('admin.login')->withInput()->with('erro-login', 'e-mail ou senha inválidos');
        }
    }
}

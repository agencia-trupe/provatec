<?php

namespace App\Http\Controllers\CadastroAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

use Auth;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $subject = 'Prova do TEC 2020 – Recuperação de senha';

    protected $guard  = 'cadastro';
    protected $broker = 'cadastros';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.cadastro');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ], [
            'email.required' => 'insira um endereço de e-mail válido',
            'email.email'    => 'insira um endereço de e-mail válido',
        ]);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
            $message->subject($this->getEmailSubject())
                    ->replyTo('sbc@provatec.com.br', 'Sociedade Brasileira de Cardiologia');
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with(
                    'enviado',
                    'um e-mail foi enviado com instruções para a redefinição de senha'
                );

            case Password::INVALID_USER:
            default:
                return redirect()->back()->withErrors(['e-mail não encontrado']);
        }
    }

    protected function getSendResetLinkEmailSuccessResponse()
    {
        return redirect()->route('profissionais.login')->with([
            'senhaRedefinida' => request('email')
        ]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->input('email');

        return view('frontend.redefinicao-de-senha', compact('email', 'token'));
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules(), [
            'password.required'  => 'insira uma senha',
            'password.min'       => 'sua senha deve ter no mínimo 6 caracteres',
            'password.confirmed' => 'a confirmação de senha não confere',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);

            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->senha = bcrypt($password);

        $user->save();

        Auth::guard($this->getGuard())->login($user);
    }

    protected function getResetSuccessResponse($response)
    {
        return redirect()->route('dadosPessoais');
    }
}

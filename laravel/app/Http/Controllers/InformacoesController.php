<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\Tools;

use App\Models\RespostaGeralRecursos;
use App\Models\RecursoResposta;

class InformacoesController extends Controller
{
    public function index()
    {
        $cadastro      = auth('cadastro')->user();
        $recursos      = $cadastro->recursos;
        $respostas     = RecursoResposta::lists('resposta', 'questao_matriz');
        $respostaGeral = RespostaGeralRecursos::first();

        $periodoRecurso = Tools::periodoRecurso();

        return view('frontend.informacoes.index', compact('cadastro', 'recursos', 'respostas', 'respostaGeral', 'periodoRecurso'));
    }

    public function declaracaoDeAprovacao()
    {
        $cadastro = auth('cadastro')->user();

        if ($cadastro->status_avaliacao != 'aprovado') {
            abort('404');
        }

        return view('frontend.informacoes.declaracao-de-aprovacao', compact('cadastro'));
    }
}

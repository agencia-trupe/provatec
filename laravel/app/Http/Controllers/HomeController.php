<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ComprovantesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cadastro;
use App\Helpers\Tools;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

    public function login()
    {
        if (\Auth::guard('cadastro')->check()) {
            return redirect()->route('dadosPessoais');
        }

        return view('frontend.login');
    }

    public function esqueci()
    {
        return view('frontend.esqueci');
    }

    public function dadosPessoais()
    {
        $cadastro = \Auth::guard('cadastro')->user();
        $enviado  = !!$cadastro->dados_pessoais_enviados;

        return view('frontend.dados-pessoais', compact('cadastro', 'enviado'));
    }

    public function dadosPessoaisPost(Request $request)
    {
        $input = [
            'nome'                 => $request->get('nome'),
            'nome_certificado'     => $request->get('nome_certificado'),
            'cpf'                  => $request->get('CPF'),
            'cep'                  => $request->get('CEP'),
            'endereco'             => $request->get('endereco'),
            'numero'               => $request->get('numero'),
            'complemento'          => $request->get('complemento'),
            'bairro'               => $request->get('bairro'),
            'cidade'               => $request->get('cidade'),
            'uf'                   => $request->get('uf'),
            'telefone_residencial' => $request->get('telefone_residencial'),
            'telefone_comercial'   => $request->get('telefone_comercial'),
            'telefone_celular'     => $request->get('telefone_celular'),
            'nascimento'           => $request->get('nascimento'),
            'sexo'                 => $request->get('sexo'),
            'lateralidade'         => $request->get('lateralidade'),
        ];

        $input['dados_pessoais_enviados'] = 1;

        $input['socio_amb'] = $request->get('socio_amb_select') ? json_encode([
            'select'   => $request->get('socio_amb_select'),
            'arquivos' => $request->get('socio_amb_select') == 'Sim'
                ? $request->get('socio_amb_arquivos')
                : []
        ]) : null;

        $crm = [];
        if ($request->get('crm_grupo')) {
            foreach ($request->get('crm_grupo') as $grupoCrm) {
                if ($request->get('crm_numero_'.$grupoCrm)) {
                    $crm[$grupoCrm] = [
                        'numero'   => $request->get('crm_numero_'.$grupoCrm),
                        'arquivos' => $request->get('crm_arquivos_'.$grupoCrm)
                    ];
                }
            }
        }
        $input['crm'] = count($crm) ? json_encode($crm) : null;

        if (\Auth::guard('cadastro')->user()->status != 'novo') {
            foreach([
                'nome',
                'nome_certificado',
                'cpf',
                'crm',
                'sexo',
                'lateralidade',
                'socio_amb'
            ] as $unsetKey) {
                unset($input[$unsetKey]);
            }
        }

        \Auth::guard('cadastro')->user()->update($input);

        return redirect()->route('dadosPessoais');
    }

    public function uploadComprovante(ComprovantesRequest $request, $tipo, $grupo = null)
    {
        try {
            $fileName = Cadastro::uploadComprovante();

            return response()->json([
                'filename'  => $fileName,
                'inputname' => $grupo != null
                    ? $tipo.'_arquivos_'.$grupo
                    : $tipo.'_arquivos'
            ]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar arquivo: '.$e->getMessage();
        }
    }

    public function sobreAProva()
    {
        return view('frontend.sobre-a-prova');
    }

    public function voucher()
    {
        $cadastro = \Auth::guard('cadastro')->user();

        if ($cadastro->cpf && $cadastro->status == 'valido' && Tools::voucherDisponivel()) {
            return view('frontend.voucher', compact('cadastro'));
        }

        return redirect()->route('dadosPessoais');
    }

    public function cartaDeConfirmacao()
    {
        $user = auth('cadastro')->user();

        if ($user->status != 'valido' || !$user->verificaPagamento() || env('PROVA_REALIZADA') || !env('CARTA_DE_CONFIRMACAO')) {
            return redirect()->route('dadosPessoais');
        }

        return view('frontend.carta-de-confirmacao', [
            'inscricao' => $user->id,
            'nome'      => $user->nome
        ]);
    }
}

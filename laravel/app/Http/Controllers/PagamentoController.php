<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagamentoController extends Controller
{
    public function index()
    {
        if (\Auth::guard('cadastro')->user()->status != 'valido') {
            return redirect()->route('dadosPessoais');
        }

        $cadastro = \Auth::guard('cadastro')->user();

        return view('frontend.pagamento', compact('cadastro'));
    }
}

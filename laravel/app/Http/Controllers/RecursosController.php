<?php

namespace App\Http\Controllers;

use App\Helpers\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests\ComprovantesRequest;
use App\Models\Recurso;
use App\Models\Cadastro;
use Carbon\Carbon;

class RecursosController extends Controller
{
    private $periodoRecurso;

    public function __construct()
    {
        $this->periodoRecurso = Tools::periodoRecurso();

        if (! $this->periodoRecurso->inicio) abort('404');
    }

    public function store()
    {
        if ($this->periodoRecurso->fim) abort('500');

        $validator = Validator::make(request()->all(), [
            // 'prova'         => 'required|in:A,B,C,D',
            'questao'       => 'required|integer|between:1,100',
            'justificativa' => 'required|max:1500',
            'bibliografia'  => 'array'
        ], [
            'prova.required' => 'Preencha o tipo de prova',
            'prova.in'       => 'Tipo de prova inválido',
            'prova.between'  => 'Tipo de prova deve ter um valor entre :min - :max',
            'questao.required' => 'Preencha o número da questão',
            'questao.integer'  => 'Número da questão inválido',
            'questao.between'  => 'Número da questão deve ter um valor entre :min - :max',
            'justificativa.required' => 'Insira sua justificativa'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator, 'protocolo')->withInput();
        }

        $bibliografia = '';

        if (request('bibliografia')) {
            $bibliografia = join("\n", request('bibliografia'));

            if (request('bibliografia_complemento')) {
                $bibliografia .= "\n".request('bibliografia_complemento');
            }
        }

        $recurso = auth('cadastro')->user()->recursos()->create([
            'prova'         => 'matriz',
            'questao'       => request('questao'),
            'justificativa' => request('justificativa'),
            'bibliografia'  => $bibliografia
        ]);

        return redirect()->to(
            route('informacoes', ['adicionado' => $recurso->id])
            .'#recurso-'.$recurso->id
        );
    }

    public function show($id)
    {
        $recurso = Recurso::findOrFail($id);

        if ($recurso->cadastro_id != auth('cadastro')->user()->id) {
            abort('404');
        }

        return view('frontend.informacoes.solicitacao-de-recurso', compact('recurso'));
    }

    public function enviar($id)
    {
        if ($this->periodoRecurso->fim) abort('500');

        $validator = Validator::make(request()->all(), [
            'documento_do_recurso'    => 'required|array',
            'comprovante_de_deposito' => 'required|array'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator, 'envio');
        }

        $recurso = Recurso::findOrFail($id);

        if ($recurso->cadastro_id != auth('cadastro')->user()->id) {
            abort('422');
        }

        try {
            $recurso->update([
                'documento_do_recurso'    => request('documento_do_recurso')[0],
                'comprovante_de_deposito' => request('comprovante_de_deposito')[0],
                'data_envio'              => (string)Carbon::now()
            ]);

            return redirect()->to(
                route('informacoes').'#recurso-'.$recurso->id
            );
        } catch (\Exception $e) {
            die('Erro ao enviar recurso: '.$e->getMessage());
        }
    }

    public function uploadComprovante(ComprovantesRequest $request, $input)
    {
        if ($this->periodoRecurso->fim) abort('500');

        try {
            $fileName = Cadastro::uploadComprovante();

            return response()->json([
                'filename'  => $fileName,
                'inputname' => $input
            ]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar arquivo: '.$e->getMessage();
        }
    }
}

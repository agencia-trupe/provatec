<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CadastroExcecao extends Model
{
    protected $table = 'cadastro_excecoes';

    protected $guarded = ['id'];

    public function cadastro()
    {
        return $this->belongsTo(Cadastro::class, 'cadastro_id');
    }
}

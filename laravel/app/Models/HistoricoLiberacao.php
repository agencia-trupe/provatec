<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricoLiberacao extends Model
{
    protected $table = 'historico_liberacoes';

    protected $guarded = ['id'];

    public function cadastro()
    {
        return $this->belongsTo(Cadastro::class, 'cadastro_id');
    }

    public function admin()
    {
        return $this->belongsTo(Administrador::class, 'admin_id');
    }
}

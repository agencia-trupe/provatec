<?php

namespace App\Models;

use App\Helpers\Tools;

class Campos
{
    public static function especializacaoEmCardiologia()
    {
        return [
            // clínica médica
            'estagio_2_anos_clinica_medica' => [
                'label' => 'Estágio de 2 anos em Clínica Médica credenciado pela SBCM',
                'valor' => 3,
            ],
            'estagio_2_anos_clinica_medica_nao_credenciado' => [
                'label' => 'Estágio de 2 anos em Clínica Médica não credenciado pela SBCM',
                'valor' => 0,
            ],
            'estagio_2_anos_clinica_medica_e_cardiologia' => [
                'label' => 'Estágio de 2 anos (Clínica Médica + Cardiologia) credenciado pela SBC',
                'valor' => 0,
            ],
            'residencia_2_anos_clinica_medica' => [
                'label' => 'Residência de 2 anos em Clínica Médica credenciada pela CNRM',
                'valor' => 5,
            ],
            'titulo_especialista_clinica_medica' => [
                'label' => 'Título de especialista em Clínica Médica',
                'valor' => 5,
            ],
            'comprovante_capacitacao_clinica_medica' => [
                'label' => 'Comprovante de capacitação por atuação prático-profissional em período equivalente ao dobro do tempo de formação do respectivo Programa de Residência Médica (ou seja, 4 anos) em Clínica Médica. [Anexo I-B / Edital]',
                'valor' => 0,
            ],

            // cardiologia
            'estagio_2_anos_cardiologia' => [
                'label' => 'Estágio de 2 anos em Cardiologia credenciada pela SBC',
                'valor' => 5,
            ],
            'estagio_2_anos_cardiologia_nao_credenciado' => [
                'label' => 'Estágio de 2 anos em Cardiologia em instituição não credenciada pela SBC',
                'valor' => 0,
            ],
            'residencia_2_anos_cardiologia' => [
                'label' => 'Residência de 2 anos em Cardiologia credenciada pela CNRM',
                'valor' => 5,
            ],
            'residencia_2_anos_cardiologia_exterior' => [
                'label' => 'Residência de 2 anos em Cardiologia realizada no exterior e revalidada pelo MEC/CNRM',
                'valor' => 5,
            ],
            'comprovante_atuacao_cardiologia' => [
                'label' => 'Comprovante de atuação prático-profissional na área de especialidade (Cardiologia), em período mínimo equivalente ao dobro do tempo de formação do respectivo Programa de Residência Médica (ou seja, 4 anos). [Anexo I-A / Edital]',
                'valor' => 0,
            ],
        ];
    }

    public static function documentosFacultativos()
    {
        return [
            'atualizacao' => [
                'participacao_congressos_cardiologia'  => [
                    'label'           => 'Participação em congressos de cardiologia (presenciais ou virtuais) sejam eles das sociedades nacionais, regionais, estaduais e departamentos da SBC; congressos internacionais de cardiologia nos últimos 2 anos*',
                    'ano_placeholder' => 'Ano de participação',
                    'limite_grupo'    => 4,
                    'btn_grupo'       => 'Incluir outro congresso',
                    'limite_anos'     => 2,
                    'valor'           => 1
                ],
                'congresso_brasileiro_cargiologia'     => [
                    'label'           => 'Congresso Brasileiro de Cardiologia (da SBC) do ano vigente<br><strong>Importante: Este evento será pontuado automaticamente após confirmação da inscrição e presença.</strong>',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 0,
                    'btn_grupo'       => '',
                    'limite_anos'     => 0,
                    'valor'           => 1.5
                ],
                'mestrado_cardiologia'                 => [
                    'label'           => 'Mestrado em Cardiologia',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 1,
                    'btn_grupo'       => '',
                    'valor'           => 3
                ],
                'doutorado_livre_docencia_cardiologia' => [
                    'label'           => 'Doutorado e Livre-Docência em Cardiologia',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 1,
                    'btn_grupo'       => '',
                    'valor'           => 4
                ],
                'cursos_presenciais_reciclagem'        => [
                    'label'           => 'Cursos presenciais de reciclagem nos moldes estabelecidos pela SBC, realizados pelas estaduais/regionais nos últimos 2 anos*',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 4,
                    'btn_grupo'       => 'Incluir outro curso',
                    'limite_anos'     => 2,
                    'valor'           => 1
                ],
                'curso_presencial_sbc'        => [
                    'label'           => 'Curso de atualização em Cardiologia da SBC (CAC) realizado nos últimos 2 anos*<br><strong>Importante: Para o curso de 2019 o certificado deve ser anexado durante a inscrição.</strong>',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 1,
                    'btn_grupo'       => '',
                    'limite_anos'     => 2,
                    'valor'           => 1.5
                ],
                'participacao_jornadas_simposios'      => [
                    'label'           => 'Participação em jornadas, simpósios e outros cursos presenciais da SBC e suas regionais, estaduais e departamentos (não concomitantes entre si ou inseridos em congressos) nos últimos 2 anos*',
                    'ano_placeholder' => 'Ano de participação',
                    'limite_grupo'    => 8,
                    'btn_grupo'       => 'Incluir outra participação',
                    'limite_anos'     => 2,
                    'valor'           => 0.5
                ],
                'conclusao_savic_acls_teca_a'          => [
                    'label'           => 'Conclusão nos cursos SAVIC, ACLS, TECA A nos últimos 2 anos*<br><strong>Observação: os cursos SAVIC consultório e SAVICO não pontuam.</strong>',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 8,
                    'btn_grupo'       => 'Incluir outro curso',
                    'limite_anos'     => 2,
                    'valor'           => 0.5
                ],
                'conclusao_cursos_distancia'           => [
                    'label'           => 'Conclusão em cursos a distância ofertados pela Universidade Corporativa nos últimos 2 anos (Não serão aceitos certificados de aulas grátis). Cursos ofertados pelas estaduais/regionais e departamentos com duração mínima de 15 horas descritas em certificado.',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 8,
                    'btn_grupo'       => 'Incluir outro curso',
                    'limite_anos'     => 2,
                    'valor'           => 0.5
                ],
                'curso_online_captec'                  => [
                    'label'           => 'Curso on-line CAPTEC oficial da SBC. Conclusão dos cinco módulos totalizando 3 pontos; em módulos individuais, serão pontuados por módulo finalizado: 0,5 cada. Será aceita documentação referente à apenas 1 curso nos últimos 2 anos*.',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 5,
                    'btn_grupo'       => 'Incluir outro módulo',
                    'limite_anos'     => 2,
                    'valor'           => 0.5
                ],
                'publicacao_artigo_original'           => [
                    'label'           => 'Publicação como autor ou coautor de artigo original ou de revisão em revista indexada (PubMed, SCIelo ou LILACS) nos últimos 3 anos**',
                    'ano_placeholder' => 'Ano de publicação',
                    'limite_grupo'    => 2,
                    'btn_grupo'       => 'Incluir outra publicação',
                    'limite_anos'     => 3,
                    'valor'           => 1
                ],
                'capitulo_livro_cardiologia'           => [
                    'label'           => 'Capítulo de livro na área de cardiologia.',
                    'ano_placeholder' => 'Ano de participação',
                    'limite_grupo'    => 2,
                    'btn_grupo'       => 'Incluir outra autoria',
                    'valor'           => 1
                ],
                'oral_ou_poster_congresso'             => [
                    'label'           => 'Autoria ou coautoria de tema livre oral ou pôster em Congresso de Cardiologia da SBC (nacional, das regionais, estaduais ou departamentos) nos últimos 2 anos*',
                    'ano_placeholder' => 'Ano de participação',
                    'limite_grupo'    => 4,
                    'btn_grupo'       => 'Incluir outra autoria',
                    'limite_anos'     => 2,
                    'valor'           => 0.5
                ],
                'participacao_registros_brasileiros'   => [
                    'label'           => 'Participação nos Registros Brasileiros Cardiovasculares da SBC (ACCEPT, BPC, BREATHE RBH, REACT e RECALL).',
                    'ano_placeholder' => 'Ano de participação',
                    'limite_grupo'    => 4,
                    'btn_grupo'       => 'Incluir outra participação',
                    'valor'           => 0.5
                ],
            ],
            'experiencia' => [
                'experiencia_profissional_15_anos' => [
                    'label'           => 'Experiência Profissional: 15 anos de formado com atuação em Cardiologia e doutorado stricto sensu ou livre docência (Anexo I-C + comprovante de mestrado ou doutorado/livre-docência) [30 pontos]',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 1,
                    'btn_grupo'       => '',
                    'valor'           => 30
                ],
                'experiencia_profissional_mestrado_doutorado' => [
                    'label'           => 'Comprovante de mestrado ou doutorado stricto sensu ou livre docência.',
                    'ano_placeholder' => 'Ano de conclusão',
                    'limite_grupo'    => 1,
                    'btn_grupo'       => '',
                    'valor'           => 0
                ],
            ]
        ];
    }

    public static function getTituloDescritivo($inscricao, $campo, $grupo = null)
    {
        $camposFacultativos = array_merge(
            Campos::documentosFacultativos()['atualizacao'],
            Campos::documentosFacultativos()['experiencia']
        );

        switch ($campo) {
            case 'crm':
                return 'CRM ' . $inscricao->getComprovante($campo)[$grupo]['numero'];
                break;
            case 'socio_amb':
                return 'Sócio AMB';
                break;
            case 'documentacao_obrigatoria':
                return
                    Tools::getOutra($inscricao->getComprovante('documentacao_obrigatoria'), 'graduacao').' | '.
                    $inscricao->getComprovante('documentacao_obrigatoria')->ano_graduacao.' | '.
                    $inscricao->getComprovante('documentacao_obrigatoria')->uf;
                break;
            case 'declaracao_veracidade_informacoes':
                return 'Declaração de Veracidade das Informações';
                break;
            case 'declaracao_pessoa_portadora_deficiencia':
                return 'Declaração de Pessoa Portadora de Deficiência';
                break;
            case array_key_exists($campo, Campos::especializacaoEmCardiologia()):
                return
                    Campos::especializacaoEmCardiologia()[$campo]['label'].' | '.
                    $inscricao->getComprovante($campo)->uf.' | '.
                    $inscricao->getComprovante($campo)->ano.'<br>'.
                    Tools::getOutra($inscricao->getComprovante($campo), 'instituicao');
                break;
            case array_key_exists($campo, $camposFacultativos):
                $dados = $camposFacultativos[$campo];
                return
                    $dados['label'].' | '.
                    ($dados['limite_grupo'] == 1 || $dados['limite_grupo'] == 0
                        ? $inscricao->getComprovante($campo)->ano
                        : $inscricao->getComprovante($campo)[$grupo]['ano']);
                break;
        }
    }
}

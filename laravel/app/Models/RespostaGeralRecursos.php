<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RespostaGeralRecursos extends Model
{
    protected $table = 'recursos_resposta_geral';

    protected $guarded = ['id'];
}

<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Helpers\Tools;
use App\Models\CadastroExcecao;
use App\Models\HistoricoLiberacao;

class Cadastro extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cadastros';

    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['senha', 'remember_token'];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function getComprovante($comprovante)
    {
        $documentosFacultativos = array_merge(
            Campos::documentosFacultativos()['atualizacao'],
            Campos::documentosFacultativos()['experiencia']
        );

        $documentosFacultativosCompostos = array_filter($documentosFacultativos,
            function($documento) {
                return $documento['limite_grupo'] > 1;
            }
        );

        $camposCompostos = array_merge(
            ['crm'],
            array_keys($documentosFacultativosCompostos)
        );

        // se o campo for composto retorna json_decode como array (segundo atributo true)
        return json_decode($this->{$comprovante}, in_array($comprovante, $camposCompostos));
    }

    public function getComprovanteStatus($comprovante, $grupo = null)
    {
        if ($grupo && is_array($this->getComprovante($comprovante))) {
            return array_key_exists('status', $this->getComprovante($comprovante)[$grupo])
                ? $this->getComprovante($comprovante)[$grupo]['status']
                : null;
        } else {
            return isset($this->getComprovante($comprovante)->status)
                ? $this->getComprovante($comprovante)->status
                : null;
        }
    }

    public function getComprovanteStatusLabel($comprovante, $grupo = null)
    {
        $campo = $this->getComprovante($comprovante);

        if (!$status = $this->getComprovanteStatus($comprovante, $grupo)) {
            return null;
        }

        if (is_array($campo)) {
            $data  = $campo[$grupo]['status_data'];
            $admin = $campo[$grupo]['status_admin'];
        } else {
            $data  = $campo->status_data;
            $admin = $campo->status_admin;
        }

        switch ($status) {
            case 'valido':
                $label = 'VÁLIDO EM '.Tools::formataData($data).' POR '.strtoupper($admin);
                break;
            case 'invalido':
                $label = 'INVÁLIDO EM '.Tools::formataData($data).' POR '.strtoupper($admin);
                break;
            case 'solicitado':
                $label = 'SUBSTITUIÇÃO SOLICITADA EM '.Tools::formataData($data).' POR '.strtoupper($admin);
                break;
            case 'substituido':
                $label = 'SUBSTITUÍDO EM '.Tools::formataData($data);
                break;
            default:
                $label = '';
                break;
        }

        return $label;
    }

    public function pontuacaoDocumentoFacultativo($campo)
    {
        $pontuacao = 0;

        $camposAtualizacao = Campos::documentosFacultativos()['atualizacao'];
        if ($this->getComprovante($campo)) {
            if (!is_array($this->getComprovante($campo))) {
                if ($this->getComprovanteStatus($campo) == 'valido') {
                    $pontuacao += $camposAtualizacao[$campo]['valor'];
                }
            } else {
                foreach($this->getComprovante($campo) as $key => $dados) {
                    if ($this->getComprovanteStatus($campo, $key) == 'valido') {
                        $pontuacao += $camposAtualizacao[$campo]['valor'];
                    }
                }
            }
        }

        if ($campo == 'curso_online_captec') {
            if (count(array_filter($this->getComprovante('curso_online_captec'), function($c) {
                return (array_key_exists('status', $c) && $c['status'] == 'valido');
            })) == 5) {
                $pontuacao += 0.5;
            }
        }

        return $pontuacao . ($pontuacao <= 1 && $pontuacao != 0 ? ' ponto' : ' pontos');
    }

    public function pontuacaoSecoesDocumentosFacultativos()
    {
        // calcula comprovantes de documentos facultativos
        // dividido em 3 seçoes
        $secao1 = 0;
        $secao2 = 0;
        $secao3 = 0;

        $camposAtualizacao = Campos::documentosFacultativos()['atualizacao'];
        foreach([
            'secao1' => [
                'participacao_congressos_cardiologia',
                'congresso_brasileiro_cargiologia',
                'mestrado_cardiologia',
                'doutorado_livre_docencia_cardiologia'
            ],
            'secao2' => [
                'cursos_presenciais_reciclagem',
                'curso_presencial_sbc',
                'participacao_jornadas_simposios',
                'conclusao_savic_acls_teca_a',
                'conclusao_cursos_distancia',
                'curso_online_captec'
            ],
            'secao3' => [
                'publicacao_artigo_original',
                'capitulo_livro_cardiologia',
                'oral_ou_poster_congresso',
                'participacao_registros_brasileiros'
            ]
        ] as $secao => $campos) {
            foreach($campos as $campo) {
                if ($this->getComprovante($campo)) {
                    if (!is_array($this->getComprovante($campo))) {
                        if ($this->getComprovanteStatus($campo) == 'valido') {
                            ${$secao} += $camposAtualizacao[$campo]['valor'];
                        }
                    } else {
                        foreach($this->getComprovante($campo) as $key => $dados) {
                            if ($this->getComprovanteStatus($campo, $key) == 'valido') {
                                ${$secao} += $camposAtualizacao[$campo]['valor'];
                            }
                        }
                    }
                }
            }
        }

        // verifica se foi comprovado o curso completo e adiciona 0.5
        if ($this->getComprovante('curso_online_captec')) {
            if (count(array_filter($this->getComprovante('curso_online_captec'), function($c) {
                return (array_key_exists('status', $c) && $c['status'] == 'valido');
            })) == 5) {
                $secao2 += 0.5;
            }
        }

        // verifica se o total da seçao ultrapassou o limite
        $secao1 = $secao1 > 4 ? 4 : $secao1;
        $secao2 = $secao2 > 4 ? 4 : $secao2;
        $secao3 = $secao3 > 2 ? 2 : $secao3;

        return [
            'b1' => $secao1 . ($secao1 <= 1 && $secao1 != 0 ? ' ponto' : ' pontos'),
            'b2' => $secao2 . ($secao2 <= 1 && $secao2 != 0 ? ' ponto' : ' pontos'),
            'b3' => $secao3 . ($secao3 <= 1 && $secao3 != 0 ? ' ponto' : ' pontos')
        ];
    }

    public function atualizaPontuacao()
    {
        $especializacao = 0;
        $facultativa    = 0;
        $total          = 0;


        // calcula comprovantes de especializacao
        foreach(Campos::especializacaoEmCardiologia() as $campo => $dados) {
            if ($this->getComprovante($campo) && $this->getComprovanteStatus($campo) == 'valido') {
                $especializacao += $dados['valor'];
            }
        }
        $especializacao = $especializacao > 10 ? 10 : $especializacao;


        // calcula comprovantes de documentos facultativos
        // dividido em 3 seçoes
        $secao1 = 0;
        $secao2 = 0;
        $secao3 = 0;

        $camposAtualizacao = Campos::documentosFacultativos()['atualizacao'];
        foreach([
            'secao1' => [
                'participacao_congressos_cardiologia',
                'congresso_brasileiro_cargiologia',
                'mestrado_cardiologia',
                'doutorado_livre_docencia_cardiologia'
            ],
            'secao2' => [
                'cursos_presenciais_reciclagem',
                'curso_presencial_sbc',
                'participacao_jornadas_simposios',
                'conclusao_savic_acls_teca_a',
                'conclusao_cursos_distancia',
                'curso_online_captec'
            ],
            'secao3' => [
                'publicacao_artigo_original',
                'capitulo_livro_cardiologia',
                'oral_ou_poster_congresso',
                'participacao_registros_brasileiros'
            ]
        ] as $secao => $campos) {
            foreach($campos as $campo) {
                if ($this->getComprovante($campo)) {
                    if (!is_array($this->getComprovante($campo))) {
                        if ($this->getComprovanteStatus($campo) == 'valido') {
                            ${$secao} += $camposAtualizacao[$campo]['valor'];
                        }
                    } else {
                        foreach($this->getComprovante($campo) as $key => $dados) {
                            if ($this->getComprovanteStatus($campo, $key) == 'valido') {
                                ${$secao} += $camposAtualizacao[$campo]['valor'];
                            }
                        }
                    }
                }
            }
        }

        // verifica se foi comprovado o curso completo e adiciona 0.5
        if ($this->getComprovante('curso_online_captec')) {
            if (count(array_filter($this->getComprovante('curso_online_captec'), function($c) {
                return (array_key_exists('status', $c) && $c['status'] == 'valido');
            })) == 5) {
                $secao2 += 0.5;
            }
        }

        // verifica se o total da seçao ultrapassou o limite
        $secao1 = $secao1 > 4 ? 4 : $secao1;
        $secao2 = $secao2 > 4 ? 4 : $secao2;
        $secao3 = $secao3 > 2 ? 2 : $secao3;

        $facultativa = $secao1 + $secao2 + $secao3;


        // verifica experiencia profissional
        // se existir soma 30 pontos no total
        // se nao calcula a soma de especializacao e documentos facultativos
        if ($this->getComprovante('experiencia_profissional_15_anos') && $this->getComprovanteStatus('experiencia_profissional_15_anos') == 'valido') {
            $facultativa = 30;
            $total = 30;
        } else {
            $total = $especializacao + $facultativa;
        }

        // atualiza cadastro
        $this->update([
            'pontuacao_especializacao' => $especializacao,
            'pontuacao_facultativa'    => $facultativa,
            'pontuacao_total'          => $total
        ]);
    }

    public function substituicoesSolicitadas()
    {
        $substituicoes = [];

        $campos = array_merge(
            [
                'crm',
                'socio_amb',
                'documentacao_obrigatoria',
                'declaracao_pessoa_portadora_deficiencia',
                'declaracao_veracidade_informacoes'
            ],
            array_keys(Campos::especializacaoEmCardiologia()),
            array_keys(Campos::documentosFacultativos()['atualizacao']),
            array_keys(Campos::documentosFacultativos()['experiencia'])
        );

        foreach($campos as $campo) {
            if (is_array($this->getComprovante($campo))) {
                foreach($this->getComprovante($campo) as $key => $dados) {
                    if ($this->getComprovanteStatus($campo, $key) == 'solicitado') {
                        $substituicoes[$campo][] = $key;
                    }
                }
            } else {
                if ($this->getComprovanteStatus($campo) == 'solicitado') {
                    $substituicoes[$campo] = true;
                }
            }
        }

        return $substituicoes;
    }

    public static function uploadComprovante()
    {
        $file = request()->file('arquivo');

        $path = 'comprovantes/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'_'.substr(md5($file->getClientOriginalName() . rand(1,100)), 0, 10).'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }

    public function verificaPagamento()
    {
        if ($this->pago) {
            return true;
        }

        $cpf = $this->cpf;
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf);

        try {
            $url = 'http://apiext.cardiol.br/InscricaoTEC/GetInscricao2020/'.$cpf;

            $data  = file_get_contents($url);
            $array = json_decode($data, true);

            if ($array['SituacaoPagamento'] == 'PAGO') {
                $this->pago = true;
                $this->save();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function verificaValidadeDocumentacaoObrigatoria()
    {
        if ($this->getComprovanteStatus('documentacao_obrigatoria') != 'valido') {
            return false;
        }

        if ($this->getComprovanteStatus('declaracao_veracidade_informacoes') != 'valido') {
            return false;
        }

        $statusCrms = [];
        foreach ($this->getComprovante('crm') as $key => $value) {
            $statusCrms[] = $this->getComprovanteStatus('crm', $key);
        }
        if (!in_array('valido', $statusCrms)) {
            return false;
        }

        $statusEspecializacao = [];
        foreach(array_keys(Campos::especializacaoEmCardiologia()) as $campo) {
            $statusEspecializacao[] = $this->getComprovanteStatus($campo);
        }
        if (!in_array('valido', $statusEspecializacao)) {
            return false;
        }

        return true;
    }

    public function recursos()
    {
        return $this->hasMany(Recurso::class, 'cadastro_id');
    }

    public function getPontuacaoFinalAttribute()
    {
        $pontuacaoProva = $this->pontuacao_prova_pos_recurso > 0
            ? $this->pontuacao_prova_pos_recurso
            : $this->pontuacao_prova;

        $total = $this->pontuacao_total + $pontuacaoProva;

        return $total > 120 ? 120 : $total;
    }

    public function historicoLiberacoes()
    {
        return $this->hasMany(HistoricoLiberacao::class, 'cadastro_id');
    }

    public function liberar($adminId, $observacoes)
    {
        $this->update([
            'status'                  => 'novo',
            'data_envio_documentacao' => null,
            'liberado'                => true
        ]);

        if (Tools::inscricoesEncerradas()) {
            CadastroExcecao::create(['cadastro_id' => $this->id]);
        }

        $this->historicoLiberacoes()->create([
            'admin_id'    => $adminId,
            'observacoes' => $observacoes,
            'dados'       => $this->toJson()
        ]);
    }
}

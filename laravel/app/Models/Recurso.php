<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    protected $table = 'recursos';

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            $model->protocolo = self::geraProtocolo($model);
            $model->questao_matriz = $model->questao;
            $model->save();

            // se não houver a questão matriz na tabela de respostas
            // cria uma linha com a resposta em branco
            if (! RecursoResposta::where('questao_matriz', $model->questao_matriz)->count()) {
                RecursoResposta::create([
                    'questao_matriz' => $model->questao_matriz,
                    'resposta'       => ''
                ]);
            }
        });
    }

    public function cadastro()
    {
        return $this->belongsTo(Cadastro::class, 'cadastro_id');
    }

    private static function geraProtocolo($recurso)
    {
        return 'R' . $recurso->cadastro->id . sprintf("%03d", $recurso->id);
    }

    public function getQuestaoBlocoAttribute()
    {
        $questao = $this->questao_matriz;

        if ($questao <= 50) {
            return $questao.' - bloco 1';
        }

        return ($questao - 50).' - bloco 2';
    }

    public static function selectQuestao()
    {
        $options = [];

        foreach (range(1, 100) as $i) {
            if ($i <= 50) {
                $options[$i] = 'questão '.$i.' - bloco 1';
            } else {
                $options[$i] = 'questão '.($i - 50).' - bloco 2';
            }
        }

        return $options;
    }

    private static function questaoMatriz($prova, $questao)
    {
        if (! in_array($prova, ['A', 'B', 'C', 'D']) || ! in_array($questao, range(1, 120))) {
            return 0;
        }

        $matriz = [
            'A' => range(1, 120),
            'B' => array_merge(
                range(31, 60),
                range(1, 30),
                range(91, 120),
                range(61, 90)
            ),
            'C' => array_merge(
                range(61, 120),
                range(1, 60)
            ),
            'D' => array_merge(
                range(91, 120),
                range(61, 90),
                range(31, 60),
                range(1, 30)
            ),
        ];

        return $matriz[$prova][$questao - 1];
    }
}

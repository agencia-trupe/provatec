<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecursoResposta extends Model
{
    protected $table = 'recursos_respostas';

    protected $guarded = ['id'];

    public function getQuestaoBlocoAttribute()
    {
        $questao = $this->questao_matriz;

        if ($questao <= 50) {
            return $questao.' - bloco 1';
        }

        return ($questao - 50).' - bloco 2';
    }
}
